import React, { useEffect } from "react";
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/auth'

const Hoc = (props) => {
    useEffect(() => {
        var timestamp = Math.floor(new Date().getTime() / 1000)     
                if( localStorage.getItem('exp') && localStorage.getItem('exp') <= timestamp) {
                    props.authLogout()
                    props.history.push('/login')
                }
    },[])
    
    if(localStorage.getItem('userData') ) {
        return <div  style ={{marginTop:30}}> {props.children} </div>
    } 
}

const mapDispatchToProps = dispatch => {
    return {
        authLogout : () => dispatch(actionTypes.authLogout())
    }
}

export default withRouter(connect(null, mapDispatchToProps)(Hoc))
