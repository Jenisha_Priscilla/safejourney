import React, { useEffect } from "react";
import {  Route, Redirect} from "react-router-dom";
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/auth'
import { withRouter } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest}) => { 
 
  useEffect(() => {
    var timestamp = Math.floor(new Date().getTime() / 1000)   
                if(localStorage.getItem('exp') && localStorage.getItem('exp') <= timestamp ) {
                  let props = {...rest}  
                  props.authLogout()
                  props.history.push('/login')
                }
  })
  
  return(
    <Route {...rest} render = {(props) => (
      localStorage.getItem('userData')
        ? <Component {...props}/>
        : <Redirect to = {{
          pathname: '/loginModal',
          state: { from: props.location }
        }} />
    )} />
  )}

  const mapDispatchToProps = dispatch => {
    return {
        authLogout : () => dispatch(actionTypes.authLogout())
    }
}

export default  withRouter(connect(null, mapDispatchToProps)(PrivateRoute))