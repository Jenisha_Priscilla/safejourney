import React, {  useState, useEffect } from "react";
import axios from 'axios';
import View from './view'

const LogInfo = () => {

    const style = {
        margin : '20px',
        outline: 'dotted',
        padding : '25px'
    } 

    const [log, setLog] = useState([])
    useEffect(() => {
        axios.get('/user/getLog')
        .then(response => {
            setLog(response.data); 
        }) 
        .catch(err => console.log(err))
    },[])

    const read = () => {
        return log.map(function(file) {
            return <View obj = {file} />
        })
    }

    return <p style = {style}> {read()}</p>
}

export default LogInfo;