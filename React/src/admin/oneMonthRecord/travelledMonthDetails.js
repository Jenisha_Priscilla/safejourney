import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'
import { connect } from 'react-redux';
import Hoc from '../../hoc/hoc'
import CountMonth from './countMonth'
import Record from '../oneDayRecord/record';
import Image from 'react-bootstrap/Image'
import Spinner from '../../spinner/spinner'

const TravelledDetails = (props) => {

    const bookingRecordList = () => {
        return props.travelledDetails.map (function(object, i){   
            return <Record obj = {object} key ={i} />; 
        });
    }
        let record = props.loading ? <Spinner/> : props.travelledDetails  ? <Table responsive  hover >
                <thead>
                    <tr>
                        <th> User Name</th>
                        <th> Bus Name </th>
                        <th> Bus Type</th>
                        <th> PlateNo.</th>
                        <th> Departure station </th>
                        <th> Arrival station </th>
                        <th> Journey date</th>
                        <th> Booked date</th>
                        <th> No.of ticket</th>
                    </tr>
                </thead>
                <tbody>
                    {bookingRecordList()}
                </tbody>
            </Table> :<div style ={{textAlign:"center",marginTop:40}}> 
                <Image
                    alt = ""
                    src = {process.env.REACT_APP_API+"/image/record.png"}
                    height = "190"
                    width = "190"
                />  <br/>
               
            </div>
         
        return (
            <>
                <CountMonth></CountMonth>
                    <h5 align = "center">  One month record </h5>
                    {record}
            </>
        )
    
}
const mapStateToProps = state => {
    return {
        travelledDetails : state.record.travelledDetails,
        loading : state.record.loading
    }
}

export default connect(mapStateToProps, null)(TravelledDetails) 
