import * as actionTypes from '../actions/actionsTypes'

const initialState = {
    bus: {},
    routeIds : [],
    redirect :false,
    updateSuccess :false,
    update: 1
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.HANDLE_BUSCHANGE :
            action.event.persist();
            const updatedBus = {
                ...state.bus
            }
            const updatedBusElement = {
                ...updatedBus[action.event.target.name]
            }
            updatedBusElement.value = action.event.target.value;
            updatedBus[action.event.target.name] = updatedBusElement.value 
            
            return {
                ...state,
                bus : updatedBus
            }

        case actionTypes.HANDLE_ROUTECHANGE:
            const UpdatedBus = {
                ...state.bus
            }
            const UpdatedBusElement = {
                ...UpdatedBus['RouteId']
            }            
            UpdatedBusElement.value = action.routeId;
            UpdatedBus['RouteId'] = UpdatedBusElement.value;
            return {
                ...state,
                bus:UpdatedBus
            }
            
        case actionTypes.SET_STATION:
            const updatedValue = {
                ...state.bus
            }
            const updatedDepartureStationElement = {
                ...updatedValue['DepartureStation']
            }
            const updatedArrivalStationElement = {
                ...updatedValue['ArrivalStation']
            }
            updatedDepartureStationElement.value =  action.stations.DepartureStation 
            updatedArrivalStationElement.value = action.stations.ArrivalStation
            updatedValue['DepartureStation'] = updatedDepartureStationElement.value
            updatedValue['ArrivalStation'] =  updatedArrivalStationElement.value
                return {
                    ...state,
                    bus:updatedValue
                }   
        
        case actionTypes.SET_ROUTEIDS:
            return {
                ...state,
                routeIds : action.routeIds
            }
            
        case actionTypes.BUS_INSERT_SUCCESS:
            const updated = {
                BusName : '',
                BusType : '',
                PlateNo : '',
                RouteId : '',
                Fare : '',
                SeatsCount :'',
                DepartureTime : ''
            }
            return {
                ...state,
                redirect:true,
                bus:updated
            }
        
        case actionTypes.SET_BUS:
            return {
                ...state,
                bus : action.busEditData
            }
        
        case actionTypes.BUS_UPDATE_SUCCESS:
            return {
                ...state,
                updateSuccess : true,
                update : state.update-2,
            }
    
        case actionTypes.BUS_UPDATE:
            return {
                ...state,
                update : state.update + 1,
                updateSuccess: false 
        }
                  
        default : 
            return state;
    }
};

export default reducer;