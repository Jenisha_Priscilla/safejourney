import * as actionTypes from '../actions/actionsTypes'

var date = new Date();
        var ddd = date.getDate();
        var mmm = date.getMonth()+1; 
        var yyy = date.getFullYear();
        if (ddd<10) {
            ddd='0'+ddd;
        } 
        
        if (mmm<10) {
            mmm='0'+mmm;
        } 
date = yyy+'-'+mmm+'-'+ddd;

const initialState = {
    record: [],
    dayRecord : [],
    cancelledCount : '',
    confirmedCount : '',
    date : date,
    cancelledDetails : [],
    confirmedDetails : [],
    travelledDetails : [],
    travelledCount : '',
    loading : true,
    perPage: 5,        
    currentPage: 1,
};

const recordReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.FETCH_REQUEST:
            return {
                ...state,
                loading : true
            }

        case actionTypes.RECORD:
            let offset = state.currentPage*state.perPage - state.perPage;
            const slice = action.record.slice(offset, offset+state.perPage);
            const data = action.record;
           
            return {
                ...state,
                record : slice,
                loading : false,
                pageCount: Math.ceil(data.length / state.perPage)
            }

        case actionTypes.DAY_RECORD:
            let off = state.currentPage*state.perPage - state.perPage;
            const daySlice = action.dayRecord.slice(off, off+state.perPage);
            const dayData = action.dayRecord;

            return {
                ...state,
                dayRecord : daySlice,
                loading : false,
                pageCount: Math.ceil(dayData.length / state.perPage)
            }

        case actionTypes.CONFIRMED_COUNT:
            return {
                ...state,
                confirmedCount : action.confirmedCount
            }

        case actionTypes.CANCELLED_COUNT:
            return {
                ...state,
                cancelledCount : action.cancelledCount
            }

        case actionTypes.SET_CANCELLED_DETAILS:
            return {
                ...state,
                cancelledDetails: action.cancelledDetails,
                loading : false
            }

        case actionTypes.SET_CONFIRMED_DETAILS:
            return {
                ...state,
                confirmedDetails : action.confirmedDetails,
                loading : false
            }   
        
        case actionTypes.SET_TRAVELLED_DETAILS:
            return {
                ...state,
                travelledDetails : action.travelledDetails,
                loading : false
            }
            
        case actionTypes.TRAVELLED_COUNT:
            return {
                ...state,
                travelledCount : action.travelledCount
            }

        case actionTypes.SET_DATE:
            return {
                ...state,
                date : action.event.target.value
            }

        case actionTypes.SET_CURRENT_PAGE:
            return {
                ...state,
                offset: 0,       
                currentPage: 1
            }

        case actionTypes.PAGINATION:
            return {
                ...state,
                currentPage : action.selectedPage,
                offset : action.selectedPage * state.perPage
                }
                
        default:
            return state;

}
}

export default recordReducer;
