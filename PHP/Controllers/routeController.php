<?php
class RouteController
{
	
	private $model;
	function __construct($tile)
	{
		$this->model = new $tile;
	}
	  
  	public function delete($id)
  	{	
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid->role == "Admin") {
			return $this->model->delete($id, $isValid->name);
		} else {
			return false;
		}
  	}
	
	public function view()
	{
		return $this->model->view();
	}

	public function insert()
	{
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid->role == "Admin") {
			$postdata =file_get_contents("php://input");
			$request = json_decode($postdata);
			require_once "./class/route.php";
			$route = new Route();
			$route->__set('departureStation',$request->departureStation);
			$route->__set('arrivalStation',$request->arrivalStation);
			return $this->model->insert( $route->toArray(), $isValid->name);
		} else {
			return false;
		}
	}

	public function select($id)
	{
		return $this->model->select($id);
	}

	public function update()
	{
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid->role == "Admin")  {
			$postdata =file_get_contents("php://input");
			$request = json_decode($postdata);
			require_once "./class/route.php";
			$route = new Route();
			$route->__set('id',$request->id);
			$route->__set('departureStation',$request->departureStation);
			$route->__set('arrivalStation',$request->arrivalStation);
			return $this->model->update($route->__get('id'), $route->toArray(), $isValid->name);
		} else {
			return false;
		}
	}

	public function getRouteId() 
	{
		return $this->model->getRouteId();
	}

	public function searchByRoute($route)
	{
		return $this->model->searchByRoute($route);
	}
}
