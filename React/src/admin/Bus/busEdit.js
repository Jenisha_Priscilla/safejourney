import React, { Component } from 'react'
import Form from 'react-bootstrap/Form'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/index'
import NetworkError from '../../component/networkError';

class BusEdit extends Component 
{
    constructor() {
        super();
        this.handleRouteChange =this.handleRouteChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            fieldError : '',
            busNameError : '',
            busTypeError : '',
            plateNoError : '',
            routeIdError : '',
            fareError : '',
            seatCountError : '',
            departureTimeError : '', 
            arrivalTimeError : '', 
        }
    }

    componentDidMount() {
        this.props.getBus(this.props.BusId);
        this.props.onInitRouteIds();
    }

    validate() {
        var val = 0 ;
        var fieldError = " ";
        var plateNoError = " ";
        var busNameError = " ";
        var routeIdError = " ";
        var busTypeError = " ";
        var fareError = " ";
        var seatCountError = " ";
        var departureTimeError = " ";
        var arrivalTimeError = " ";
        
        if (this.props.bus.RouteId == '' || this.props.bus.BusName == '' || this.props.bus.BusType == '' || 
        this.props.bus.Fare == '' || this.props.bus.PlateNo == '' || this.props.bus.SeatsCount == '' || 
        this.props.bus.DepartureTime == '' || this.props.bus.ArrivalTime == '' ) {
            fieldError = '* All fields must be filled';
        }
        if(fieldError === " ") {
            var patt1 = /^[a-zA-Z ]+$/g; 
            if (!this.props.bus.BusName.match(patt1)) {
                busNameError = 'Must contain only alphabets';
            }
            if (!this.props.bus.BusType.match(patt1)) {
                busTypeError = 'Must contain only alphabets';
            }
            var patt2 = /[A-Z]{2}\s[0-9|\s]{1,2}\s[A-Z|\s]{1,2}\s[0-9]{1,4}/g; 
            if (!this.props.bus.PlateNo.match(patt2)) {
                plateNoError = 'Enter valid plate number';
            }
            var patt3 = /^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/
            if (!this.props.bus.DepartureTime.match(patt3)) {
                departureTimeError = 'Enter valid time';
            }
            if (!this.props.bus.ArrivalTime.match(patt3)) {
                arrivalTimeError = 'Enter valid time';
            }
            if (this.props.bus.SeatsCount < 0) {
                seatCountError = 'Enter valid seat count';
            }
            if (this.props.bus.Fare < 0) {
                fareError = 'Enter valid fare';
            }
            for (var i = 0; i < this.props.routeIds.length; i++) {
                if (this.props.bus.RouteId == this.props.routeIds[i]['Id']) {
                    val = val+1;
                }
            } 
            if (val === 0) {
                routeIdError = 'Enter valid route id';
            }
        }

        if (fieldError != " " || busNameError != " " || busTypeError != " " || departureTimeError != " "||
        arrivalTimeError != " " || plateNoError != " " || routeIdError!= " " || fareError != " " || seatCountError != " ") {
            this.setState({fieldError, busNameError, busTypeError,departureTimeError, arrivalTimeError,plateNoError, routeIdError, fareError, seatCountError})
            return false;
        }

        return true;
    }
    

    onSubmit(e) {
      
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
        const busData = {
                routeId:this.props.bus.RouteId,
                busName:this.props.bus.BusName,
                busType:this.props.bus.BusType,
                plateNo:this.props.bus.PlateNo,
                seatCount:this.props.bus.SeatsCount,
                fare:this.props.bus.Fare,
                departureTime:this.props.bus.DepartureTime,
                arrivalTime :this.props.bus.ArrivalTime
            };
            this.props.onBusUpdate(this.props.BusId,busData)
            this.props.onEdit() 
        }
    }

    handleRouteChange(e) {
        console.log(e.target.value)
        this.props.getStation(e.target.value)
        this.props.onRouteChange(e.target.value)
    }

    render() {
        const style = {
            fontSize : 10,
            color : 'red'
        }
        if(this.props.showError) {
            console.log("networkError")
            return  <NetworkError/>
        }
        
            return (
                <tr>
                    <td>
                        <Form.Control type = "number" 
                            value = {this.props.bus.RouteId} name = "RouteId"
                            onChange = {this.handleRouteChange}
                        />
                        <div style = {style}>
                            {this.state.routeIdError}
                        </div> 
                              
                        <div style ={style}>
                            {this.state.fieldError}
                        </div> 
                   </td>
                   <td > 
                        <Form.Control type = "text"
                            value = {this.props.bus.DepartureStation}
                            disabled 
                        />   
                   </td>
                   <td> 
                        <Form.Control type = "text" 
                            value = {this.props.bus.ArrivalStation}
                            disabled 
                        />
                   </td>
                   <td> 
                        <Form.Control type = "text" name = "BusName"
                               value = {this.props.bus.BusName}
                               onChange = {this.props.onBusChange}
                        />   
                        <div style = {style}>
                            {this.state.busNameError}
                        </div>
                   </td>
                    <td>
                        <Form.Control type = "text" name = "BusType"
                            value= {this.props.bus.BusType}
                            onChange = {this.props.onBusChange}
                        />
                        <div style = {style}>
                            {this.state.busTypeError}
                        </div>  
                    </td>
                   <td>
                        <Form.Control type = "text" name = "PlateNo"
                            value= {this.props.bus.PlateNo}
                            onChange = {this.props.onBusChange}
                        />
                        <div style = {style}>
                            {this.state.plateNoError}
                        </div>
                   </td>
                   <td>
                        <Form.Control type = "number" name = "SeatsCount"
                            value= {this.props.bus.SeatsCount}
                            onChange = {this.props.onBusChange}
                        />
                        <div style = {style}>
                            {this.state.seatCountError}
                        </div>   
                   </td>
                   <td>
                        <Form.Control type ="number" name = "Fare"
                            value = {this.props.bus.Fare}
                            onChange = {this.props.onBusChange}
                        />
                        <div style = {style}>
                            {this.state.fareError}
                        </div> 
                   </td>
                   <td>
                        <Form.Control type = "text" name = "DepartureTime"
                            value = {this.props.bus.DepartureTime}
                            onChange = {this.props.onBusChange}
                        />
                        <div style ={style}>
                            {this.state.departureTimeError}
                        </div> 
                   </td>
                   <td>
                        <Form.Control type = "text" name = "ArrivalTime"
                            value = {this.props.bus.ArrivalTime}
                            onChange = {this.props.onBusChange}
                        />
                        <div style ={style}>
                            {this.state.arrivalTimeError}
                        </div> 
                   </td>
                   <td >
                        <div variant="dark" onClick = {this.onSubmit} > 
                            <FontAwesomeIcon icon = {faCheck} size = "sm" />
                        </div>
                   </td>
                </tr>       
            )
        
    }
}
const mapStateToProps = state => {
    return {
        bus : state.bus.bus,
        routeIds : state.bus.routeIds,
        updateSuccess : state.bus.updateSuccess,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        onBusChange : (e) => dispatch(actionTypes.handleBusChange(e)),
        onRouteChange : (routeId) => dispatch(actionTypes.handleRouteChange(routeId)),
        onInitRouteIds : () => dispatch(actionTypes.initRouteIds()),
        getStation : (routeId) => dispatch(actionTypes.getStation(routeId)),
        getBus : (busId) => dispatch(actionTypes.getBus(busId)),
        onBusUpdate : (busId, busData)=> dispatch(actionTypes.busUpdate(busId, busData))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusEdit)