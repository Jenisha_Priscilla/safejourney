import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/inValidModal'
import Modal from 'react-bootstrap/Modal'
import React from "react";
const InValidModal = (props) => 
{ 
    return (
      <>
        <Modal
          size = "lg"
          show = {props.show}
          onHide = {()=>props.hide()}
        >
          <Modal.Header closeButton>
            <Modal.Title >
              Session expired
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>Session expired login again</Modal.Body>
        </Modal>
      </>
    );
  }

  const mapStateToProps = state => {
    return {
        show : state.inValidModal.show
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        hide : () => dispatch(actionTypes.hide()),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(InValidModal)