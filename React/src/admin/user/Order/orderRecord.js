import React from 'react'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { withRouter } from 'react-router-dom';
import { faLongArrowAltRight } from "@fortawesome/free-solid-svg-icons";

const OrderRecord = (props) => {
    const date = (d) => {
        var date = new Date( d);
        var ddd = date.getDate();
        var mmm = date.getMonth()+1; 
        var yyy = date.getFullYear();
        if (ddd<10) {
            ddd='0'+ddd;
        } 
        
        if (mmm<10) {
            mmm='0'+mmm;
        } 
        return date = ddd+'/'+mmm+'/'+yyy;
    }

    const status = () => {
        var today = new Date();
        var dd = today.getDate();  
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if (dd<10) {
            dd='0'+dd;
        } 
        
        if (mm<10) {
            mm='0'+mm;
        } 
        var time = (today.getHours()+':'+today.getMinutes()+':'+today.getSeconds())
        today = yyyy+'-'+mm+'-'+dd;

    
        if (props.obj.del == 1 || props.obj.NoOfTickets == 0) {
            return( <div style = {{ color:"red"}}>
            Cancelled
        </div>)
        }

        if ( new Date(today+' '+time) <= new Date(props.obj.JourneyDate +' '+props.obj.DepartureTime) && props.obj.NoOfTickets != 0) {
            return(
                <div style = {{ color:"green"}}>
            Confirmed
        </div>
                
            )
        }
        else {
           
            return(<div style = {{ color:"blue"}}>
            Travelled
        </div>)
        }
    }

        return (

             <>
                <Card border = 'success' className = "d-none d-lg-block p-auto"   onClick ={() =>  props.history.push('/cancelOrder/'+props.obj.Id+'/'+props.obj.JourneyDate+'/'+props.obj.DepartureTime+'/'+props.obj.del)}>
                    <Card.Body>
                        <Row>
                            <Col style = {{fontSize : 13, fontWeight: 'bold'}}>
                                {props.obj.BusName}
                            </Col>
                            <Col >
                                {props.obj.DepartureStation}
                            </Col>
                            <Col sm = {1} style = {{paddingRight :'0', paddingLeft: '0'}}>
                                <FontAwesomeIcon icon={ faLongArrowAltRight }/>
                            </Col>
                            <Col >
                                {props.obj.ArrivalStation}
                            </Col>
                            <Col style = {{fontWeight : 'bold', fontSize : 12}}>
                                {props.obj.PlateNo}
                            </Col>
                            <Col>
                                {status()}
                            
                            </Col>
                        </Row>
                        <Row style = {{fontSize : 10}}>
                            <Col>
                                {props.obj.BusType}
                            </Col>   
                            < Col >
                                {props.obj.DepartureTime}
                            </Col>
                            < Col sm = {1} style = {{paddingRight :'0', paddingLeft: '0'}}></Col>
                            <Col>
                            {props.obj.ArrivalTime}
                            </Col>
                            < Col>
                            {date(props.obj.JourneyDate)}
                            </Col>
                            <Col></Col>
                        </Row>
                    </Card.Body>
                </Card>
               
                  
                <Card border = 'success' className = "d-lg-none d-md-block" 
                      onClick ={() => props.history.push('/cancelOrder/'+props.obj.Id+'/'+props.obj.JourneyDate+'/'+props.obj.DepartureTime+'/'+props.obj.del)}>
                    <Card.Body>
                    <Row sm={4}>
                        <Col xs = {6} style = {{fontSize : 13, fontWeight: 'bold'}}>
                            {props.obj.BusName}
                        </Col>
                        <Col style =  {{fontSize : 13, fontWeight: 'bold'}}>
                        {props.obj.PlateNo}
                        </Col>
                    </Row>
                    <Row sm={4} style = {{fontSize : 10}}>
                        <Col xs = {7}> {props.obj.BusType}</Col>
                        <Col>  {date(props.obj.JourneyDate)}</Col>
                    </Row>
                    <Row sm={4} style = {{fontSize : 10}}>
                        <Col xs ={7}> {props.obj.DepartureStation}{' '}
                        <FontAwesomeIcon icon = { faLongArrowAltRight }/> 
                        {' '}   {props.obj.ArrivalStation}
                        </Col>
                        <Col>
                            {status()}
                        </Col>
                    </Row>
                    <Row sm={4} style = {{fontSize : 10}}>
                        <Col xs = {8}> {props.obj.DepartureTime } {' '}
                        <FontAwesomeIcon icon = { faLongArrowAltRight }/> 
                        {' '}{props.obj.ArrivalTime} 
                        </Col>
                    </Row>
                    </Card.Body>
                </Card> 
                <br/>
            </>
        )
}

export default withRouter(OrderRecord)