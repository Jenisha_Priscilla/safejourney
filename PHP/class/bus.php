<?php
class Bus
{
    private $id;
    private $routeId;
    private $busName;
    private $busType;
	private $plateNo;
	private $seatsCount;
	private $fare;
    private $departureTime;
    private $arrivalTime;

    public function __set($property, $value)
    { 
        require_once './validation/validateProperty.php';
        $isValid = valid($value);
        if($isValid) {
            if(property_exists($this, $property)) {
                $this->$property = $value;
            }
           
        }
    }

    public function __get($property)
    {
        if(property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function toArray () 
	{	
        return [
            "routeId" => $this->__get('routeId'),
            "busType" => $this->__get('busType'),
			"busName" => $this->__get('busName'),
            "plateNo" => $this->__get('plateNo'),
			"seatsCount" => $this->__get('seatsCount'),
            "fare" => $this->__get('fare'),
            "departureTime" => $this->__get('departureTime'),
            "arrivalTime" => $this->__get('arrivalTime')
        ];
    }
}
