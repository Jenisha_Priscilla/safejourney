<?php
class BookingRecordController
{
	private $model;
	function __construct($tile)
	{
		$this->model = new $tile;
	}

	public function confirmedBooking($date)
	{
		return $this->model->confirmedBooking($date);
	}

	public function cancelledBooking($date)
	{
		return $this->model->cancelledBooking($date);
	}

	public function todayBookingDetails()
	{
		return $this->model->todayBookingDetails();
	}

	public function searchRecord()
	{
		return $this->model->searchRecord();
	}

	public function searchUserName($userName)
	{
		return $this->model->searchUserName($userName);
	}

	public function searchBusName($busName)
	{
		return $this->model->searchBusName($busName);
	}

	public function searchDepartureStation($departureStation) 
	{
		return $this->model->searchDepartureStation($departureStation);
	}

	public function searchArrivalStation($arrivalStation) 
	{
		return $this->model->searchArrivalStation($arrivalStation);
	}

	public function searchTicket($ticket) 
	{
		return $this->model->searchTicket($ticket);
	}

	public function searchConfTicket($confTicket) 
	{
		return $this->model->searchConfTicket($confTicket);
	}

	public function searchJourneyDate($journeyDate)
	{
		return $this->model->searchJourneyDate($journeyDate);
	}

	public function searchBookedDate($bookedDate)
	{
		return $this->model->searchBookedDate($bookedDate);
	}

	public function searchStatus($status)
	{
		return $this->model->searchStatus($status);
	}

	public function oneDayRecord($date)
	{
		return $this->model->oneDayRecord($date);
	}

	public function cancelledDetails($date)
	{
		return $this->model->cancelledDetails($date);
	}

	public function confirmedDetails($date)
	{
		return $this->model->confirmedDetails($date);
	}

	public function oneMonthRecord($date)
	{
		return $this->model->oneMonthRecord($date);
	}

	public function cancelledMonthDetails($date)
	{
		return $this->model->cancelledMonthDetails($date);
	}

	public function confirmedMonthDetails($date)
	{
		return $this->model->confirmedMonthDetails($date);
	}

	public function cancelledMonthCount($date)
	{
		return $this->model->cancelledMonthCount($date);
	}

	public function confirmedMonthCount($date)
	{
		return $this->model->confirmedMonthCount($date);
	}

	public function travelledCount($date)
	{
		return $this->model->travelledCount($date);
	}
	
	public function travelledDetails($date)
	{
		return $this->model->travelledDetails($date);
	}
}
