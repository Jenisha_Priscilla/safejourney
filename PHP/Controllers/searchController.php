<?php
class SearchController
{
	private $model;
  	function __construct($tile)
  	{
		  $this->model = new $tile;
  	}

	public function index()
	{
		return $this->model->index();
	}

	public function applyFilter()
	{
		return $this->model->applyFilter();
	}

	public function destinationStation()
	{
		return $this->model->destinationStation();
	}

	public function arrivalStation()
	{
		return $this->model->arrivalStation();
	}

	public function isLoggedIn() 
	{
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid) {
			return $isValid;
		} else {
			return false;
		}
	}
}
 ?>

