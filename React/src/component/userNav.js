import React from 'react';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import './nav.css'

const Navg = () => {
 
    const style = {
      color : 'grey',
      padding : '0.5rem 1rem',
      textDecoration : 'none'
    }
  
    var status = 'Login'
    if (localStorage.getItem('userData')) {
       status = 'Logout'
    }
    
    if (localStorage.getItem('userData') === '' ) {
       status = 'Login'
    }

    return (
      <Navbar  collapseOnSelect expand = "lg" variant = 'dark' >
        <Navbar.Brand >
         <img
           alt = ""
           src = {process.env.REACT_APP_API+"/image/makeTrip.png"}
           width = "30"
           height = "30"
          /> {' '}    <Link to = "/" className = 'd-inline p-2 text-decoration-none text-white'>Safe Journey</Link> 
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse >
        <Nav className = "mr-auto"></Nav>
        <Nav>
          {
          localStorage.getItem('userData') ?
          <Link id="RouterNavLink" style={style}  to="/order">My trips</Link>
         :null
          }
         <Link to = {"/"+ status }  style = {style}> {status} </Link>
        </Nav>
        </Navbar.Collapse>
      </Navbar>  
    )
}
const mapStateToProps = state => {
  return {
      auth : state.auth.userId
  }
}


export default connect(mapStateToProps, null)(Navg)

