<?php
class Route
{
    private $id;
    private $departureStation;
    private $arrivalStation;

    public function __set($property, $value)
    { 
        require_once './validation/validateProperty.php';
        $isValid = valid($value);
        if($isValid) {
            if(property_exists($this, $property)) {
                $this->$property = $value;
            }
            return $this;
        }
    }

    public function __get($property)
    {
        if(property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function toArray () 
	{	
        return [
            "departureStation" => $this->__get('departureStation'),
            "arrivalStation" => $this->__get('arrivalStation')
        ];
    }
}
