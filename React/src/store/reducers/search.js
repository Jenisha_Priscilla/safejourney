import * as actionTypes from '../actions/actionsTypes'

const initialState = {
    search : {
        departureStation :'',
        arrivalStation :'',
        journeyDate :'',
    },
    searchResult: [],
    filterResult :[],
    type : [{type: "'AC sleeper'", check : false, value : 'AC sleeper'},
        {type: "'Non AC sleeper'", check : false,  value : 'Non AC sleeper'},
        {type: "'AC semi sleeper'", check : false, value : 'AC semi sleeper'},
        {type: "'Non AC semi sleeper'", check : false, value : 'Non AC semi sleeper'}
    ],
    time :[{time : 'morning', check : false, value : 'Morning (00:01 - 11:59)'},
        {time : 'noon', check : false, value : 'Noon (12:00 - 18:00)'},
        {time : 'night', check : false, value : 'Night (18:01 - 23:59)'}],
    busType : [],
    departureTime : [],
    loading : true,
    filterOpen : false,
    stateLost : '1',
    perPage: 3,        
    currentPage: 1,
} 

const searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.HANDLE_CHANGE:
            action.event.persist();
            const updatedSearch = {
                ...state.search
            }
            const updatedSearchElement = {
                ...updatedSearch[action.event.target.name]
            }
            
            updatedSearchElement.value = action.event.target.value;
            updatedSearch[action.event.target.name] = updatedSearchElement.value 
            localStorage.setItem(action.event.target.name, action.event.target.value)
            return {
                ...state,
                search : updatedSearch
            }

        case actionTypes.SEARCH_RESULT:
            let off = state.currentPage*state.perPage - state.perPage;
            const slice = action.searchResult.slice(off, off+state.perPage);
            const data = action.searchResult;
            return {
                ...state,
                searchResult : slice,
                stateLost : '0',
                loading : false,
                pageCount: Math.ceil(data.length / state.perPage)
            }
            
        case actionTypes.NO_RESULT:
            return {
                ...state,
                stateLost : '0'
            }
        
        case actionTypes.FETCH_REQUEST:
            return {
                ...state,
                loading : true
            }
        
        case actionTypes.FILTER_SUCCESS:
            return {
                ...state,
                filterResult : action.filterData,
                stateLost : '0',
                loading: false
            }

        case actionTypes.FILTER_TYPE: 
            let filterType = state.type
            for(let i = 0; i < state.type.length; i++ ){
                if(state.type[i]['type'] == action.value) {
                    filterType[i]['check'] = true;
                }
            }
            return {
                ...state,
                type : filterType,
                busType : [...state.busType,(action.value)]
            }

        case actionTypes.FILTER_REMOVE_TYPE:
            let filterRemoveType = state.type
            for(let i = 0; i < state.type.length; i++ ){
                if(state.type[i]['type'] == action.value) {
                    filterRemoveType[i]['check'] = false;
                }
            }
        return {
            ...state,
            type : filterRemoveType,
            busType: state.busType.filter(r => r !== action.value)
        }

        case actionTypes.FILTER_TIME: 
            let filterTime = state.time
            for(let i = 0; i < state.time.length; i++ ){
                if(state.time[i]['time'] == action.value) {
                    filterTime[i]['check'] = true;
                }
            }
            return {
                ...state,
                time: filterTime,
                departureTime : [...state.departureTime,(action.value)]
            }

        case actionTypes.FILTER_REMOVE_TIME:
            let filterRemoveTime = state.time
            for(let i = 0; i < state.time.length; i++ ){
                if(state.time[i]['time'] == action.value) {
                    filterRemoveTime[i]['check'] = false;
                }
            }
        return {
            ...state,
            time : filterRemoveTime,
            departureTime: state.departureTime.filter(r => r !== action.value)
        }

        case actionTypes.REMOVE_ALL_FILTER:
            let removeFilterTime = state.time
            for(let i = 0; i < state.time.length; i++ ){
                    removeFilterTime[i]['check'] = false;
            }
            let removeFilterType = state.type
            for(let i = 0; i < state.type.length; i++ ){
                    removeFilterType[i]['check'] = false;
            }
            return {
                ...state,
                time : removeFilterTime,
                type : removeFilterType,
                busType : [],
                departureTime : []
            }
        
        case actionTypes.FILTER_OPEN: 
            return {
                ...state,

                filterOpen : !state.filterOpen
            }
            
        case actionTypes.SEARCH_PAGINATION:
            return {
                ...state,
                currentPage : action.selectedPage,
                offset : action.selectedPage * state.perPage
            }
            
        default : 
            return state;
    }
}
export default searchReducer;


