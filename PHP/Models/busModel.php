<?php
class BusModel 
{
	public $pdo;
	function __construct()
	{
		$db = new Database();
		$this->pdo = $db->__get('pdo');
	//   $this->pdo = $pdo;
	}
	  
	public function insert($data, $name)
	{
		$routeId = $data["routeId"];
		$busName = $data["busName"];
		$busType = $data["busType"];
		$plateNo = $data["plateNo"];
		$seatCount = $data["seatsCount"];
		$fare = $data["fare"];
		$departureTime = $data["departureTime"];
		$arrivalTime = $data["arrivalTime"];
		$sql = "INSERT INTO Bus(RouteId, BusName, BusType, PlateNo, SeatsCount, Fare, DepartureTime, ArrivalTime)
		VALUES 
		('$routeId', '$busName', '$busType', '$plateNo', '$seatCount', '$fare', '$departureTime', '$arrivalTime')";
		 if($this->pdo->query($sql))
		 {
			require_once "./log/log.php";
			logg($name, __METHOD__, $data);
			return true;
		 }
		
	}

	public function view()
	{
		$data = [];
		$sql = "select Bus.RouteId,Bus.Id,Bus.ArrivalTime,Bus.BusName,Bus.BusType,Bus.DepartureTime,Bus.PlateNo,
		Bus.fare,Bus.SeatsCount,Route.ArrivalStation,Route.DepartureStation from Bus inner join 
		Route on Bus.RouteId = Route.Id where Bus.del = 0 ORDER BY Bus.Id desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['BusId']=$row['Id']; 
			$data[$cr]['RouteId']=$row['RouteId']; 
			$data[$cr]['BusName']=$row['BusName']; 
			$data[$cr]['BusType']=$row['BusType']; 
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['SeatsCount']=$row['SeatsCount'];
			$data[$cr]['Fare']=$row['fare'];
			$data[$cr]['DepartureTime']=$row['DepartureTime'];
			$data[$cr]['ArrivalTime']=$row['ArrivalTime'];
			$data[$cr]['DepartureStation']=$row['DepartureStation'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation'];
			$cr++;
		}
		return json_encode($data);
	}

	public function delete($id, $name)
	{
		$id = $id[0];
		$sql = "update Bus set del = 1 where Id ='$id' ";
		$query = $this->pdo->query($sql);
		$sql1 = "UPDATE BusBookingDetails set del = '1'
		where BusId = '$id' AND BusBookingDetails.JourneyDate >= curdate();";
		if($this->pdo->query($sql1)) {
			require_once "./log/log.php";
			logg($name, __METHOD__, $id);
			return true;
		}
	}

	public function select($id)
	{
		$data = [];
		$id = $id[0];
		$sql2 = "select  Bus.Id, Bus.RouteId,Bus.ArrivalTime,
		Bus.BusName, Bus.BusType, Bus.PlateNo, Bus.SeatsCount,
		Bus.Fare,Bus.DepartureTime,Route.DepartureStation,
		Route.ArrivalStation  from Bus inner join Route on Bus.RouteId = Route.Id where Bus.Id = '$id';";		
		$query = $this->pdo->query($sql2);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['BusId']=$row['Id']; 
			$data[$cr]['RouteId']=$row['RouteId']; 
			$data[$cr]['BusName']=$row['BusName']; 
			$data[$cr]['BusType']=$row['BusType']; 
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['SeatsCount']=$row['SeatsCount'];
			$data[$cr]['Fare']=$row['Fare'];
			$data[$cr]['DepartureTime']=$row['DepartureTime'];
			$data[$cr]['ArrivalTime']=$row['ArrivalTime'];
			$data[$cr]['DepartureStation']=$row['DepartureStation'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation'];
			$cr++;
		}
		
		return json_encode($data);
	}

	public function update($id, $data, $name)
	{
		$routeId = $data["routeId"];
		$busName = $data["busName"];
		$busType = $data["busType"];
		$plateNo = $data["plateNo"];
		$seatCount = $data["seatsCount"];
		$fare = $data["fare"];
		$departureTime = $data["departureTime"];
		$arrivalTime = $data["arrivalTime"];
		
		$sql = "UPDATE Bus SET 
			RouteId = '$routeId',
			BusName= '$busName',
			BusType = '$busType',
			PlateNo = '$plateNo',
			SeatsCount = '$seatCount',
			Fare = '$fare',
			ArrivalTime = '$arrivalTime',
			DepartureTime = '$departureTime' WHERE Id = $id LIMIT 1";	
		if($this->pdo->query($sql)) {
			require_once "./log/log.php";
			logg($name, __METHOD__ , $id, $data);
			return true;
		}
		
	}

	public function searchByBusName($busName) 
	{
		$search = $busName[0];
		$sql = "select Bus.RouteId,Bus.Id,Bus.ArrivalTime,Bus.BusName,Bus.BusType,Bus.DepartureTime,Bus.PlateNo,
		Bus.fare,Bus.SeatsCount,Route.ArrivalStation,Route.DepartureStation from Bus inner join 
		Route on Bus.RouteId = Route.Id where Bus.del = 0 and BusName like '%$search%' ORDER BY Bus.Id desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['BusId']=$row['Id']; 
			$data[$cr]['RouteId']=$row['RouteId']; 
			$data[$cr]['BusName']=$row['BusName']; 
			$data[$cr]['BusType']=$row['BusType']; 
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['SeatsCount']=$row['SeatsCount'];
			$data[$cr]['Fare']=$row['fare'];
			$data[$cr]['DepartureTime']=$row['DepartureTime'];
			$data[$cr]['ArrivalTime']=$row['ArrivalTime'];
			$data[$cr]['DepartureStation']=$row['DepartureStation'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation'];
			$cr++;
		}
		return json_encode($data);
	}
}
