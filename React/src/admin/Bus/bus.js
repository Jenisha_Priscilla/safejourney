import React from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col' 
import BusInsert from './busInsert';
import RouteView from '../Route/routeView'

const Bus = () => {
    return (
        <>
            <Row style = {{margin: '10px'}}>
                <Col>
                    <BusInsert/>
                </Col>
                <Col >
                    <RouteView/>
                </Col>
            </Row>
        </>
        )
}

export default Bus
