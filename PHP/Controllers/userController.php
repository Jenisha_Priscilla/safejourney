<?php
class UserController
{
	private $model;
	function __construct($tile)
	{
		$this->model = new $tile;
	}
	  
	public function book()
	{
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid) {
			$postdata =file_get_contents("php://input");
			$request = json_decode($postdata);
			$taskList=$request->taskList;
			require_once "./class/busBookingDetails.php";
			$busBooking = new BusBookingDetails();
			$busBooking -> __set('userId',$request->userId);
			$busBooking -> __set('noOfTickets',$request->ticket);
			$busBooking -> __set('journeyDate',$request->JourneyDate);
			$busBooking -> __set('busId',$request->busid);
			$busBooking -> __set('rate',$request->rate);
			$this->model->busBooking( $busBooking->toArray());
	
			require_once "./class/passenger.php";
			$passenger = new Passenger();
			for ($x = 0; $x < $request->ticket; $x++) {
				$myArray = json_decode(json_encode($taskList[$x]), true); 
				$passenger -> __set('name',$myArray['Name']);
				$passenger -> __set('age',$myArray['Age']);
				$passenger -> __set('seatNo',$myArray['SeatNo']);
				$this->model->passenger( $passenger ->toArray());
			}
		} else {
			return false;
		}
	}

	public function getFare($id)
	{
		return $this->model->getFare($id);
	}

	public function login()
	{
		return $this->model->login();
	}
	
	public function signup()
	{
		$postdata =file_get_contents("php://input");
		$request = json_decode($postdata);
		require_once "./class/user.php";
		$user = new User();
		$user->__set('name',$request->Name);
		$user->__set('password',$request->Password);
		return $this->model->signup($user->toArray());
	}

	public function getSeat($record)
	{
		return $this->model->getSeat($record);	
	}

	public function viewOrder($id)
	{
		return $this->model->viewOrder($id);
	}

	public function listPassenger($id)
	{
		return $this->model->listPassenger($id);
	}

	public function orderCancel()
	{
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid) {
			return $this->model->orderCancel();
		} else {
			return false;
		}
	}

	public function getLog()
	{
		$this->model->getLog();
	}
}

