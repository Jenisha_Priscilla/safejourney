import React from 'react'
import Table from 'react-bootstrap/Table'
import { connect } from 'react-redux';
import Count from './count'
import Record from './record';
import Image from 'react-bootstrap/Image'
import Spinner from '../../spinner/spinner'

const CancelledDetails = (props) => {

    const bookingRecordList = () => {
        return props.cancelledDetails.map (function(object, i){   
            return <Record obj = {object} key ={i} />; 
        });
    }
        let record = props.loading ? <Spinner/> : props.cancelledDetails.length? <Table responsive  hover >
                <thead>
                    <tr>
                        <th> User Name</th>
                        <th> Bus Name </th>
                        <th> Bus Type</th>
                        <th> PlateNo.</th>
                        <th> Departure station </th>
                        <th> Arrival station </th>
                        <th> Journey date</th>
                        <th> Booked date</th>
                        <th> No.of ticket</th>
                    </tr>
                </thead>
                <tbody>
                    {bookingRecordList()}
                </tbody>
            </Table> : <div style ={{textAlign:"center",marginTop:40}}> 
                <Image
                    alt = ""
                    src = {process.env.REACT_APP_API+"/image/record.png"}
                    height = "190"
                    width = "190"
                />  <br/>
               
            </div>
        return (
            <>
                <Count></Count>
                <h5 align = "center">  One day record  </h5>
                        {record}
            </>
        )
    
}
const mapStateToProps = state => {
    return {
        cancelledDetails : state.record.cancelledDetails,
        loading : state.record.loading,
        date : state.record.date
    }
}


export default connect(mapStateToProps, null)(CancelledDetails) 
