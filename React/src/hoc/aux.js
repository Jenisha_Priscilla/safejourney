import React, { useEffect } from "react";
import LoginModal from '../component/loginModal';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/auth'
import { withRouter } from 'react-router-dom';

const Aux = (props) => {
    useEffect(() => {
        var timestamp = Math.floor(new Date().getTime() / 1000)   
                    if(localStorage.getItem('exp') && localStorage.getItem('exp') <= timestamp ) {
                        console.log("jeni")
                        props.authLogout()
                        props.history.push('/login')
                    }
    },[])
    
    if(localStorage.getItem('userData') ) {
        return <div  style = {{marginTop:50}}> {props.children} </div>
    } else {
        return <LoginModal/>   
    }
}

const mapDispatchToProps = dispatch => {
    return {
        authLogout : () => dispatch(actionTypes.authLogout())
    }
}

export default  withRouter(connect(null, mapDispatchToProps)(Aux))

