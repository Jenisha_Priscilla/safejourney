import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Table from 'react-bootstrap/Table'
import RouteList from './routeList'
import * as actionType from '../../store/actions/networkError'
import NetworkError from '../../component/networkError'

import { connect } from 'react-redux';
import { Redirect} from "react-router-dom";
import Spinner from '../../spinner/spinner'

const RouteView  = (props)=> {
    
    const [routeData, setRouteData] = useState([])
    const [search , setSearch] = useState('')
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        if(search.length > 1) {
            searchData()
        } else {
            receivedData()
        }
    }, [props.insertSuccess, props.updateSuccess])
    
   const routeList = () => {
        return routeData.map (function(object, i) {
            return <RouteList obj = {object} key ={i} />;        
      });
    }

    const handleChange = (e) => {
        searchData()
        setSearch(e.target.value)
        if(e.target.value.length == 1) {
            receivedData()
        }
    }

    const receivedData = () => {
        axios.get('/route/view')
        .then(response => {
            setRouteData(response.data)
            setLoading(false)
        })
        .catch(err => props.showNetworkError())
    }

    const searchData = () => {
        axios.get('/route/searchByRoute/'+search)
            .then(response => {
                if(response.data) {
                    setRouteData(response.data)
                }    
            }) 
            .catch(err => props.showNetworkError())
    }

    if(props.show) {
        return <Redirect to = './login'/>
    }

    if(props.showError) {
        console.log("networkError")
        return  <NetworkError/>
    }


    if(loading) {
        return <Spinner/>
    }

        return (
            
            <>
                 <Col md = {{ span: 4, offset: 7 }}>
                        <Form.Control type="text" placeholder = "Search by route" 
                            onKeyDown = {handleChange} 
                        />
                    </Col>
                    <br/>
                <center>
                    
                    <Table responsive hover style = {{width:500}}>
                        <thead>
                            <tr>
                                <th> Id </th>
                                <th> Departure Station</th>
                                <th> Arrival Station </th>
                                <th colSpan = "2"> Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {routeList()}
                        </tbody>
                    </Table> 
                    </center>
            </>
        )
    
}
const mapStateToProps = state => {
    return {
        insertSuccess: state.route.insertSuccess,
        updateSuccess : state.route.updateSuccess,
        show : state.inValidModal.show,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        showNetworkError : () => dispatch(actionType.showNetworkError()), 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RouteView)

