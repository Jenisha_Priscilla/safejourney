import * as actionTypes from './actionsTypes'

export const showNetworkError = () => {
    return {
        type : actionTypes.SHOW_NETWORK_ERROR
    }
}

export const hideNetworkError = () => {
    return {
        type : actionTypes.HIDE_NETWORK_ERROR
    }
}
