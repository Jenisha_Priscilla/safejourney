<?php

class BookingRecordModel 
{
	public $pdo;
	function __construct()
	{
		$db = new Database();
		$this->pdo = $db->__get('pdo');
	//   $this->pdo = $pdo;
	}

	public function confirmedBooking($date)
	{
		$date = $date[0];
		$sql = "select count(BusPassenger.del) as confirmedTicket from BusPassenger inner join BusBookingDetails 
		on BusBookingDetails.Id = BusPassenger.BookId 
		where cast(BusBookingDetails.created_at as date) = '$date' and BusPassenger.del = 0; ";
		$query = $this->pdo->query($sql);
		$row = $query->fetch();
		$data = $row['confirmedTicket'];
		return json_encode($data);
	}

	public function confirmedMonthCount($date)
	{
		$date = $date[0];
		$firstDayOfMonth = date('Y-m-01' , strtotime($date));
		$lastDayOfMonth = date('Y-m-t',  strtotime($date));
		$sql = "select count(BusPassenger.del) as confirmedTicket from BusPassenger inner join BusBookingDetails 
		on BusBookingDetails.Id = BusPassenger.BookId 
		where cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth' and BusPassenger.del = 0;";
		$query = $this->pdo->query($sql);
		$row = $query->fetch();
		$data = $row['confirmedTicket'];
		if( $row['confirmedTicket'] == null) {
			$data = 0;
		}
		return json_encode($data);
	}

	public function cancelledMonthCount($date)
	{
		$date = $date[0];
		$firstDayOfMonth = date('Y-m-01' , strtotime($date));
		$lastDayOfMonth = date('Y-m-t',  strtotime($date));
		$sql = "select count(BusPassenger.del) as cancelledTicket from BusPassenger inner join BusBookingDetails 
		on BusBookingDetails.Id = BusPassenger.BookId 
		where cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth' and BusPassenger.del = 1;";
		$query = $this->pdo->query($sql);
		$row = $query->fetch();
		$data = $row['cancelledTicket'];
		if( $row['cancelledTicket'] == null) {
			$data = 0;
		}
		return json_encode($data);
	}

	public function travelledCount($date)
	{
		$date = $date[0];
		$firstDayOfMonth = date('Y-m-01' , strtotime($date));
		$lastDayOfMonth = date('Y-m-t',  strtotime($date));
		$sql = "select count(BusPassenger.del) as travelledTicket from BusPassenger inner join BusBookingDetails 
		on BusBookingDetails.Id = BusPassenger.BookId 
		where cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth' and BusPassenger.del = 0
        and cast(BusBookingDetails.JourneyDate as date)  <= curdate();";
		$query = $this->pdo->query($sql);
		$row = $query->fetch();
		$data = $row['travelledTicket'];
		if( $row ['travelledTicket'] == null) {
			$data = 0;
		}
		return json_encode($data);
	}

	public function cancelledBooking($date)
	{
		$date = $date[0];
		$sql = "select count(BusPassenger.del) as cancelledTicket from BusPassenger inner join BusBookingDetails 
		on BusBookingDetails.Id = BusPassenger.BookId 
		where cast(BusBookingDetails.created_at as date) = '$date' and BusPassenger.del = 1; ";
		$query = $this->pdo->query($sql);
		$row = $query->fetch();
		$data = $row['cancelledTicket'];
		if( $row['cancelledTicket'] == null) {
			$data = 0;
		}
		return json_encode($data);
	}

	public function cancelledDetails($date) 
	{
		$date = $date[0];
		$data = [];
		$sql = "select UserBus.Name, Bus.BusName, Bus.BusType, Bus.PlateNo,Route.ArrivalStation, Route.DepartureStation,
		BusBookingDetails.JourneyDate, count(BusPassenger.id) as Tickets,
	   BusBookingDetails.created_at as BookedDate
	   FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
	   inner join Route on Bus.RouteId = Route.Id
	   inner join UserBus on UserBus.Id = BusBookingDetails.UserId 
	   inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
	   where cast(BusBookingDetails.created_at as date)  ='$date'  and
	   BusPassenger.del =1  group by BusBookingDetails.Id;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['BusType']=$row['BusType'];
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['Tickets']=$row['Tickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$cr++;
		}
		return json_encode($data);   
	}

	public function confirmedDetails($date) 
	{
		$date = $date[0];
		$data = [];
		$sql = "select UserBus.Name, Bus.BusName, Bus.BusType, Bus.PlateNo,Route.ArrivalStation, Route.DepartureStation,
		BusBookingDetails.JourneyDate, count(BusPassenger.id) as Tickets,
	   BusBookingDetails.created_at as BookedDate
	   FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
	   inner join Route on Bus.RouteId = Route.Id
	   inner join UserBus on UserBus.Id = BusBookingDetails.UserId 
	   inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
	   where cast(BusBookingDetails.created_at as date)  ='$date'  and
	   BusPassenger.del = 0  group by BusBookingDetails.Id;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['BusType']=$row['BusType'];
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['Tickets']=$row['Tickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$cr++;
		}
		return json_encode($data);  
	}

	public function cancelledMonthDetails($date)
	{
		$date = $date[0];
		$firstDayOfMonth = date('Y-m-01' , strtotime($date));
		$lastDayOfMonth = date('Y-m-t',  strtotime($date));
		$sql = "select UserBus.Name, Bus.BusName,Bus.BusType, Bus.PlateNo, Route.ArrivalStation, Route.DepartureStation,
	    BusBookingDetails.JourneyDate, count(BusPassenger.id) as cancelledTickets,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId 
		inner join BusPassenger on BusBookingDetails.Id = BusPassenger.BookId
		where (cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth')
		 and  BusPassenger.del = 1 group by BusBookingDetails.Id;";
		$query = $this->pdo->query($sql); 
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['BusType']=$row['BusType'];
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['Tickets']=$row['cancelledTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$cr++;
		}
		return json_encode($data);  
	}

	public function confirmedMonthDetails($date)
	{
		$date = $date[0];
		$firstDayOfMonth = date('Y-m-01' , strtotime($date));
		$lastDayOfMonth = date('Y-m-t',  strtotime($date));
		$sql= "select UserBus.Name, Bus.BusName,Bus.BusType, Bus.PlateNo, Route.ArrivalStation, Route.DepartureStation,
	    BusBookingDetails.JourneyDate, count(BusPassenger.id) as confirmedTickets,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId 
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where (cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth' )
		 and  BusPassenger.del = 0 group by BusBookingDetails.Id ;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['BusType']=$row['BusType'];
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['Tickets']=$row['confirmedTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$cr++;
		}
		return json_encode($data);  
	}

	public function todayBookingDetails()
	{
		$data = [];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, Route.ArrivalStation, Route.DepartureStation,
		BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate, 
		BusBookingDetails.created_at as BookedDate,if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId 
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		OR cast(BusBookingDetails.JourneyDate as date)  >= curdate() group by BusBookingDetails.Id order by BookedDate desc  ";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	
	}

	
	public function searchUserName($userName)
	{
		$search = $userName[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp() ,'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation, BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where  UserBus.Name like '%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}

	public function searchBusName($busName)
	{
		$search = $busName[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation, BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where  Bus.BusName like'%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc ;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}

	public function searchDepartureStation($departureStation)
	{
		$search = $departureStation[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation, BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where Route.DepartureStation like'%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}
	public function searchArrivalStation($arrivalStation)
	{
		$search = $arrivalStation[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation, BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where Route.ArrivalStation like'%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}
	public function searchConfTicket($confTicket)
	{
		$search = $confTicket[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation, BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where BusBookingDetails.NoOfTickets like'%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}

	public function searchTicket($ticket)
	{
		$search = $ticket[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation,BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where  
		(cast(BusBookingDetails.created_at as date)   between '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id 
        having count(BusPassenger.id) like '%$search%'";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}

	public function searchJourneyDate($journeyDate)
	{
		$search = $journeyDate[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp() ,'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation,BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where  BusBookingDetails.JourneyDate like'%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}
	public function searchBookedDate($bookedDate)
	{
		$search = $bookedDate[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation,BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where cast(BusBookingDetails.created_at as date)  like '%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}
	public function searchStatus($status)
	{
		$search = $status[0];
		$firstDayOfMonth = date('Y-m-01');
		$lastDayOfMonth = date('Y-m-t');
		$sql = "select UserBus.Name, Bus.BusName, if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0 ,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') as Status,
		Route.ArrivalStation, Route.DepartureStation,BusBookingDetails.NoOfTickets  ,count(BusPassenger.id) as Tickets, BusBookingDetails.JourneyDate,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where if(BusBookingDetails.del =0 and BusBookingDetails.NoOfTickets !=0,
		if(concat(BusBookingDetails.JourneyDate,' ',Bus.DepartureTime)< current_timestamp(),'Travelled', 'Confirmed'),'Cancelled') 
		like '%$search%' AND 
		(cast(BusBookingDetails.created_at as date)   between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		or cast(BusBookingDetails.JourneyDate as date) >= curdate()) group by BusBookingDetails.Id order by BookedDate desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Name']=$row['Name'];
			$data[$cr]['BusName']=$row['BusName'];
			$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
			$data[$cr]['DepartureStation']=$row['DepartureStation']; 
			$data[$cr]['NoOfTickets']=$row['NoOfTickets']; 
			$data[$cr]['JourneyDate']=$row['JourneyDate'];
			$data[$cr]['BookedDate']=$row['BookedDate'];
			$data[$cr]['Status']=$row['Status'];
			$data[$cr]['Tickets'] = $row['Tickets'];
			$cr++;
		}
		return json_encode($data);
	}

	public function oneDayRecord($date) 
	{
		$date = $date[0];
		$data = [];
		$sql = "select  distinct Bus.BusName,Bus.BusType,Bus.PlateNo, Route.ArrivalStation, Route.DepartureStation,
		count(if(BusPassenger.del = 0,1,null))'Confirmed',
		count(if(BusPassenger.del = 1,1,null))'Cancelled'
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId 
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where cast(BusBookingDetails.created_at as date)  = '$date' 
		group by Bus.Id;"; 
			$query = $this->pdo->query($sql);
			$cr=0;
			while ($row = $query->fetch()) {
				$data[$cr]['BusName']=$row['BusName'];
				$data[$cr]['BusType']=$row['BusType'];
				$data[$cr]['PlateNo']=$row['PlateNo'];
				$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
				$data[$cr]['DepartureStation']=$row['DepartureStation']; 
				$data[$cr]['Confirmed']=$row['Confirmed']; 
				$data[$cr]['Cancelled']=$row['Cancelled'];
				$cr++;
			}
		return json_encode($data);
	}
	
	public function oneMonthRecord($date) 
	{
		$date = $date[0];
		$data = [];
		$firstDayOfMonth = date('Y-m-01' , strtotime($date));
		$lastDayOfMonth = date('Y-m-t',  strtotime($date));
		$sql = "select Bus.BusType, Bus.BusName,Bus.PlateNo, BusBookingDetails.JourneyDate, Route.ArrivalStation,Route.DepartureStation,count(if(BusPassenger.del = 0,1,null))'Confirmed',count(if(BusPassenger.del = 1,1,null))'Cancelled' FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id inner join Route on Bus.RouteId = Route.Id inner join UserBus on UserBus.Id = BusBookingDetails.UserId inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId where cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth' group by BusBookingDetails.JourneyDate, Bus.Id;";
		$query = $this->pdo->query($sql);
			$cr=0;
			while ($row = $query->fetch()) {
				$data[$cr]['BusName']=$row['BusName'];
				$data[$cr]['BusType']=$row['BusType'];
				$data[$cr]['PlateNo']=$row['PlateNo'];
				$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
				$data[$cr]['DepartureStation']=$row['DepartureStation']; 
				$data[$cr]['Confirmed']=$row['Confirmed']; 
				$data[$cr]['Cancelled']=$row['Cancelled'];
				$cr++;
			}
		return json_encode($data);
	}

	public function travelledDetails($date) 
	{
		$date = $date[0];
		$firstDayOfMonth = date('Y-m-01' , strtotime($date));
		$lastDayOfMonth = date('Y-m-t',  strtotime($date));
		$sql = "select UserBus.Name, Bus.BusName,Bus.BusType, Bus.PlateNo, Route.ArrivalStation, Route.DepartureStation,
	    BusBookingDetails.JourneyDate, count(BusPassenger.id) as Tickets,
		BusBookingDetails.created_at as BookedDate
		FROM Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		inner join UserBus on UserBus.Id = BusBookingDetails.UserId 
		inner join BusPassenger on BusBookingDetails.Id= BusPassenger.BookId
		where (cast(BusBookingDetails.created_at as date)  between  '$firstDayOfMonth' AND '$lastDayOfMonth'
		and cast(BusBookingDetails.JourneyDate as date)  <= curdate() ) and  BusPassenger.del = 0 group by BusBookingDetails.Id ;";
		$query = $this->pdo->query($sql);
			$cr=0;
			while ($row = $query->fetch()) {
				$data[$cr]['Name']=$row['Name'];
				$data[$cr]['BusName']=$row['BusName'];
				$data[$cr]['BusType']=$row['BusType'];
				$data[$cr]['PlateNo']=$row['PlateNo'];
				$data[$cr]['ArrivalStation']=$row['ArrivalStation']; 
				$data[$cr]['DepartureStation']=$row['DepartureStation']; 
				$data[$cr]['Tickets']=$row['Tickets']; 
				$data[$cr]['JourneyDate']=$row['JourneyDate'];
				$data[$cr]['BookedDate']=$row['BookedDate'];
				$cr++;
			}
		return json_encode($data);
	}

}