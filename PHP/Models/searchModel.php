<?php

class SearchModel 
{
	public $pdo;
  	function __construct()
  	{
		// $this->pdo = $pdo;
		$db = new Database();
		$this->pdo = $db->__get('pdo');
  	}

	public function index()
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$DepartureStation = $request->DepartureStation;
		$ArrivalStation = $request->ArrivalStation;
		$JourneyDate = $request->JourneyDate;
		$data = [];
		$stmt = $this->pdo->prepare( "SELECT Id FROM Route WHERE DepartureStation = ? AND ArrivalStation = ? ");
		$stmt->execute([$DepartureStation, $ArrivalStation]);
		$row = $stmt->fetch();
		$id = $row['Id'];
		$sql = "select Bus.Id, Bus.DepartureTime, Bus.ArrivalTime, Bus.BusName, Bus.BusType, Bus.PlateNo, Bus.Fare, 
		max(Bus.SeatsCount)-sum(coalesce(BusBookingDetails.NoOfTickets,0)) as rem 
		from Bus 
		left join BusBookingDetails
		on Bus.Id = BusBookingDetails.BusId AND BusBookingDetails.JourneyDate = '$JourneyDate' 
		AND BusBookingDetails.del = '0' 
		where Bus.RouteId= '$id' and Bus.del = '0' AND concat('$JourneyDate',' ',Bus.DepartureTime)> current_timestamp()+interval 5 minute
		group by Bus.Id;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['Id']=$row['Id'];
			$data[$cr]['DepartureTime']=$row['DepartureTime'];
			$data[$cr]['ArrivalTime']=$row['ArrivalTime'];
			$data[$cr]['BusName']=$row['BusName']; 
			$data[$cr]['BusType']=$row['BusType']; 
			$data[$cr]['PlateNo']=$row['PlateNo'];
			$data[$cr]['Fare']=$row['Fare'];
			$data[$cr]['rem']=$row['rem'];
			$cr++;
		}
		return json_encode($data);
	}

	public function destinationStation()
	{
		$sql = "SELECT distinct DepartureStation FROM Route where del =0;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['DepartureStation']=$row['DepartureStation'];
			$cr++;
		}
		return json_encode($data);
	}

	public function arrivalStation()
	{
		$sql = "SELECT distinct ArrivalStation FROM Route where del =0;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {
			$data[$cr]['ArrivalStation'] = $row['ArrivalStation'];
			$cr++;
		}
		return json_encode($data);
	}

	public function applyFilter() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$DepartureStation = $request->DepartureStation;
		$ArrivalStation = $request->ArrivalStation;
		$JourneyDate = $request->JourneyDate;
		$BusType = implode(",",$request->BusType);
		$DepartureTime = $request->DepartureTime;
		$mrngStartTime = '00:00';
		$mrngEndTime = '00:00';
		$noonStartTime = '00:00';
		$noonEndTime = '00:00';
		$nightStartTime = '00:00';
		$nightEndTime = '00:00';
		for($i = 0; $i < count($DepartureTime);$i++){
		
			if($DepartureTime[$i] == 'morning') {
				$mrngStartTime = '00:01';
				$mrngEndTime = '11:59';
			}
			if($DepartureTime[$i] == 'noon') {
				$noonStartTime = '12:00';
				$noonEndTime = '18:00';
			}
			if($DepartureTime[$i] == 'night') {
				$nightStartTime = '18:01';
				$nightEndTime = '23:59';
			}
		}
		$stmt = $this->pdo->prepare( "SELECT Id FROM Route WHERE DepartureStation = ? AND ArrivalStation = ? ");
		$stmt->execute([$DepartureStation, $ArrivalStation]);
		$row = $stmt->fetch();
		$id = $row['Id'];
		if($BusType && $DepartureTime){
			$sql= "select Bus.Id, Bus.DepartureTime, Bus.BusName,Bus.ArrivalTime, Bus.BusType, Bus.PlateNo, Bus.Fare, 
			max(Bus.SeatsCount)-sum(coalesce(BusBookingDetails.NoOfTickets,0)) as rem 
			from Bus 
			left join BusBookingDetails
			on Bus.Id = BusBookingDetails.BusId AND BusBookingDetails.JourneyDate = '$JourneyDate' 
			AND BusBookingDetails.del = '0' 
			where Bus.RouteId= '$id' and Bus.del = '0' AND 
			concat('$JourneyDate' ,' ',Bus.DepartureTime)> current_timestamp()+interval 5 minute
			and Bus.BusType IN ($BusType) and (Bus.DepartureTime between '$mrngStartTime' and '$mrngEndTime' or 
            Bus.DepartureTime between '$noonStartTime' and '$noonEndTime' or Bus.DepartureTime between '$nightStartTime' and '$nightEndTime')
			group by Bus.Id;";

		}
		if(empty($BusType)  && $DepartureTime){
			$sql= "select Bus.Id, Bus.DepartureTime, Bus.BusName, Bus.ArrivalTime,Bus.BusType, Bus.PlateNo, Bus.Fare, 
			max(Bus.SeatsCount)-sum(coalesce(BusBookingDetails.NoOfTickets,0)) as rem 
			from Bus 
			left join BusBookingDetails
			on Bus.Id = BusBookingDetails.BusId AND BusBookingDetails.JourneyDate = '$JourneyDate' 
			AND BusBookingDetails.del = '0' 
			where Bus.RouteId= '$id' and Bus.del = '0' AND 
			concat('$JourneyDate' ,' ',Bus.DepartureTime)> current_timestamp()+interval 5 minute
			and (Bus.DepartureTime between '$mrngStartTime' and '$mrngEndTime' or 
            Bus.DepartureTime between '$noonStartTime' and '$noonEndTime' or Bus.DepartureTime between '$nightStartTime' and '$nightEndTime')
			group by Bus.Id;";
		
		}
		if($BusType && empty($DepartureTime)){
			$sql= "select Bus.Id, Bus.DepartureTime, Bus.BusName, Bus.ArrivalTime, Bus.BusType, Bus.PlateNo, Bus.Fare, 
			max(Bus.SeatsCount)-sum(coalesce(BusBookingDetails.NoOfTickets,0)) as rem 
			from Bus 
			left join BusBookingDetails
			on Bus.Id = BusBookingDetails.BusId AND BusBookingDetails.JourneyDate = '$JourneyDate' 
			AND BusBookingDetails.del = '0' 
			where Bus.RouteId= '$id' and Bus.del = '0' AND 
			concat('$JourneyDate' ,' ',Bus.DepartureTime)> current_timestamp()+interval 5 minute
			and Bus.BusType IN ($BusType) 
			group by Bus.Id;";
		}
		if($sql) {
			$query = $this->pdo->query($sql);
			$cr=0;
			while ($row = $query->fetch()) {
				$data[$cr]['Id']=$row['Id'];
				$data[$cr]['DepartureTime']=$row['DepartureTime'];
				$data[$cr]['ArrivalTime']=$row['ArrivalTime'];
				$data[$cr]['BusName']=$row['BusName']; 
				$data[$cr]['BusType']=$row['BusType']; 
				$data[$cr]['PlateNo']=$row['PlateNo'];
				$data[$cr]['Fare']=$row['Fare'];
				$data[$cr]['rem']=$row['rem'];
				$cr++;
		}
		}
		
		return json_encode($data);
	}

}
