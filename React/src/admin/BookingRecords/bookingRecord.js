import React from 'react'

const BookingRecord = (props) => 
{ 
   const date = (d) => {
        var date = new Date(d);
        var ddd = date.getDate();
        var mmm = date.getMonth()+1; 
        var yyy = date.getFullYear();
        if (ddd<10) {
            ddd = '0'+ddd;
        } 
        
        if (mmm<10) {
            mmm ='0'+ mmm;
        }   
        return date = ddd+'/'+mmm+'/'+yyy;
    }

    const status = (status) => {
        if (status === 'Cancelled') {
            return (
                <p style = {{ color: "red"}}>
                    {status}
                </p> 
            )
        }
        if (status === 'Confirmed') {
            return (
                <p style = {{ color: "green"}}>
                    {status}
                </p> )
        }
        return (
            <p style = {{ color: "blue"}}>
                {status}
            </p>) 
    }

        return (
            <tr>
                <td>
                    {props.obj.Name}
                </td>
                <td>
                    {props.obj.BusName}
                </td>
                <td>
                    {props.obj.DepartureStation}
                </td>
                <td>
                    {props.obj.ArrivalStation}
                </td>
                <td>
                    {props.obj.Tickets}
                </td>
                <td>
                    {props.obj.NoOfTickets}
                </td>
                
                <td>
                    {date(props.obj.JourneyDate)}
                </td>
                <td>
                    {date(props.obj.BookedDate)}
                </td>
                <td>
                    {status(props.obj.Status)}
                </td>
            </tr>
        )
   
}

export default BookingRecord;



