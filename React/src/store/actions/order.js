import * as actionTypes from './actionsTypes'
import axios from 'axios';
import { authLogout } from './auth';
import { showNetworkError } from './networkError';

export const getOrders = (userId,currentPage) => {
    return dispatch => {
        axios.get('/user/viewOrder/'+ userId+'/'+currentPage,{
        })
        .then(response => {
            console.log(response.data)
            dispatch(setOrders(response.data.data))
            dispatch(setCount(response.data.count))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
} 

export const setCount = (count) => {
    return { 
        type : actionTypes.SET_COUNT,
        count : count
    }
}

export const setOrders = (orders) => {
    return {
        type : actionTypes.SET_ORDERS,
        orders : orders
    }
}
export const getOrder = (bookId) => {

    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/user/listPassenger/'+bookId)
        .then(response => {
            dispatch(setOrder(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const fetchRequest = () => {
    return {
        type : actionTypes.FETCH_REQUEST
    }
}

export const setOrder = (order) => {
    return {
        type : actionTypes.SET_ORDER,
        order : order
    }
}

export const cancelOrder = (obj, bookId) => {
    return dispatch => {
        axios.post('/user/orderCancel', obj,
        // {
        //     headers : {
        //         'AUTHENTICATION' : localStorage.getItem('token') 
        //     }
        // }
        )

        .then((response) => {
            if(response.data != false){
                dispatch(getOrder(bookId))
                dispatch(orderCancelSuccess())
                localStorage.removeItem('url')
            } else {
                dispatch(authLogout()); 
            }
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const orderCancelSuccess = () => {
    return {
        type : actionTypes.CANCEL_ORDER_SUCCESS
    }
}

export const pagination = (selectedPage) => {
    return {
        type : actionTypes.PAGINATION,
        selectedPage : selectedPage
    }
}