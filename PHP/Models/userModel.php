<?php

class UserModel 
{
	public $pdo;

	function __construct()
	{
	//   $this->pdo = $pdo;
		$db = new Database();
		$this->pdo = $db->__get('pdo');
	}
	  
	public function getFare($id) 
	{
		$id = $id[0];
		$sql2 = 'SELECT * FROM Bus WHERE Id ='.$id;
		$query = $this->pdo->query($sql2);
		$cr = 0;
		while ($row = $query->fetch()) {	
			$data = $row['Fare'];
		}
		return json_encode($data);
	}

	

	public function signup($data)
	{
		$Name = $data['name'];
		$Password = $data['password'] ;
		$sql = "INSERT INTO UserBus(RoleId, Name, Password ) 
		VALUES 
			('2', '$Name', '$Password')";
		$query = $this->pdo->query($sql);
		$sql1 = "select row_count()";
		$query = $this->pdo->query($sql1);
		$row = $query->fetch();
		$count = $row['row_count()'] ;
		$stmt = $this->pdo->prepare("SELECT Id FROM UserBus WHERE Name = ?  AND Password = ?");
		$stmt->execute([$Name, $Password]);
		$row = $stmt->fetch();
		$id = $row['Id'];
		require_once './auth/generateToken.php';
		$payload = json_encode(['count' => $count, 'user_id' => $id ,'exp' => time()+(60*60)]);
		$data = generateToken($payload);
		return json_encode($data);
	}

	public function login()
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$Name = $request->Name;	
		$Password = $request->Password ;
		$stmt = $this->pdo->prepare("SELECT userBus.Id, Role.Role, userBus.Name 
		from userBus inner join Role on userBus.RoleId = Role.Id 
		WHERE Name = ?  AND Password = ?");
		$stmt->execute([$Name, $Password]);
		$row = $stmt->fetch();
		$id = $row['Id'];
		$role = $row['Role'];
		$name = $row['Name'];
		require_once './auth/generateToken.php';
		$payload = json_encode(['user_id' => $id, 'role' => $role, 'name' => $name, 'exp' => time()+(60*60)]);
		$data = generateToken($payload);
		return json_encode($data);	
	}

	public function busBooking($data)
	{
		$userId = $data['userId'];
		$ticket = $data['noOfTickets'];
		$date = $data['journeyDate'];
		$busId = $data['busId'];
		$rate = $data['rate'];
		
		$sql = "INSERT INTO BusBookingDetails (UserId, BusId, NoOfTickets, JourneyDate,amount) 
		VALUES 
			('$userId','$busId', '$ticket','$date','$rate')";
		$query = $this->pdo->query($sql);
		
	}

	public function passenger($data)
	{
		$passengerName = $data["name"];
		$passengerAge = $data["age"];
		$passengerSeat = $data["seatNo"];
		$sql1 = "SELECT Id from BusBookingDetails order by Id desc limit 1";
		$query = $this->pdo->query($sql1);
		$row = $query->fetch();
		$bookId = $row['Id'];
		$sql2 = "INSERT INTO BusPassenger (BookId, Name , Age, SeatNo) 
		VALUES 
			('$bookId', '$passengerName', '$passengerAge', '$passengerSeat')";
		$query = $this->pdo->query($sql2);
	
	}

	public function getSeat($record)
	{
		$data = [];
		$id = $record[0];
		$yy = $record[1]; 
		$mm = $record[2];
		$dd = $record[3];
		$date = $yy.'-'.$mm.'-'.$dd;
		$sql = "select BusPassenger.SeatNo
		from BusBookingDetails
		inner join BusPassenger on BusPassenger.BookId = BusBookingDetails.Id
		where BusId = '$id' AND JourneyDate = '$date' AND BusBookingDetails.del = 0 
		AND BusBookingDetails.NoOfTickets != 0  AND BusPassenger.del =0;";
		$query = $this->pdo->query($sql);
		$cr = 0;
		while ($row = $query->fetch()) {		
			$data[$cr]['seat'] = $row['SeatNo'];
			$cr++;
		}
		$recur_flat_arr_obj = new RecursiveIteratorIterator(new RecursiveArrayIterator($data));
		$flat_arr = iterator_to_array($recur_flat_arr_obj, false);
		$sql1  = "select SeatsCount from Bus where Id = '$id'";
		$query = $this->pdo->query($sql1);
		while ($row = $query->fetch()) {	
			$SeatsCount = $row['SeatsCount'];	
		}
		$cc = 0;
		for ($i=1; $i<= $SeatsCount; $i++) {
			$seats[$cc]['seat'] = $i;
			$cc++;
		}
		
		for ($i=0; $i<count($data); $i++) {
		$seats[$data[$i]['seat']-1]['seat'] = 'Booked';
		}
		return json_encode($seats);
	}

	public function viewOrder($arg) {
		$data = [];
		$id = $arg['0'];
		$currentPage = $arg['1'];
		$perPage = 7;
		$offset = $currentPage * $perPage - $perPage;
		
		$sql1= "select count(*) as count
		from  Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		where BusBookingDetails.UserId = '$id'";
		$query = $this->pdo->query($sql1);
		$row = $query->fetch();
		$count = $row['count'];

		$sql = "select BusBookingDetails.Id,Bus.DepartureTime,Bus.ArrivalTime, Route.DepartureStation, Route.ArrivalStation, 
		Bus.BusName,Bus.BusType, Bus.PlateNo,BusBookingDetails.del, BusBookingDetails.JourneyDate, 
		BusBookingDetails.NoOfTickets 
		from  Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		where BusBookingDetails.UserId = '$id' order by BusBookingDetails.JourneyDate desc LIMIT $offset,$perPage;";
		$query = $this->pdo->query($sql);
		$cr = 0;
		while ($row = $query->fetch()) {
			$data[$cr]['Id'] = $row['Id'];
			$data[$cr]['del'] = $row['del'];
			$data[$cr]['BusName'] = $row['BusName'];
			$data[$cr]['BusType'] = $row['BusType'];
			$data[$cr]['PlateNo'] = $row['PlateNo'];
			$data[$cr]['DepartureTime'] = $row['DepartureTime'];
			$data[$cr]['ArrivalTime'] = $row['ArrivalTime'];
			$data[$cr]['JourneyDate'] = $row['JourneyDate'];
			$data[$cr]['NoOfTickets'] = $row['NoOfTickets'];
			$data[$cr]['DepartureStation'] = $row['DepartureStation'];
			$data[$cr]['ArrivalStation'] = $row['ArrivalStation'];
			$cr++;
		}
		
		return json_encode(['data'=>$data, 'count'=>$count/$perPage ]);
	}

	
	public function listPassenger($id) 
	{
		$id = $id[0];
		$sql = "select BusPassenger.BookId,BusPassenger.Name,BusPassenger.Age,BusPassenger.SeatNo,
		BusPassenger.id,BusPassenger.del,BusBookingDetails.amount,BusBookingDetails.NoOfTickets
		from BusPassenger inner join BusBookingDetails on BusBookingDetails.Id= BusPassenger.BookId
		where BusBookingDetails.Id = $id;";
		$query = $this->pdo->query($sql);
		$cr = 0;
		while ($row = $query->fetch()) {
			$data[$cr]['id'] = $row['id'];
			$data[$cr]['BookId'] = $row['BookId'];
			$data[$cr]['Name'] = $row['Name'];
			$data[$cr]['Age'] = $row['Age'];
			$data[$cr]['SeatNo'] = $row['SeatNo'];
			$data[$cr]['del'] = $row['del'];
			$data[$cr]['amount'] = $row['amount'];
			$data[$cr]['NoOfTickets'] = $row['NoOfTickets'];
			$cr++;
		}
		return json_encode($data);
	}

	public function orderCancel() 
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$id = implode(",",$request->id);
		$noOfTicket = count($request->id);
		$bookId = $request->bookId;
		$sql = "update BusPassenger set del = 1 where id in($id);";
		$query = $this->pdo->query($sql);
		$sql1 = "update BusBookingDetails set NoOfTickets = NoOfTickets-$noOfTicket where id = $bookId;";
		$query = $this->pdo->query($sql1);
		return true;
	}

	public function getLog()
	{
		$file = './log/log.txt';
		if (file_exists($file)) {
			print_r(json_encode(file($file)));
			exit;
		}
	}
}

