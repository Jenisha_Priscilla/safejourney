import * as actionTypes from '../actions/actionsTypes'

const initialState = {
    order : [],
    amount : '',
    noOfTicket : '',
    success :false,
    loading : true,
    perPage: 7,        
    currentPage: 1,
};

const orderReducer = (state = initialState, action) => {
    switch(action.type) {
        
        case actionTypes.FETCH_REQUEST:
            return {
                ...state,
                loading : true
            }

        case actionTypes.CANCEL_ORDER_SUCCESS:
            return {
                ...state,
                success : true
            }
            
        case actionTypes.SET_COUNT:
            return {
                ...state,
                pageCount : action.count
            }

        case actionTypes.SET_ORDERS:    
            return {
                ...state,
                order : action.orders,
                redirect : true,
                loading : false
            }
        
        case actionTypes.SET_ORDER:
            return {
                ...state,
                order : action.order,
                amount : action.order['0']['amount'],
                noOfTicket : action.order['0']['NoOfTickets'],
                loading : false
              }

        case actionTypes.PAGINATION:
            return {
                ...state,
                currentPage : action.selectedPage
            }
            
        default : 
            return state;
    }
}
export default orderReducer;