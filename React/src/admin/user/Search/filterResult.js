import React, { useEffect } from 'react'
import SearchRecord from './searchRecord'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/search'
import * as actionType from '../../../store/actions/auth'
import FilterRecord from './filterRecord'
import SearchDetail from './searchDetail'
import Spinner from '../../../spinner/spinner'
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import {Redirect} from 'react-router-dom';
import NetworkError from '../../../component/networkError'

const FilterResult = (props) => { 
 
    useEffect(() => {
        localStorage.removeItem('url')
    },[])

    const searchList = () => {
        return props.filterResult.map (function(object, i){
            return <SearchRecord obj = {object} key ={i} />;   
        });
    }

        let filterClass = []
        if(props.stateLost === '1'){
            return <Redirect to = '/'/>
        }

        if(props.showError) {
            return <NetworkError/>  
        }
        
        if(props.loading) {
            return <Spinner  />
        }
       
        if(props.filterOpen){
            filterClass = "d-lg-none d-md-block"
        } else {
            filterClass = "d-none "
        }

       let record = (
           props.filterResult!= null ? 
           <Card>
                {searchList()}
           </Card>  :
             <div style = {{textAlign:"center", marginTop:50}}> 
                <Image  
                    height = "200"
                    width = "200" 
                    src = {process.env.REACT_APP_API+"/image/clearFilter.png"}
                />
            <br/> <br/>
            <div style = {{fontSize : 15, fontWeight : 'bold'}}> Too many filter applied, Please clear filter </div> 
                <br/>
                               
           </div>
        )
        
        return ( 
            <Col style = {{marginTop:15}}>
                <SearchDetail></SearchDetail>
               
                <div className = "d-lg-none d-md-block p-3 m-auto"  onClick={() => props.filterStatus()}><FontAwesomeIcon icon={ faFilter }/> {' '}Filter </div> 
                        <div className = {filterClass}>
                            <FilterRecord />
                        </div>
                <div style = {{marginTop:20}}>
                    <Row>
                        <Col sm = {3} className = "d-none d-lg-block">
                            <FilterRecord ></FilterRecord>
                        </Col>
                        <Col>
                            {record}
                        </Col>
                    </Row> 
                </div>     
            </Col>
        ) 
}

const mapStateToProps = state => {
    console.log(state)
    return {
        filterResult : state.search.filterResult,
        loading: state.search.loading,
        filterOpen : state.search.filterOpen,
        stateLost : state.search.stateLost,
        showError : state.networkError.showNetworkError
    }
}
const mapDispatchToProps = dispatch => { 
    return {
        searchSuccess : (searchData) => dispatch(actionTypes.searchSuccess(searchData)),
        removeAllFilter : () => dispatch(actionTypes.removeAllFilter()),
        filterStatus : () => dispatch(actionTypes.filterOpen()),
        authLogout : () => dispatch(actionType.authLogout())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FilterResult);

