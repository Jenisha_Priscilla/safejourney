import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/index';
import InValidModal from '../../../component/inValidModal'
import Image from 'react-bootstrap/Image'
import Card from 'react-bootstrap/Card'
import * as actionType from '../../../store/actions/networkError'
import NetworkError from '../../../component/networkError';
import {Link} from 'react-router-dom'

class Login extends Component 
{
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            userNameError: '',
            fieldError: ''
        };
    this.onSubmit = this.onSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({[e.target.name]:e.target.value})
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.state.username && this.state.password) {
            this.setState ({fieldError : ""})
            const obj= {
                Name:this.state.username,
                Password:this.state.password
            };
            axios.post('/user/login',obj)
            .then(response => {
                console.log(response.data)
                if (response.data.payload.user_id != null) {
                    localStorage.setItem('userData',response.data.payload.user_id);
                    localStorage.setItem('exp', response.data.payload.exp);
                    localStorage.setItem('role', response.data.payload.role);
                    localStorage.setItem('token', response.data.token);
                    // localStorage.setItem('Token', JSON.stringify(response.data.token));
                    localStorage.setItem('name',response.data.payload.name)
                    this.props.authSuccess(response.data.payload.user_id);
                    
                }
                else {
                    this.setState({ userNameError: "Type username or password correctly" })
                }
            })
            .catch(err => this.props.showNetworkError())
        } 
        else {
        this.setState({ fieldError: "Must fill all the feilds" })
        } 
    }

   

    render() {
        const { from } = this.props.location.state || { from: { pathname: '/' } }
     let login = (
        <React.Fragment>
            <Row  style = {{justifyContent: "center"}}>
                <Col md = 'auto' className = "d-none d-lg-block p-0" style = {{ marginTop:15}}>
                  <Image  
                       width = '250'
                       height = '550'
                        src = {process.env.REACT_APP_API+"/image/1598770901744.jpg"}
                    />
                </Col>
            <Col sm = {5} className = "d-lg-block p-lg-0 m-lg-0 m-sm-auto w-sm-auto">  
            <h4 style = {{marginTop:80,  textAlign: "center"}}> Login </h4> 
            <br/>     
              
                    <Card  border="primary">
                        <Card.Body>
                        <br/>
                        <Form>
                            <Form.Row>
                                <Form.Label column lg = {2}> Name </Form.Label>
                                <Col>
                                    <Form.Control type = "text" placeholder = "Username" name = "username"
                                        onChange = {this.handleChange} 
                                    />
                                </Col>
                            </Form.Row>
                            <br/>
                            <Form.Row>
                                <Form.Label column lg = {2}> Password </Form.Label>
                                    <Col>
                                        <Form.Control type = "password" placeholder = "Password" 
                                            name = "password" onChange ={this.handleChange}
                                        />
                                    </Col>
                            </Form.Row>

                            <div style = {{fontSize:12, color:"red"}}>
                                {this.state.userNameError}
                            </div> 
    
                            <div style ={{ fontSize:12, color:"red"}}>
                                {this.state.fieldError}
                            </div> 

                            <br/>

                            <Button variant = "dark" onClick = {this.onSubmit} > Login </Button>
                        </Form>

                        <p> Need an account ?  <Link to = {{
                                                    pathname: '/signup',
                                                    state: { from: this.props.location.state }
                                                    }} className = 'p-2'> Sign up</Link> </p>
                            
                        </Card.Body>
                    </Card>
            
            </Col>
            <Col  md = 'auto' className = "d-none d-lg-block p-0" style = {{marginTop:15}}>
                <Image  
                        width = '250'
                        height = '550'
                        src = {process.env.REACT_APP_API+"/image/1598770901752.jpg"}
                />
            </Col>
        </Row>
        </React.Fragment>
     
        ) 
        if(this.props.show) {
            login = < InValidModal/>
        }
        if (localStorage.getItem('userData')) { 
             login = <Redirect to =  '/' />
            if (this.props.location.state) {
                login = (<Redirect to=  {this.props.location.state} />)
            } 
        } 
        if(this.props.showError) {
            console.log("networkError")
            login = <NetworkError/>
        }
        
        return login;
    }
}
const mapStateToProps = state => {
    return {
        show : state.inValidModal.show,
        showError : state.networkError.showNetworkError
    }
}


const mapDispatchToProps = dispatch => {
    return {
        authSuccess : (userId) => dispatch(actionTypes.authSuccess(userId)), 
        showNetworkError : () => dispatch(actionType.showNetworkError()),
    }
}

export default connect( mapStateToProps , mapDispatchToProps)(Login)