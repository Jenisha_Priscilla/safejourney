import React, {Component} from 'react';
import Modal from 'react-bootstrap/Modal'
import {Link, Redirect} from 'react-router-dom'
import { withRouter} from "react-router-dom";

class LoginModal extends Component 
{
  constructor() {
    super()
    this.state = {
      show: true
    }
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.setState({ show: false});
    this.props.history.push('/login')
  }

  render() {
    console.log(this.props.location.state.from.pathname )
    console.log(this.props)
    
      return (
        <Modal show = {this.state.show} onHide = {this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Not logged in</Modal.Title>
          </Modal.Header>
          <Modal.Body> Looks like you were not logged in. We are redirecting you to Login page </Modal.Body>
          <Modal.Footer>
            <Link to = {{
            pathname: '/login',
            state: this.props.location.state.from.pathname
          }} style = {{textDecoration: 'none'}}> Okay </Link>
          </Modal.Footer>
        </Modal>
      )
    
     
  }
}

export default withRouter(LoginModal)