import * as actionTypes from './actionsTypes'
import axios from 'axios';
import { showNetworkError } from './networkError';

export const handleChange = (event) => {
    return {
        type: actionTypes.HANDLE_CHANGE,
        event : event
    }
};

export const searchSuccess = (searchData) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.post('/search/',searchData)
            .then(response => {
                dispatch(searchResult(response.data))
            })
            .catch(err =>  dispatch(showNetworkError()))
    }
}


export const searchResult = (searchResult) => {
    return {
        type : actionTypes.SEARCH_RESULT,
        searchResult : searchResult
    }
}

export const noResult = () => {
    return {
        type: actionTypes.NO_RESULT
    }
}

export const applyFilter = (obj) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.post('/search/applyFilter',obj)
        .then(response => {
            dispatch(filterSuccess(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const filterSuccess = (filterData) => {
    return{
        type: actionTypes.FILTER_SUCCESS,
        filterData : filterData
    }
}

export const filterType = (value) => {
    return {
        type : actionTypes.FILTER_TYPE,
        value :value
    }
}

export const filterRemoveType = (value) => {
    return {
        type : actionTypes.FILTER_REMOVE_TYPE,
        value : value
    }
}

export const filterTime = (value) => {
    return {
        type : actionTypes.FILTER_TIME,
        value : value
    }
}

export const filterRemoveTime = (value) => {
    return {
        type : actionTypes.FILTER_REMOVE_TIME,
        value : value
    }
}

export const removeAllFilter = () => {
    return {
        type:actionTypes.REMOVE_ALL_FILTER
    }
}

export const filterOpen = () => {
    return {
        type : actionTypes.FILTER_OPEN
    }
}

export const pagination = (selectedPage) => {
    return {
        type : actionTypes.SEARCH_PAGINATION,
        selectedPage : selectedPage
    }
}

export const fetchRequest = () => {
    return {
        type : actionTypes.FETCH_REQUEST
    }
}