import React, {useEffect } from 'react';
import OneDayRecordResult from './oneDayRecordResult'
import Table from 'react-bootstrap/Table'
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/record'
import Pagination from "react-js-pagination";
import Image from 'react-bootstrap/Image'
import Count from './count'
import Spinner from '../../spinner/spinner'

const OneDayRecord = (props) => {

    useEffect(() => {
       receivedData()
        props.setCurrentPage()
    },[])
    
    const receivedData = () => {
        props.getOneDayRecord(props.date);
    }

    const handleClick = (e) => {
        props.pagination(e.selected)
        receivedData()
    }

    const bookingRecordList = () => {
        return props.oneDayRecord.map (function(object, i){   
            return <OneDayRecordResult obj = {object} key ={i} />; 
        });
    }

        let record =  props.loading ? <Spinner/> : props.oneDayRecord.length?  
        
                <Table responsive hover  >
                <thead>
                    <tr>
                        <th> Bus Name </th>
                        <th> Bus Type</th>
                        <th> PlateNo.</th>
                        <th> Departure station </th>
                        <th> Arrival station </th>
                        <th> Confirmed tkts </th>
                        <th> Cancelled tkts </th>
                    </tr>
                </thead>
                <tbody>
                    {bookingRecordList()}
                </tbody>
            </Table> 
            : <div style ={{textAlign : "center",marginTop : 40}}>
                        <Image
                            alt = ""
                            src = {process.env.REACT_APP_API+"/image/record.png"}
                            height = "190"
                            width = "190"
                        />  <br/><br/>
                     
                    </div>
            return (
                <>
                    <Count></Count>
                        <h5 align = "center"> One day record  </h5>
                        {record}
                        <Pagination
                            activePage={props.currentPage}
                            pageRangeDisplayed={5}
                            itemsCountPerPage={1}
                            totalItemsCount={props.pageCount}
                            onChange={handleClick}
                            disabledClass = 'pagination--disabled'
                        />
                </>
        )
}
const mapStateToProps = state => {
    return {
        oneDayRecord : state.record.dayRecord,
        date : state.record.date,
        loading : state.record.loading,
        pageCount : state.record.pageCount
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getOneDayRecord  : (date) => dispatch(actionTypes.getOneDayRecord(date)),
        setCurrentPage : () => dispatch(actionTypes.setCurrentPage()),
        pagination : (selectedPage) => dispatch(actionTypes.pagination(selectedPage)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OneDayRecord) 
