import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import React, { Component } from 'react';
import AdminNav from './component/adminNav'
import BusInsert from './admin/Bus/bus'
import Search  from './admin/user/Search/search'
import BusView from './admin/Bus/busView';
import Login from './admin/user/Authentication/login';
import Logout from './admin/user/Authentication/logout';
import SignUp from './admin/user/Authentication/signUp';
import Book  from './admin/user/Book/bookForm'
import Order from './admin/user/Order/order'
import Navg from './component/userNav'
import TodayBooking from './admin/BookingRecords/todayBooking';
import CancelOrder from './admin/user/Order/cancelOrder';
import Layout from './Layout/layout';
import RouteView from './admin/Route/route';
import FilterResult from './admin/user/Search/filterResult'
import SearchResult from './admin/user/Search/searchResult'
import { connect } from 'react-redux';
import filterResult from './admin/user/Search/filterResult';
import OneDayRecord from './admin/oneDayRecord/oneDayRecord';
import CancelledDetails from './admin/oneDayRecord/cancelledDetails';
import ConfirmedDetails from './admin/oneDayRecord/confirmedDetails';
import InValidModal from './component/inValidModal'
import oneMonthRecord from './admin/oneMonthRecord/oneMonthRecord';
import ConfirmedMonthDetails from './admin/oneMonthRecord/confirmedMonthDetails';
import CancelledMonthDetails from './admin/oneMonthRecord/cancelledMonthDetails'
import TravelledDetails from './admin/oneMonthRecord/travelledMonthDetails';
import LoginModal from './component/loginModal'
import PrivateRoute from './ProtectedRoute/privateRoute';
import order from './admin/user/Order/order';
import NotFound from './component/notFound';
import LogInfo from './admin/logs/logInfo';

class App extends Component 
{
	render() {
	let route = (
		<Switch>
			<Route  path = '/searchResult' component = {SearchResult}/>
			<PrivateRoute path = '/order' component = {order} />  
			<PrivateRoute path = '/book/:id/:DepartureTime' component = {Book} />   
			<Route  path = '/login' component = {Login}/>
			<Route  path = '/signup' component = {SignUp}/>
			<Route  path = '/logout' component = {Logout}/>
			<Route  path = '/filterResult' component = {filterResult}/>
			<Route  path = '/loginModal' component = {LoginModal}/>
			<PrivateRoute  path = '/cancelOrder/:id/:journeyDate/:departureTime/:cancel' component = {CancelOrder} />   
			<Route exact path = '/' component = {Search}/>  	
			<Route path = '/' component = {NotFound}/>  	
		</Switch>
	);
	let navBar = <Navg/>

	if(localStorage.getItem('role') == "Admin") { 
		navBar = <AdminNav/>
		route = (
			<Switch>
				<Route path = '/search' component = {Search}/>
				<Route path = '/searchResult' component = {SearchResult}/>   
				<Route path = '/filterResult' component = {FilterResult}/>
				<PrivateRoute path = '/cancelledDetails' component = {CancelledDetails} />  
				<PrivateRoute path = '/confirmedDetails' component = {ConfirmedDetails} /> 
				<PrivateRoute path = '/cancelledMonthDetails'component = {CancelledMonthDetails} />  
				<PrivateRoute path = '/confirmedMonthDetails' component = {ConfirmedMonthDetails} />  
				<PrivateRoute path = '/travelledDetails'component = {TravelledDetails} />  
				<PrivateRoute  path = '/viewBus'component = {BusView} />  
				<PrivateRoute path = '/insertBus' component = {BusInsert} />  
				<PrivateRoute  path = '/book/:id/:DepartureTime' component = {Book} />  
				<Route path = '/login' component = {Login}/> 
				<Route path = '/logout' component = {Logout}/>
				<PrivateRoute path = '/order' component = {Order} />  
				<PrivateRoute path = '/cancelOrder/:id/:journeyDate/:departureTime/:cancel' component = {CancelOrder} />  
				<PrivateRoute path = '/route' component = {RouteView}/>  
				<PrivateRoute path = '/bookings' component = {TodayBooking}/>  
				<Route path = '/modal' component = {InValidModal}/>
				<PrivateRoute  path = '/oneMonthRecord' component = {oneMonthRecord}/>  
				<Route path = '/loginModal' component = {LoginModal}/>
				<Route path = '/log' component = {LogInfo}/>
				<PrivateRoute exact path = '/' component = {OneDayRecord}/>
				<Route path = '/' component = {NotFound}/>  
			</Switch>
		);
	}
	
	let app = (
		<Router>
			<Layout nav = {navBar}>			 
					{route}
			</Layout>
		</Router>
  	)	
	return app;	
	}
}
const mapStateToProps = state => {
    return {
        auth : state.auth.userId
    }
}

export default connect(mapStateToProps, null)(App)

