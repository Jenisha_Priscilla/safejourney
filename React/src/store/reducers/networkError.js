import * as actionTypes from '../actions/actionsTypes'

const initialState = {
    showNetworkError : false
};

const networkErrorReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SHOW_NETWORK_ERROR:
            return {
                showNetworkError : true
            }
            
        case actionTypes.HIDE_NETWORK_ERROR:
            return {
                showNetworkError : false
            }
      
        default :
            return state
    }
}

export default networkErrorReducer