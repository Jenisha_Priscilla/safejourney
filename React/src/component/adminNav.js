import React from 'react';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import {Link} from 'react-router-dom'
import './nav.css'

const AdminNav = () => {
    const style = {
      color : 'grey',
      padding : '0.5rem 1rem',
      textDecoration : 'none'
    }
  
    return (
      <Navbar collapseOnSelect expand = "lg"  variant = "dark">
        <Navbar.Brand >
          <img
            alt = ""
            src = {process.env.REACT_APP_API+"/image/makeTrip.png"}
            width = "30"
            height = "30"
          /> {'  '}  <Link to = "/" className =  'd-inline p-2 text-decoration-none text-white'>Safe Journey</Link>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse >
          <Nav className = "mr-auto">
            <Link to = "/search"  style = {style}> Search </Link>
            <Link to ="/bookings"  style = {style}> Bookings </Link>
            <Link to ="/insertBus"   style = {style}> Add bus </Link>
            <Link to ="/viewBus"  style = {style}> view bus </Link>
              
            <Link to ="/route"  style = {style}> Route </Link>
          </Nav>
          <Nav>
          <Link to = "/log" style = {style}> Activity </Link>
            <Link to = "/order" style = {style}> My Trips </Link>
            <Link to = "/logout"  style = {style}> Logout </Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar> 
    );

}

export default AdminNav