import React from 'react'
import Badge from 'react-bootstrap/Badge'
import { withRouter } from 'react-router-dom';
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/search'

const FilterRecord = (props) => {

    const applyFilter = () => {
        if(props.busType.length || props.departureTime.length) {
            const filterData = {
                DepartureStation:localStorage.getItem('departureStation'),
                ArrivalStation:localStorage.getItem('arrivalStation'),
                JourneyDate:localStorage.getItem('journeyDate'),
                BusType : props.busType,
                DepartureTime : props.departureTime
            };
            console.log(filterData)
            props.applyFilter(filterData);
            props.filterOpen();
            props.history.push('/filterResult')
        }
        // else {
        //     props.filterOpen();
        //     console.log("err")
        // }
    }
    
    const removeFilter = () => {
        const searchData= {
            DepartureStation:localStorage.getItem('departureStation'),
            ArrivalStation:localStorage.getItem('arrivalStation'),
            JourneyDate:localStorage.getItem('journeyDate')  
        };
        props.searchSuccess(searchData);
        props.removeAllFilter();
        props.filterOpen();
        props.history.push('/searchResult')
    }

    const onCheck = (e) => {
        if(e.target.checked) {
            props.filterType(e.target.value)
        }  
        if(!e.target.checked) {
            props.filterRemoveType(e.target.value)
        }    
    }

    const onCheckDepartureTime = (e) => {
        if(e.target.checked) {
            props.filterTime(e.target.value)
        }  
        if(!e.target.checked) {
            props.filterRemoveTime(e.target.value)
        }    
    }

 
        return (
                <Card bg = 'light'>
                <Card.Body>
                    Bus type
                    <Form >
                        {
                            props.type.map(function(object) {
                            return  <Form.Check type ='checkbox'>
                                <Form.Check.Input  onClick = {onCheck} value = {object.type} 
                                    checked = {object.check}
                                    type = 'checkbox' 
                                /> 
                                <Form.Check.Label style = {{fontSize : 14}}> 
                                    {object.value}
                                </Form.Check.Label>
                            </Form.Check>
                            })
                        }
                    <br/>
                    Departure time 
                        {
                            props.time.map (function(object) {
                            return  <Form.Check type ='checkbox'>
                                <Form.Check.Input  onClick = {onCheckDepartureTime} value = {object.time} 
                                        checked = {object.check}
                                        type = 'checkbox' 
                                /> 
                                <Form.Check.Label style = {{fontSize : 14}}> 
                                    {object.value}
                                </Form.Check.Label>                            
                            </Form.Check>
                            })
                        }
                        <br/>
                        <Badge  variant="dark" onClick = {applyFilter}>
                            Apply filter
                        </Badge>{' '}
                        <Badge  variant="dark" onClick = {removeFilter}>
                            Clear filter
                        </Badge>
                    </Form>
                </Card.Body>
            </Card>    
 
        )
    
}

const mapStateToProps = state => {
    return {
        searchResult : state.search.searchResult,
        type : state.search.type,
        time: state.search.time,
        busType : state.search.busType,
        departureTime : state.search.departureTime,
        filterOpen : state.search.filterOpen
    }
}
const mapDispatchToProps = dispatch => { 
    return {
        applyFilter : (filterData) => dispatch(actionTypes.applyFilter(filterData)),
        searchSuccess : (searchData) => dispatch(actionTypes.searchSuccess(searchData)),
        filterType : (value) => dispatch(actionTypes.filterType(value)),
        filterRemoveType : (value) => dispatch(actionTypes.filterRemoveType(value)),
        filterRemoveTime : (value) => dispatch(actionTypes.filterRemoveTime(value)),
        filterTime : (value) => dispatch(actionTypes.filterTime(value)),
        removeAllFilter : () => dispatch(actionTypes.removeAllFilter()),
        filterOpen : () => dispatch(actionTypes.filterOpen())
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FilterRecord));
