import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios'
import Card from 'react-bootstrap/Card'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/index'
import * as actionType from '../../../store/actions/networkError'
import Image from 'react-bootstrap/Image'
import NetworkError from '../../../component/networkError';

class SignUp extends Component 
{
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            redirect: false,
            userNameError: '',
            fieldError : ''
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.validate = this.validate.bind(this);
    }

    validate() {
        var fieldError = ''
        
        var userNameError = ''
        if (this.state.username == "" && this.state.password == "") {
            fieldError = '* All fields must be filled'
        }
        if(fieldError == '') {
            const usernamePattern = /^[a-zA-Z ]+$/g; 
           
            if(!this.state.username.match(usernamePattern)){
                userNameError = 'Must contain only alphabets'
            }
        }
        if (fieldError != "" || userNameError != "" ) {
            this.setState({fieldError, userNameError})
            return false;
        }

        return true;
    }
  
    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate()
        if(isValid ){
            this.setState({passwordError : ''})
        
            const obj = {
                Name:this.state.username,
                Password:this.state.password,
            };  
            
            axios.post('/user/signup',obj)
            .then(response => {
                console.log(response.data)
                if (response.data.payload.count == 1) {
                    this.setState ({userNameError: " ", redirect :true})
                    localStorage.setItem('userData',response.data.payload.user_id);
                    localStorage.setItem('exp', response.data.payload.exp);
                    localStorage.setItem('token', response.data.token);
                    this.props.authSuccess(response.data.payload.user_id);
                } 
                else {
                    this.setState({userNameError : "User Name unavailable ", passwordError : ''})
                }
            })
            .catch(err => this.props.showNetworkError())
        }  
    }

    handleChange(e) {
        this.setState({[e.target.name]:e.target.value})
    }

    render() {
        
        const { from } = this.props.location.state || { from: { pathname: '/' } }
        console.log(this.props)
            let signup =  (  
                <Row style = {{justifyContent: "center"}}>
                    <Col md = 'auto' className = "d-none d-lg-block p-0" style = {{marginTop:15}}>
                        <Image  
                            width = '250'
                            height = '550'
                            src = {process.env.REACT_APP_API+"/image/1598770901744.jpg" }
                        />
                    </Col>
                <Col md = {5} className = "d-lg-block p-lg-0 m-lg-0 m-sm-auto w-sm-auto">  
             <h4 style = {{marginTop:80, textAlign : "center"}}> Sign up </h4> 
            <br/>    
                
                    <Card  border = "primary">
                        <Card.Body>
                            <br/>
                            <Form>
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.fieldError}
                                </div>   
                                <Form.Row>
                                    <Form.Label column lg={2}> Name </Form.Label>
                                    <Col>
                                        <Form.Control type ="text" placeholder="Username" name = "username"
                                            onChange ={this.handleChange} 
                                        />
                                    </Col>
                                </Form.Row>

                                <br/>

                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.userNameError}
                                </div>  

                                
                                    <Form.Row>
                                        <Form.Label column lg={2}> Password </Form.Label>
                                        <Col>
                                            <Form.Control type ="password" placeholder="Password" 
                                            name = "password" onChange ={this.handleChange}
                                        />
                                        </Col>
                                    </Form.Row>
                               
                                
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.passwordError}
                                </div> 

                                <br/>

                                <Button variant="dark" onClick = {this.onSubmit} > Sign up </Button>
                            </Form>
                            <br/>
                            </Card.Body>
                    </Card>
                  
                </Col>
                <Col  md = 'auto' className = "d-none d-lg-block p-0" style = {{marginTop:15}}>
                <Image  
                        width = '250'
                        height = '550'
                        src = {process.env.REACT_APP_API+"/image/1598770901752.jpg"}
                />
            </Col>
        </Row>
            );
        
            if (localStorage.getItem('userData')) { 
                if (this.props.location.state) {
                    console.log(from)
                    signup = (<Redirect to =  {from} />)
                } 
                
            } 
            if(this.props.showError) {
                console.log("networkError")
                signup = <NetworkError/>
            }

       return signup;
    }
}

const mapStateToProps = state => {
    return {
        showError : state.networkError.showNetworkError
    }
}
const mapDispatchToProps = dispatch => {
    return {
        authSuccess : (userId) => dispatch(actionTypes.authSuccess(userId)), 
        showNetworkError : () => dispatch(actionType.showNetworkError()),
    }
}

export default connect( mapStateToProps, mapDispatchToProps)(SignUp)
