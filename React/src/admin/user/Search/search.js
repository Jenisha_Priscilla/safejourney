import React, { Component } from 'react'
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import axios from 'axios'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/search'
import * as actionType from '../../../store/actions/auth'
import * as action from '../../../store/actions/networkError'
import NetworkError from '../../../component/networkError'
import './bground.css'

class Search extends Component 
{
    constructor() {
        super();
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            departureStationOption:[],
            arrivalStationOption:[],
            redirect: true,
            searchError:''
        }
    }
    
    componentDidMount() {
      
// let url1 = 'http://localhost/safeJourney/bus/view';
// let url2 = 'http://localhost/safeJourney/route/view' ;
// let url3 = 'http://localhost/safeJourney/bus/view';
 
//  const promise1 = axios.get(url1);
//  const promise2 = axios.get(url2);
//  const promise3 = axios.get(url3);


//  Promise.all([promise1, promise2, promise3]).then(function(values) {
//     console.log(values);
//   });
        localStorage.removeItem('url')
        axios.get('/search/destinationStation')
        .then(response => {
            this.setState({ departureStationOption:response.data });
        })
        .catch(err => this.props.showNetworkError())  

        axios.get('/search/arrivalStation')
        .then(response => {
            this.setState({ arrivalStationOption:response.data });
        })
        .catch(err => this.props.showNetworkError())  
    }
    
    onSubmit(e) {
        e.preventDefault();
        if (localStorage.getItem('departureStation') && localStorage.getItem('arrivalStation') && localStorage.getItem('journeyDate')) {
            if (localStorage.getItem('departureStation') === localStorage.getItem('arrivalStation')) {
                this.setState({ searchError : '* Destination station and Arrival station cannot be same'})
            }
            else {
                this.props.result()
                this.setState({ searchError : ''})
                this.props.history.push('/searchResult')
            }
        }
        else {
           this.setState({ searchError : '* Must fill all the field'})
        } 
    }
  

    render() {
        var today = new Date();
        var dd = today.getDate() ;
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if (dd<10) {
            dd='0'+dd;
        } 
      
        if (mm<10) {
            mm='0'+mm;
        } 
        today = yyyy+'-'+mm+'-'+dd;
        if(this.props.showError) {
            return <NetworkError/>  
        }
                
           return(       <div className = "bground">
               
                            <Form onChange = {this.props.handleChange}>
                            
                                <Row >
                                    <Col md = {4}>
                                        <Form.Label> Departure Station </Form.Label>
                                        <Form.Control as = "select"  name = "departureStation"
                                        value = {localStorage.getItem('departureStation')} >
                                            <option value = "From" disabled selected> From </option>
                                            {this.state.departureStationOption.map (function(object, i) {  
                                                return  <option key ={i}> {object.DepartureStation}   </option>  ; 
                                            })}
                                        </Form.Control>
                                    </Col>
                                    <Col md = {4}>
                                        <Form.Label> Arrival Station </Form.Label>
                                        <Form.Control as = "select" name = "arrivalStation"
                                        value = {localStorage.getItem('arrivalStation')}>
                                             <option value = "To" disabled selected>To</option>
                                            { this.state.arrivalStationOption.map (function(object, i) {  
                                                return  <option key = {i}> {object.ArrivalStation}  </option>; 
                                            })}
                                        </Form.Control>
                                    </Col>
                                    <Col md = {4}>
                                        <Form.Label> Journey Date </Form.Label>
                                        <Form.Control type = "date" placeholder = "Enter Journey Date" 
                                        name = "journeyDate"
                                        value = {localStorage.getItem('journeyDate')}  min = {today} required/>
                                    </Col>
                                </Row>
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.searchError}
                                </div> 
                                <div style = {{marginTop:40, textAlign: "center"}}> 
                                <Button variant = "dark" onClick = {this.onSubmit} >
                                    Search
                                </Button>
                            </div>
                        </Form>
                        </div>
                
           )

           
       
    }   
}


const mapStateToProps = state => {
    return {
        searchResult : state.search.searchResult,
        redirect: state.search.redirect,
        search : state.search.search,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        handleChange : (e) => dispatch(actionTypes.handleChange(e)),
        searchSuccess : (searchData) => dispatch(actionTypes.searchSuccess(searchData)),
        result : () => dispatch(actionTypes.noResult()),
        authLogout : () => dispatch(actionType.authLogout()), 
        showNetworkError : () => dispatch(action.showNetworkError())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Search)