import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faHome } from "@fortawesome/free-solid-svg-icons";

const NotFound = (props) =>
    <div style = {{textAlign : 'center', lineHeight : '3' }}>
        <h3> Page not found </h3>
        <p> We are sorry the page you are looking for does not exist </p>
        <div onClick = {()=> props.history.push('/')}>
            <h5 style = {{display : 'inline'}}> Go </h5> <FontAwesomeIcon icon = {faHome} size = "2x" /> 
        </div>
    </div>

export default NotFound;