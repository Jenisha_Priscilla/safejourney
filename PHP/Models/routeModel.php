<?php

class RouteModel 
{
	public $pdo;
	function __construct()
	{
		$db = new Database();
		$this->pdo = $db->__get('pdo');
	//   $this->pdo = $pdo;
	}
	  
	public function insert($data, $name) 
	{
		$departureStation = $data["departureStation"];
		$arrivalStation = $data["arrivalStation"];
		$stmt = $this->pdo->prepare("SELECT Id FROM Route WHERE DepartureStation = ?  AND ArrivalStation = ?");
		$stmt->execute([$departureStation, $arrivalStation]);
		$row = $stmt->fetch();
		$id = $row['Id'];
		
		$sql = "INSERT INTO Route( DepartureStation, ArrivalStation ) 
		VALUES ('$departureStation', '$arrivalStation')";
		if($this->pdo->query($sql)) {
			require_once "./log/log.php";
			logg($name, __METHOD__, $data);
			return true;
		}
	}

	public function view()
	{
		$data = [];
		$sql = "SELECT * FROM Route where del = 0 ORDER BY Id desc";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {		
			$data[$cr]['Id'] = $row['Id']; 
			$data[$cr]['DepartureStation'] = $row['DepartureStation']; 
			$data[$cr]['ArrivalStation'] = $row['ArrivalStation']; 
			$cr++;
		}
		return json_encode($data);
	}

	public function delete($id, $name)
	{
		$id = $id[0];
		$sql = "update Route set del = 1 where Id ='$id' ";
		$query = $this->pdo->query($sql);
		$sql1 = "update Bus set del = '1'
		where RouteId = '$id';";
		$query = $this->pdo->query($sql1);
		$sql2 = "update BusBookingDetails 
		inner join
		(select Bus.Id from Bus 
		inner join Route on Bus.RouteId = Route.Id
		where Route.Id= '$id') as c on c.Id = BusBookingDetails.BusId
		and BusBookingDetails.del = 0 and BusBookingDetails.JourneyDate >= curdate()
		set BusBookingDetails.del = 1 ;";

		if($this->pdo->query($sql2)) {
			require_once "./log/log.php";
			logg($name, __METHOD__, $id);
			return true;
		}
	}

	public function update($id, $data, $name) 
	{
		$departureStation = $data["departureStation"];
		$arrivalStation = $data["arrivalStation"];
		$stmt = $this->pdo->prepare("SELECT Id FROM Route WHERE DepartureStation = ?  AND ArrivalStation = ?");
		$stmt->execute([$departureStation, $arrivalStation]);
		$row = $stmt->fetch();
		$routeId = $row['Id'];
		
		$sql = "UPDATE Route SET DepartureStation = '$departureStation',
		ArrivalStation = '$arrivalStation'
		WHERE Id = $id LIMIT 1";
		if($this->pdo->query($sql)) {
			require_once "./log/log.php";
			logg($name,__METHOD__, $id, $data);
			return true;
		}
	}

	public function select($id)
	{
		$id = $id[0];
		if($id) {
			$sql2 = "SELECT * FROM Route WHERE Id = $id and del =0";
			$query = $this->pdo->query($sql2);
			$cr=0;
			while ($row = $query->fetch()) {		
				$data[$cr]['Id'] = $row['Id']; 
				$data[$cr]['DepartureStation'] = $row['DepartureStation']; 
				$data[$cr]['ArrivalStation'] = $row['ArrivalStation']; 
				$cr++;
				return json_encode($data);
		}	
		}
		
		
	}

	public function getRouteId()
	{
		$sql = "SELECT Id FROM Route where del = 0";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {	
			$data[$cr]['Id']=$row['Id']; 
			$cr++;
		}
		return json_encode($data);
	}

	public function searchByRoute($route)
	{
		$search = $route[0];
		$sql = "SELECT * FROM Route where del = 0 and  (DepartureStation like '%$search%' or ArrivalStation like '%$search%') 
		ORDER BY Id desc;";
		$query = $this->pdo->query($sql);
		$cr=0;
		while ($row = $query->fetch()) {		
			$data[$cr]['Id'] = $row['Id']; 
			$data[$cr]['DepartureStation'] = $row['DepartureStation']; 
			$data[$cr]['ArrivalStation'] = $row['ArrivalStation']; 
			$cr++;
		}
		return json_encode($data);
	}
}
