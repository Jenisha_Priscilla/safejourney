import React from 'react'
import Table from 'react-bootstrap/Table'
import { connect } from 'react-redux';
import CountMonth from './countMonth'
import Record from '../oneDayRecord/record';
import Spinner from '../../spinner/spinner'
import Image from 'react-bootstrap/Image'

const CancelledMonthDetails = (props) => {

    const bookingRecordList = () => {
        return props.cancelledMonthDetails.map (function(object, i) {   
            return <Record obj = {object} key ={i} />; 
        });
    }
        let record = props.loading ? <Spinner/> : props.cancelledMonthDetails ? <Table responsive hover >
                <thead>
                    <tr>
                        <th> User Name</th>
                        <th> Bus Name </th>
                        <th> Bus Type</th>
                        <th> PlateNo.</th>
                        <th> Departure station </th>
                        <th> Arrival station </th>
                        <th> Journey date</th>
                        <th> Booked date</th>
                        <th> No.of ticket</th>
                    </tr>
                </thead>
                <tbody>
                    {bookingRecordList()}
                </tbody>
            </Table> : <div style ={{textAlign:"center",marginTop:40}}> 
                <Image
                    alt = ""
                    src = {process.env.REACT_APP_API+"/image/record.png"}
                    height = "190"
                    width = "190"
                />  <br/>
               
            </div>
        
        return (
            <>
                <CountMonth></CountMonth>
                    <h5 align = "center"> One month record  </h5>
                    {record}
            </>
        )
    }

const mapStateToProps = state => {
    return {
        cancelledMonthDetails : state.record.cancelledDetails,
        loading : state.record.loading
    }
}

export default connect(mapStateToProps, null)(CancelledMonthDetails) 
