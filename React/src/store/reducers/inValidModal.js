import * as actionTypes from '../actions/actionsTypes'

const initialState = {
    show : false
};

const inValidModalreducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SHOW_MODAL:
            return {
                show : true
            }
            
        case actionTypes.HIDE_MODAL:
            return {
                show : false
            }
      
        default :
            return state
    }
}

export default inValidModalreducer