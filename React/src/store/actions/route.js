import * as actionTypes from './actionsTypes'
import axios from 'axios';
import { authLogout } from './auth';
import {show} from './inValidModal'
import { showNetworkError } from './networkError';

export const getRoute = (routeId) => {
    return dispatch => {
        axios.get('/route/select/'+routeId)
        .then(response => {
            const route ={...response.data['0']}
            dispatch(setRoute(route))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }  
}

export const setRoute = (route) => {
    return {
        type: actionTypes.SET_ROUTE,
        route : route
    }
}

export const handleStationChange = (event) => {
    return {
        type: actionTypes.HANDLE_STATIONCHANGE,
        event : event
    }
};

export const handleRouteEdit = (event) => {
    return {
        type: actionTypes.HANDLE_ROUTE_EDIT,
        event : event
    }
};

export const updateRoute = () => {
    return {
        type: actionTypes.ROUTE_UPDATE
    }
}

export const routeUpdate = (routeData) => {
    return dispatch => {
        axios.post('/route/update',routeData,
        // {
        //     headers : {
        //         'AUTHENTICATION' : localStorage.getItem('token') 
        //     }
        // }
        )
            .then((response) => {
                console.log(response.data);
                if(response.data != false){
                    alert('Data edited successfully');
                    dispatch(routeUpdateSuccess())
                } else {
                    dispatch(show())
                    dispatch(routeUpdateSuccess())
                    dispatch(authLogout());
                }    
            })
            .catch(err =>  dispatch(showNetworkError()))
    }
}

export const routeUpdateSuccess = () => { 
    return {
        type: actionTypes.ROUTE_UPDATE_SUCCESS
    }
}

export const routeInsert = (routeData) => {
    return dispatch => {
        axios.post('/route/insert',routeData,
        // {
        //     headers : {
        //         'AUTHENTICATION' : localStorage.getItem('token') 
        //     }
        // }
        )
            .then((response) => { 
                if(response.data != false){
                    alert('Data added successfully');
                    dispatch(routeInsertSuccess())
                } else {
                    dispatch(show())
                    dispatch(authLogout());
                }   
            })
            .catch(err =>  dispatch(showNetworkError()))
    }
}

export const routeInsertSuccess = () => {
    return {
        type : actionTypes.ROUTE_INSERT_SUCCESS
    }
}






