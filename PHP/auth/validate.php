<?php
require_once 'carbon.php';
function validate() {
    $headers = apache_request_headers();
    $string = $headers['AUTHENTICATION'];
    $token = trim($string,'"'); // double quotes
    $tokenParts = explode('.', $token);
    $header = base64_decode($tokenParts[0]);
    $payload = base64_decode($tokenParts[1]);
    $signatureProvided = $tokenParts[2];
    //$payload->exp time in unix timestamp
    //Carbon::createFromTimestamp(json_decode($payload)->exp) time in UTC
    $expiration = Carbon::createFromTimestamp(json_decode($payload)->exp);
    $tokenExpired = (Carbon::now()->diffInSeconds($expiration, false) < 0);

    $base64UrlHeader = str_replace(['+','/','='],['-','_',''],base64_encode($header));
    $base64UrlPayload = str_replace(['+','/','='],['-','_',''],base64_encode($payload));
    $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abc123!', true);
    $base64UrlSignature =str_replace(['+','/','='],['-','_',''],base64_encode($signature));

    $signatureValid = ($base64UrlSignature === $signatureProvided);
    if (!$tokenExpired && $signatureValid) {
        return json_decode($payload);
    } else {
        return false;
    }
}