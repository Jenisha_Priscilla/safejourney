import React, {memo} from 'react';
import Form from 'react-bootstrap/Form'
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import Button from 'react-bootstrap/Button'

const BookList = (props) =>
{
    return (
      props.taskList.map((val, idx) => {
        return (
          <>
            <tr>
              <td style = {{width :300}}> 
                <Form.Control 
                  type = "text"  pattern = "[a-zA-Z]+" title = "Must contain alphabets only " 
                  placeholder = "Name"  min = "1" name = "Name" data-id={idx}  
                />
              </td>
              <td style ={{width :180}}>
                <Form.Control 
                  type = "number" placeholder = "Age" name="Age" 
                  data-id={idx} min = "1" max = "120"/>
              </td>
              <td style ={{width :180}}>
                <Form.Control 
                  type="number" placeholder="Seat No" 
                  name="SeatNo" data-id = {idx} />
              </td>
              <td>
                { 
                idx===0? <Button variant="dark"  onClick = {props.add} > <FontAwesomeIcon icon={faPlus} size = "sm" /> </Button> : 
                         <Button variant="danger" onClick={(() => props.delete(val))} > <FontAwesomeIcon icon={faMinus} size = "sm" /> </Button>
                }
              </td>
            </tr >
          </>
        )
      })
    )
}
  

export default memo(BookList)