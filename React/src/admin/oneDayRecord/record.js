import React from 'react'

const Record = (props) => {
  
        return (
            <tr>
                <td>
                    {props.obj.Name}
                </td>
                <td>
                    {props.obj.BusName}
                </td>
                <td>
                    {props.obj.BusType}
                </td>
                <td>
                    {props.obj.PlateNo}
                </td>
                <td>
                    {props.obj.DepartureStation}
                </td>
                <td>
                    {props.obj.ArrivalStation}
                </td>
                <td>
                    {props.obj.JourneyDate}
                </td>
                <td>
                    {props.obj.BookedDate}
                </td>
                <td>
                    {props.obj.Tickets}
                </td>
            </tr>
        )
} 
export default Record