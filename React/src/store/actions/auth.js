import * as actionTypes from './actionsTypes'
export const authSuccess = (userId) => {
    return {
        type : actionTypes.AUTH_SUCCESS,
        userId : userId
    }
}

export const authLogout = () => {
    localStorage.removeItem('userData')
    localStorage.removeItem('token')
    localStorage.removeItem('role')
    
    return {
        type : actionTypes.AUTH_LOGOUT
    }
}