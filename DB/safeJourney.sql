
create database safeJourney;
use safeJourney;
drop database safeJourney;
-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Bus`
--

DROP TABLE IF EXISTS `Bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RouteId` int(11) DEFAULT NULL,
  `BusName` varchar(20) DEFAULT NULL,
  `BusType` varchar(20) DEFAULT NULL,
  `PlateNo` varchar(20) DEFAULT NULL,
  `SeatsCount` int(11) DEFAULT NULL,
  `Fare` int(11) DEFAULT NULL,
  `del` int(11) DEFAULT '0',
  `DepartureTime` time DEFAULT '10:00:00',
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ArrivalTime` time DEFAULT '10:00:00',
  PRIMARY KEY (`Id`),
  KEY `FK_RouteId` (`RouteId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BusBookingDetails`
--

DROP TABLE IF EXISTS `BusBookingDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BusBookingDetails` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) DEFAULT NULL,
  `BusId` int(11) DEFAULT NULL,
  `NoOfTickets` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `JourneyDate` date DEFAULT NULL,
  `del` int(11) DEFAULT '0',
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_BusId` (`BusId`),
  KEY `FK_UserId` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BusPassenger`
--

DROP TABLE IF EXISTS `BusPassenger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BusPassenger` (
  `BookId` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `SeatNo` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `del` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_BookId1` (`BookId`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `Id` int(11) NOT NULL,
  `Role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `Route`
--

DROP TABLE IF EXISTS `Route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Route` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DepartureStation` varchar(20) DEFAULT NULL,
  `ArrivalStation` varchar(20) DEFAULT NULL,
  `del` int(11) DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `UserBus`
--

DROP TABLE IF EXISTS `UserBus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserBus` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`),
  KEY `FK_RoleId` (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



alter table Bus
add  CONSTRAINT `FK_RouteId` FOREIGN KEY (`RouteId`) REFERENCES `Route` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

alter table BusBookingDetails
add  CONSTRAINT `FK_BusId` FOREIGN KEY (`BusId`) REFERENCES `Bus` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
add CONSTRAINT `FK_UserId` FOREIGN KEY (`UserId`) REFERENCES `UserBus` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

alter table BusPassenger
add CONSTRAINT `FK_BookId1` FOREIGN KEY (`BookId`) REFERENCES `BusBookingDetails` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

alter table UserBus
add   CONSTRAINT `FK_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `Role` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;



LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` VALUES (1,'Admin'),(2,'User');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `UserBus` WRITE;
/*!40000 ALTER TABLE `UserBus` DISABLE KEYS */;
INSERT INTO `UserBus` VALUES (1,1,'Admin','hyworld'),(2,2,'Jenisha','jeni'),(75,2,'Lovisha','lovi'),(76,2,'Jebaraj','jeba'),(77,2,'rajan','rajan'),(79,2,'rajan1','rajan'),(80,2,'Beaula','beaula'),(81,2,'',''),(85,2,'Bunty','bunty'),(87,2,'kans','kans'),(89,2,'kevin','kevin'),(93,2,'benny','benny'),(105,2,'Lee','lee'),(107,2,'sujan','suajn'),(117,2,'kamal','kamalhassan');
/*!40000 ALTER TABLE `UserBus` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `Route` WRITE;
/*!40000 ALTER TABLE `Route` DISABLE KEYS */;
INSERT INTO `Route` VALUES (1,'Coimbatore','Chennai',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(2,'Chennai','Tuticorin',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(4,'Banglore','Chennai',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(5,'Chennai','Banglore',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(6,' ',' ',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(7,' ',' ',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(8,' ',' ',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(9,' ',' ',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(10,' ',' ',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(11,' ',' ',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(12,'Banglore','Chenn',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(13,' ',' ',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(14,'Banglore','Chennai',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(15,'Banglore','Chennai',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(16,'Chennai','Coimbatore',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(17,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(18,'hr','f',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(19,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(20,'1','jh',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(21,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(22,'214',',',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(23,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(24,'g','g',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(25,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(26,'ejf','gwr',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(27,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(28,'jeni','sha',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(29,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(30,'rg','eg',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(31,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(32,'f4','34t',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(33,'f4','34t',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(34,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(35,'45gv','rw4',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(36,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(37,'gt','t3',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(38,'gt','t3',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(39,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(40,'grgn442','r3',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(41,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(42,'r432','2wr5',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(43,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(44,'5t3','rge3gq5',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(45,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(46,'jeni','sha',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(47,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(48,'ew','ew',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(49,'e','dd',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(50,'jf','jenisha',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(51,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(52,'jenisha','aaada',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(53,'','',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(54,'te','e',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(55,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(56,'jeni','da',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(57,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(58,'jeni','sha',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(59,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(60,'the','jeni',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(61,'','',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(62,'renin','renin',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(63,'','',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(64,'Manglore','Salem',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(65,'','',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(66,'Cochin','Salem',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(67,'Cochin','Salem',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(68,'Tuticorin','Coimbatore',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(69,'Tuticorin','Manglore',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(70,'Coimbatore','Chennai',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(71,'Banglore','Chennai',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(72,'Coimbatore','Tuticorin',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(73,'Tuticorin','Chennai',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(74,'Salem','Manglore',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(75,'jenisha','lovisha',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(76,'Coimbatore','k',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(77,'Tuticorin','t',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(78,'Tuticorin','jeni',1,'2020-08-23 08:26:59','2020-09-02 11:40:48'),(79,'Tuticorin','Manglore',0,'2020-08-23 08:26:59','2020-09-02 11:41:09'),(80,'Tuticorin','lovisha',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(81,'rtfg','Manglore',1,'2020-08-23 08:26:59','2020-09-02 11:31:24'),(82,'Tuticorin','jenisha',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(83,'Tuticorin','Chenna',0,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(84,'Tuticorin','Banglore',0,'2020-08-23 08:26:59','2020-08-28 15:55:19'),(85,'Tuticorin','Chenn',1,'2020-08-23 08:26:59','2020-08-23 08:28:57'),(86,'lovu','jni',1,'2020-09-02 11:08:24','2020-09-02 11:30:07');
/*!40000 ALTER TABLE `Route` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `Bus` WRITE;
/*!40000 ALTER TABLE `Bus` DISABLE KEYS */;
INSERT INTO `Bus` VALUES (1,1,'YBM travels','AC sleeper','TN 38 AC 2345',40,800,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(2,NULL,' RV Travels',' A.C Sleeper',' TN 38 AC 0015',43,850,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(3,1,' RV Travels',' AC sleeper',' TN 38 AC 0015',41,850,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(4,2,'SR Travels','AC Semi-Sleeper','TN 38 AC 1998',40,650,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(5,2,'YBM Travels','NON-AC Semi Sleeper','TN 36 AB 9690',45,550,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(7,16,'RV Travels','AC Sleeper','TN 35 TH 3045',43,670,1,'21:15:00','2020-08-28 16:00:00','2020-08-23 08:25:23','10:00:00'),(11,2,'2','2','2',2,2,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(12,1,'YBM travels','AC sleeper','TN 38 AC 2345',40,800,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(13,1,'gf','ef','TN 33 DW 342',1,44,1,'10:00:00','2020-08-05 12:37:25','2020-08-23 08:25:23','10:00:00'),(14,64,'RV Travels','AC Sleeper','TN 35 TH 3345',43,569,1,'21:15:00','2020-09-02 11:19:21','2020-08-23 08:25:23','10:00:00'),(15,64,'Orange Bus','AC Sleeper  ','TN 3 BG 0005',26,970,0,'08:00:00','2020-09-04 14:00:56','2020-08-23 08:25:23','13:00:00'),(16,64,'Orange Bus','AC Sleeper  ','TN 3 BG 0001',45,895,1,'23:10:00','2020-09-01 06:16:56','2020-08-23 08:25:23','10:00:00'),(17,5,'YM Travels','AC Sleeper  ','TN 3 BG 0013',40,900,0,'10:00:10','2020-09-12 13:55:45','2020-08-23 08:25:23','15:30:00'),(18,5,'YBM Travels','AC sleeper','TN 38 CG 0081',45,700,1,'08:00:00','2020-08-20 06:52:18','2020-08-23 08:25:23','10:00:00'),(19,16,'YBM Travels','AC semi sleeper','TN 38 BG 0111',30,800,0,'10:00:00','2020-09-04 14:00:36','2020-08-23 08:25:23','16:00:00'),(20,5,'Orange Bus','AC Sleeper','TN 38 BG 0099',41,800,1,'14:05:00','2020-09-12 07:04:31','2020-08-23 08:25:23','22:00:00'),(21,5,'Orange Bus','AC Sleeper','TN 38 BG 0099',40,870,1,'16:05:00','2020-09-12 13:42:04','2020-08-23 08:25:23','21:00:00'),(22,64,'RV Travels','AC semi sleeper','TN 37 BG 0699',41,840,0,'09:00:00','2020-09-12 16:40:19','2020-08-23 08:25:23','15:00:10'),(23,5,'Orange Bus','AC Sleeper','TN 38 BG 0099',41,800,0,'15:05:00','2020-09-12 16:40:05','2020-09-04 13:43:28','21:00:00');
/*!40000 ALTER TABLE `Bus` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `BusBookingDetails` WRITE;
/*!40000 ALTER TABLE `BusBookingDetails` DISABLE KEYS */;
INSERT INTO `BusBookingDetails` VALUES (3,1,1,1,'2020-04-04 09:29:54','2020-04-08',0,NULL),(8,2,1,1,'2020-04-08 13:00:58','2020-04-08',0,NULL),(9,1,3,1,'2020-04-22 09:10:53','2020-04-22',1,NULL),(13,2,1,2,'2020-04-23 07:39:14','2020-04-23',0,NULL),(14,80,1,1,'2020-04-23 09:09:48','2020-04-23',0,NULL),(15,1,1,1,'2020-04-24 08:22:49','2020-04-30',1,NULL),(16,1,1,1,'2020-04-24 10:11:46','2020-04-30',1,NULL),(17,76,3,1,'2020-04-25 09:09:03','2020-05-01',1,NULL),(18,1,1,1,'2020-04-25 10:00:14','2020-06-01',1,NULL),(19,1,3,1,'2020-04-27 11:16:11','2020-04-27',1,NULL),(20,1,1,1,'2020-05-01 09:56:30','2020-05-30',1,NULL),(21,1,1,1,'2020-05-01 10:25:07','2020-05-28',1,NULL),(22,1,1,1,'2020-05-07 10:28:00','2020-05-22',1,NULL),(23,2,3,2,'2020-05-07 11:40:04','2020-06-02',1,NULL),(24,1,1,1,'2020-05-09 10:16:09','2020-05-16',0,NULL),(25,1,3,1,'2020-05-17 16:46:31','2020-05-30',1,NULL),(26,1,3,1,'2020-05-17 16:51:25','2020-05-23',1,NULL),(27,1,4,1,'2020-05-17 17:54:23','2020-05-22',1,NULL),(28,2,7,1,'2020-05-18 10:22:42','2020-05-31',1,NULL),(29,2,1,1,'2020-05-18 10:48:14','2020-05-29',1,NULL),(30,2,7,1,'2020-05-18 11:43:19','2020-05-23',1,NULL),(31,2,1,1,'2020-05-18 12:25:35','2020-05-30',1,NULL),(32,2,7,1,'2020-05-18 12:27:27','2020-05-30',1,NULL),(33,2,7,1,'2020-05-18 12:28:07','2020-05-30',1,NULL),(34,2,7,1,'2020-05-18 12:28:42','2020-05-29',1,NULL),(35,2,1,1,'2020-05-18 12:34:09','2020-05-29',1,NULL),(36,1,1,1,'2020-05-18 12:57:58','2020-05-20',1,NULL),(37,2,1,1,'2020-05-18 12:59:49','2020-05-20',1,NULL),(38,1,13,1,'2020-05-18 13:29:13','2020-05-27',1,NULL),(39,1,7,1,'2020-05-18 13:35:32','2020-05-22',1,NULL),(40,2,7,1,'2020-05-18 13:37:26','2020-05-22',1,NULL),(41,1,14,1,'2020-05-23 10:20:52','2020-05-30',0,NULL),(42,2,14,1,'2020-05-26 09:54:58','2020-05-28',0,NULL),(43,1,1,1,'2020-06-09 10:00:09','2020-06-11',0,NULL),(44,1,1,1,'2020-07-16 07:56:33','2020-07-16',1,NULL),(45,1,14,1,'2020-07-20 14:28:30','2020-07-23',0,NULL),(46,1,14,1,'2020-07-24 14:36:52','2020-07-24',0,NULL),(47,2,14,1,'2020-07-24 18:01:19','2020-07-25',0,NULL),(48,2,14,1,'2020-07-25 07:52:05','2020-07-26',1,NULL),(49,2,14,1,'2020-07-25 08:03:08','2020-07-26',1,NULL),(50,2,14,1,'2020-07-25 08:07:46','2020-07-26',1,NULL),(51,2,14,1,'2020-07-25 08:10:01','2020-07-26',1,NULL),(52,2,14,1,'2020-07-25 08:10:19','2020-07-26',1,NULL),(53,1,7,2,'2020-07-27 11:06:00','2020-07-28',1,NULL),(54,1,14,1,'2020-07-27 11:07:20','2020-07-31',0,569),(55,1,7,2,'2020-07-27 11:18:00','2020-07-28',0,569),(56,1,14,0,'2020-07-29 10:36:40','2020-07-31',0,569),(57,1,7,0,'2020-07-30 06:11:48','2020-07-31',0,850),(58,1,14,0,'2020-07-31 11:20:46','2020-08-01',0,569),(59,1,14,1,'2020-07-31 11:21:35','2020-08-01',0,569),(60,1,14,0,'2020-08-05 07:33:33','2020-08-05',0,569),(61,1,14,0,'2020-08-05 07:55:48','2020-08-05',0,569),(62,1,14,1,'2020-08-05 09:57:03','2020-08-06',0,569),(63,1,14,0,'2020-08-05 09:57:42','2020-08-06',0,569),(64,1,14,1,'2020-08-05 13:01:01','2020-08-06',0,569),(65,1,14,1,'2020-08-06 07:13:10','2020-08-06',0,569),(66,1,14,1,'2020-08-07 08:08:27','2020-08-07',0,569),(67,1,14,2,'2020-08-07 08:08:54','2020-08-07',0,569),(68,1,14,1,'2020-08-11 13:25:34','2020-08-13',0,569),(69,1,14,1,'2020-08-12 05:07:15','2020-08-13',0,569),(70,1,14,2,'2020-08-14 15:25:52','2020-08-15',0,569),(71,1,14,0,'2020-08-14 15:31:26','2020-08-15',0,569),(72,1,14,0,'2020-08-14 15:39:57','2020-08-15',0,569),(73,1,17,0,'2020-08-16 11:37:52','2020-08-17',0,890),(74,1,17,1,'2020-08-16 11:43:15','2020-08-20',0,890),(75,1,17,0,'2020-08-20 09:40:14','2020-08-27',0,890),(76,1,17,0,'2020-08-20 09:44:07','2020-08-21',0,890),(77,1,17,0,'2020-08-20 09:47:27','2020-08-21',0,890),(78,1,17,0,'2020-08-20 10:04:32','2020-08-28',0,890),(79,1,17,0,'2020-08-20 10:25:39','2020-08-21',0,890),(80,2,7,1,'2020-08-27 14:49:21','2020-08-30',1,670),(81,1,7,2,'2020-08-27 15:30:38','2020-08-30',1,670),(82,2,17,1,'2020-08-27 17:33:11','2020-08-30',0,890),(83,1,22,1,'2020-08-30 13:52:57','2020-08-31',0,800),(84,2,19,2,'2020-09-01 06:14:28','2020-09-09',0,800),(85,2,22,1,'2020-09-01 06:16:45','2020-09-09',0,800),(86,1,20,1,'2020-09-01 08:27:43','2020-09-01',0,800),(87,1,20,1,'2020-09-01 08:29:29','2020-09-01',0,800),(88,1,19,1,'2020-09-05 05:47:31','2020-09-09',0,800),(89,2,19,1,'2020-09-05 06:07:47','2020-09-11',0,800),(90,2,19,1,'2020-09-07 09:55:51','2020-09-17',0,800),(91,2,22,1,'2020-09-07 10:01:06','2020-09-17',0,800),(92,2,19,1,'2020-09-07 10:08:52','2020-09-17',0,800),(93,2,19,1,'2020-09-07 10:11:38','2020-09-17',0,800),(94,2,19,0,'2020-09-07 10:13:34','2020-09-17',0,800),(95,2,19,1,'2020-09-07 10:26:58','2020-09-17',0,800),(96,1,17,1,'2020-09-07 13:35:50','2020-09-11',0,890),(97,2,19,1,'2020-09-07 16:59:15','2020-09-24',0,800),(98,1,22,1,'2020-09-07 17:01:05','2020-09-17',0,800),(99,85,17,1,'2020-09-07 17:01:48','2020-09-10',0,890),(100,85,19,1,'2020-09-07 17:03:29','2020-09-24',0,800),(101,2,17,1,'2020-09-07 17:04:51','2020-09-10',0,890),(102,1,19,1,'2020-09-07 17:05:49','2020-09-10',0,800),(103,2,22,1,'2020-09-11 08:02:11','2020-09-17',0,800),(104,1,19,1,'2020-09-11 09:26:52','2020-09-17',0,800),(105,85,20,1,'2020-09-11 16:18:51','2020-09-25',1,800),(106,85,19,1,'2020-09-11 16:23:00','2020-09-19',0,800);
/*!40000 ALTER TABLE `BusBookingDetails` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `BusPassenger` WRITE;
/*!40000 ALTER TABLE `BusPassenger` DISABLE KEYS */;
INSERT INTO `BusPassenger` VALUES (3,'Jebaraj ',56,29,1,0),(8,'fef ',23,23,2,0),(8,'abcde ',44,18,3,0),(8,'def ',55,33,4,0),(8,'ght ',55,40,5,0),(9,'Jenisha ',21,21,6,0),(9,'Beaula ',46,40,7,0),(9,'Lovisha ',27,27,8,0),(13,'Lovisha ',27,21,9,0),(13,'Jenisha ',21,27,10,0),(14,'Renin ',22,22,11,0),(15,'Lovisha ',27,21,12,0),(16,'Rajan ',31,31,13,0),(17,'Jebaraj ',55,40,14,0),(18,'Admin ',21,31,15,0),(19,'Lovi ',55,23,16,0),(20,'Jenisha ',21,21,17,0),(21,'Rajan ',30,30,18,0),(22,'Hema ',21,6,19,0),(23,'Jenisha ',21,21,20,0),(23,'Lovisha ',27,22,21,0),(24,'gr ',23,29,22,0),(25,'ffffffffff ',55,23,23,0),(26,'sdwsd ',1,24,24,0),(27,'fef ',23,29,25,0),(28,'Lovisha ',55,23,26,0),(29,'Lovisha ',1,23,27,0),(30,'ffffffffff ',23,27,28,0),(31,'sdwsd ',23,29,29,0),(32,'efge ',56,27,30,0),(33,'fef ',23,23,31,0),(34,'sdwsd ',56,29,32,0),(35,'efge ',23,27,33,0),(36,'Lovisha ',23,23,34,0),(37,'sdwsd ',23,28,35,0),(38,'ffffffffff ',56,1,36,0),(39,'f ',23,29,37,0),(40,'fef ',56,23,38,0),(41,'sdwsd ',55,2,39,0),(42,'fef ',55,21,40,0),(43,'sdwsd ',55,27,41,0),(44,'Lovisha',23,24,42,0),(45,'ffffffffff',55,23,43,0),(46,'fef',23,23,44,0),(47,'Jenisha 3',22,22,45,0),(48,'jenisha',55,27,46,0),(49,'ffffffffff',23,29,47,0),(50,'ffffffffff',23,27,48,0),(51,'efge',23,29,49,0),(52,'efge',56,2,50,0),(53,'hgfhgf',55,6,51,0),(53,'ccc',44,22,52,0),(54,'sds',34,4,53,0),(55,'agfsh',44,22,54,0),(55,'vxhgvx',33,33,55,0),(56,'sdwsd',55,24,56,1),(56,'efge',56,29,57,1),(57,'jenisha',21,21,58,1),(57,'Lovisha',27,22,59,1),(58,'fef',56,24,60,1),(58,'efge',23,29,61,1),(59,'ffffffffff',23,29,62,0),(59,'efge',23,27,63,1),(60,'sdwsd',55,29,64,1),(61,'Lovisha',55,27,65,1),(61,'ffffffffff',23,27,66,1),(61,'Lovisha',27,21,67,0),(61,'Jenisha',34,34,68,0),(62,'ffffffffff',56,24,69,0),(63,'ffffffffff',23,23,70,1),(64,'Lovisha',27,29,71,0),(65,'jeni',22,22,72,1),(65,'mathi',22,23,73,0),(66,'sdwsd',55,27,74,0),(67,'fef',23,29,75,0),(67,'ffffffffff',56,24,76,0),(68,'Jenisha',22,22,77,0),(69,'Lovisha',27,21,78,0),(69,'Lovisha',27,43,79,0),(69,'jenisha',21,42,80,0),(71,'Lovisha',27,43,81,1),(71,'ffffffffff',21,42,82,1),(72,'Lovisha',21,43,83,1),(73,'Hema 5',45,4,84,1),(74,'Lovisha43',23,40,85,0),(75,'Jenisha',22,40,86,1),(75,'Lovisha',29,39,87,1),(76,'Lovisha',44,40,88,1),(77,'Jebaraj',56,4,89,1),(78,'Lovisha',3,27,90,1),(79,'Lovisha',27,21,91,1),(79,'Lovisha',27,27,92,0),(79,'Jenisha',22,22,93,0),(80,'Lovisha',27,27,94,0),(80,'Jenisha',21,22,95,1),(81,'Lovisha',22,22,96,0),(81,'Jenisha',27,28,97,0),(82,'Lovisha',22,22,98,0),(82,'Beaula',23,32,99,1),(83,'ffffffffff',1,1,100,0),(84,'Lovisha',29,29,101,1),(84,'jenisha',22,22,102,0),(84,'Beaula',49,23,103,0),(85,'Lovisha',22,22,104,1),(85,'Jenisha',21,21,105,0),(86,'ffffffffff',27,28,106,0),(87,'Lovisha',14,1,107,0),(88,'lovi',32,3,108,0),(89,'Jenisha',3,22,109,0),(90,'Jenisha',22,22,110,0),(91,'Jeni',23,23,111,0),(92,'Lovisha',56,29,112,0),(93,'Beaula',22,23,113,0),(94,'Jebaraj',22,25,114,1),(95,'Lovisha',27,30,115,0),(96,'sdwsd',56,24,116,0),(97,'Lovisha',27,21,117,0),(98,'Lovisha',27,13,118,0),(99,'efge',56,27,119,0),(100,'sdwsd',23,29,120,0),(101,'ffffffffff',56,24,121,0),(102,'ffffffffff',27,28,122,0),(103,'Lovisha',21,15,123,0),(104,'Lovisha',27,20,124,0),(105,'Lovisha',21,28,125,0),(106,'Lovisha',27,24,126,0);
/*!40000 ALTER TABLE `BusPassenger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-15  8:59:12
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'JP1998';
FLUSH PRIVILEGES;
SELECT user,authentication_string,plugin,host FROM mysql.user;


SELECT * FROM Bus;
SELECT * FROM Bus limit 0,10;

select BusBookingDetails.Id,Bus.DepartureTime,Bus.ArrivalTime, Route.DepartureStation, Route.ArrivalStation, 
		Bus.BusName,Bus.BusType, Bus.PlateNo,BusBookingDetails.del, BusBookingDetails.JourneyDate, 
		BusBookingDetails.NoOfTickets 
		from  Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		where BusBookingDetails.UserId = '1' order by BusBookingDetails.JourneyDate desc ;
select count(*)
		from  Bus inner join BusBookingDetails on BusBookingDetails.BusId = Bus.Id
		inner join Route on Bus.RouteId = Route.Id
		where BusBookingDetails.UserId = 1 order by BusBookingDetails.JourneyDate desc;
