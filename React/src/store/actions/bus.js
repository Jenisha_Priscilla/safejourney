import * as actionTypes from './actionsTypes'
import axios from 'axios';
import { authLogout } from './auth';
import {show} from './inValidModal'
import { showNetworkError } from './networkError';


export const handleBusChange = (event) => {
    return {
        type: actionTypes.HANDLE_BUSCHANGE,
        event : event
    }
};

export const handleRouteChange = (routeId) => {
    return {
        type : actionTypes.HANDLE_ROUTECHANGE,
        routeId : routeId
    }
}

export const setRouteIds = (routeIds) => {
    return {
        type: actionTypes.SET_ROUTEIDS,
        routeIds : routeIds
    }
};

export const initRouteIds = () => {
    return dispatch => {
        axios.get('/route/getRouteId')
        .then(response => {
            console.log(response.data)
            dispatch(setRouteIds(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
};

export const busInsertSuccess = () => {
    return {
        type: actionTypes.BUS_INSERT_SUCCESS,
    }
}

export const busInsert = (busData) => {
    console.log(busData)
    return dispatch => {
        axios.post('/bus/insert',busData,
        // {
        //     headers : {
        //         'AUTHENTICATION' : localStorage.getItem('token') 
        //     }
        // }
        )
            .then((response) => { 
              console.log(response.data)
                if(response.data != false){
                    alert('Data added successfully');
                    dispatch(busInsertSuccess())
                } else {
                    dispatch(show())
                    dispatch(authLogout());
                }   
            })
            .catch(err =>  dispatch(showNetworkError()))
    }
}

export const getStation = (routeId) => {
    return dispatch => {
        axios.get('/route/select/'+routeId)
        .then(response => {
            console.log(response.data)
            const result = { ...response.data['0']}
            dispatch(setStation(result))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setStation = (stations) => {
    return {
        type: actionTypes.SET_STATION,
        stations : stations
    }
}

export const getBus = (busId) => {
    return dispatch => {
        axios.get('/bus/select/'+busId)
        .then(response => {
            const result ={...response.data['0']}
            dispatch(setBus(result))
        })

        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setBus = (busEditData) => {
    return {
        type:actionTypes.SET_BUS,
        busEditData : busEditData
    }
}

export const busUpdate = (busId, busData) => {   
    return dispatch => {
        axios.post('/bus/update/'+busId,busData,
        {
            headers : {
                'AUTHENTICATION' : localStorage.getItem('token') 
            }
        }
        )
            .then((response) => {
                console.log(response.data);
                if(response.data != false){
                    
                    dispatch(busUpdateSuccess())
                    alert('Data edited successfully');
                } else {
                    dispatch(busUpdateSuccess())
                    dispatch(show())
                    dispatch(authLogout());
                } 
                
            })
            .catch(err =>  dispatch(showNetworkError()))
            
    }
}

export const busUpdateSuccess = () => {
    return {
        type: actionTypes.BUS_UPDATE_SUCCESS
    }
}

export const updateBus = () => {
    return {
        type: actionTypes.BUS_UPDATE
    }
}


