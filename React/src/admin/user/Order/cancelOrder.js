import React, {Component} from 'react'
import { Form, Button } from 'react-bootstrap';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Alert from 'react-bootstrap/Alert'
import NetworkError from '../../../component/networkError'
import * as actionTypes from '../../../store/actions/index'
import { connect } from 'react-redux';
import Spinner from '../../../spinner/spinner'
import * as action from '../../../store/actions/networkError'

class CancelOrder extends Component {
    constructor() {
        super();
        this.state = {
            id:[],
            redirect : false,
            error :''
        }
        this.onCheck= this.onCheck.bind(this)
        this.onSubmit = this.onSubmit.bind(this);  
    }

    componentDidMount() {
       // localStorage.setItem('url', window.location.pathname);
        this.props.getOrder(this.props.match.params.id)
    }
      onCheck(e){
        if(e.target.checked) {
            console.log(e.target.value)
            this.setState({id : [...this.state.id,(e.target.value)]})
            console.log(this.state.id);
        }  
        if(!e.target.checked) {
            this.setState({
                id: this.state.id.filter(r => r !== e.target.value)
            });
            console.log(this.state.id);
        }    
    }
    
    status() {
        var today = new Date();
        var cancelTime = new Date();
        var dd = today.getDate();  
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if (dd<10) {
            dd='0'+dd;
        } 
        
        if (mm<10) {
            mm='0'+mm;
        } 
        var time = (today.getHours()+':'+today.getMinutes()+':'+today.getSeconds())
        cancelTime.setHours(cancelTime.getHours()+5);

        today = yyyy+'-'+mm+'-'+dd;

        if (this.props.match.params.cancel== 1 || this.props.noOfTicket == 0) {
            return( <div style = {{ color:"red"}}>
            Cancelled
        </div>)
        }

        if ( new Date(today +' '+time) <= new Date(this.props.match.params.journeyDate +' '+this.props.match.params.departureTime) && this.props.noOfTicket  != 0) {
            if(new Date(cancelTime) <= new Date(this.props.match.params.journeyDate +' '+this.props.match.params.departureTime)) {
               
                return(
                    <Button variant = "danger" onClick ={this.onSubmit}  style = {{fontSize: 12, color:'white'}}> Cancel</Button>    
                )
            } else {
              
                return(
                    <Button variant = "danger" disabled  style = {{fontSize: 12, color:'white'}}> Cancel</Button>
                )
                 
            }
            
        }
        else {
            return(<div style = {{ color:"blue"}}>
            Travelled
        </div>)
        }
    }

    onSubmit(e){
        e.preventDefault();
        if(this.state.id.length ) {
            const obj = {
                id: this.state.id,
                bookId: this.props.match.params.id
            }
            this.props.cancelOrder(obj, this.props.match.params.id);
            this.setState({redirect:true})
        }
        else {
            this.setState({error : '*Select passenger to cancel ticket'})
        }
        
    }  
    render() {
        if(this.props.showError) {
            console.log("networkError")
            return <NetworkError/>   
        }
        if(this.props.loading) {
            return  <Spinner  />          
         }
        const success = this.props.success? <Card.Header> 
        <Alert variant= 'success'>
            Order has been cancelled successfully 
        </Alert>
        </Card.Header> : null
        
        return(
          
                   
                        <Col md = {{ span: 5, offset: 4 }}>
                            <Card>
                                {success}
                                
                                <Card.Body>
                                    {
                                    this.props.order.map (function(object) {
                                    return  <Form.Check type ='checkbox'>
                                    <Form.Check.Input  onClick = {this.onCheck} value = {object.id} 
                                            disabled = {(object.del ==1 ? true:false)} 
                                            type = 'checkbox' style = {{ color: 'black' }} 
                                    /> 
                                    <Form.Check.Label> 
                                        Passenger Name : {object.Name}
                                    </Form.Check.Label>
                                    <br/>
                                    Age : {object.Age} <br/>
                                    Seat No : {object.SeatNo}  <br/> <br/>
                                    </Form.Check>    
                                    }.bind(this))
                                    }
 
                                    <div style = {{fontSize:12, color:"red"}}>
                                        {this.state.error}
                                    </div>
                                </Card.Body>
                                <Card.Footer >
                                    <Row>
                                        <Col sm = {8}>  
                                            Rs.{this.props.amount}/- x {this.props.noOfTicket} <br/>
                                            Total amount = Rs.{this.props.amount*this.props.noOfTicket}/-
                                        </Col>
                                        <Col> {this.status()} </Col>
                                    </Row>
                                </Card.Footer>
                            </Card>  
                        </Col>
     
        )  
    }
}

const mapStateToProps = state => {
    return {
        order : state.order.order,
        amount : state.order.amount,
        noOfTicket : state.order.noOfTicket,
        loading : state.order.loading,
        success : state.order.success,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getOrder : (bookId) => dispatch(actionTypes.getOrder(bookId)),
        cancelOrder : (obj, bookId) => dispatch(actionTypes.cancelOrder(obj, bookId)),
        showNetworkError : () => dispatch(action.showNetworkError())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CancelOrder);