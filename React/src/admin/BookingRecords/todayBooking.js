import React, {Component} from 'react';
import axios from 'axios'
import Table from 'react-bootstrap/Table'
import BookingRecord from './bookingRecord'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Image from 'react-bootstrap/Image'
import Spinner from '../../spinner/spinner'
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/networkError'
import NetworkError from '../../component/networkError';

class TodayBooking extends Component 
{
    constructor() {
        super();
        this.onChangeRecord = this.onChangeRecord.bind(this);
        this.handleChange = this.handleChange.bind(this);
        
        this.state = {
            record : "",
            initRecord : [],
            loading : true
        }
    }

    receivedData() {
        axios.get('/bookingRecord/todayBookingDetails')
        .then(response => { 
            this.setState({ initRecord:response.data, loading : false});  
        })
         .catch(err => this.props.showNetworkError())
    }

    componentDidMount() {
        this.receivedData()
    }

    bookingRecordList() { 
        return this.state.initRecord.map (function(object, i){   
            return <BookingRecord obj = {object} key ={i} />; 
        });
    }
    
    print() {
        window.print()
    }

    onChangeRecord(e) {
        this.setState({record:e.target.value})
    }

    handleChange(e) {
            axios.get('/bookingRecord/'+e.target.name+'/'+e.target.value)
            .then(response => {
                if(response.data.length){
                    this.setState({ initRecord:response.data });    
                }
            }) 
            .catch(err => this.props.showNetworkError())
        

        if(e.target.value.length == 1) {
            this.receivedData()
        }
    }
    
    render() {        
        let booking =  ( this.state.loading ? <Spinner/> : <>      
                        <Col sm = "2">
                            <Button variant = "dark" type = "submit"  onClick = {this.print}>
                                Print
                            </Button>
                        </Col>

                   <br/>
                   { this.state.initRecord.length ?
                    <Table responsive hover >
                        <thead>
                            <tr>
                                <th> User Name <Form.Control type="text" placeholder = "User name" 
                                name = "searchUserName"
                                onKeyDown = {this.handleChange} /></th>
                                <th> Bus Name <Form.Control type="text" placeholder = "Bus name" 
                                name = "searchBusName"
                                onKeyDown = {this.handleChange} /></th>
                                <th  > Dep. stn <Form.Control type="text" placeholder = "Departure station" 
                                name = "searchDepartureStation"
                                onKeyDown = {this.handleChange} /></th>
                                <th > Arr. stn<Form.Control type="text" placeholder = "Arrival station" 
                                name = "searchArrivalStation"
                                onKeyDown = {this.handleChange} /></th>
                                <th > Bkd tkts <Form.Control type="text" placeholder = "Ticket count" 
                                name = "searchTicket"
                                onKeyDown = {this.handleChange} /></th>
                                <th> Conf. tkts <Form.Control type="text" placeholder = "Ticket count" 
                                name = "searchConfTicket"
                                onKeyDown = {this.handleChange} /></th>
                                <th> Journey Date <Form.Control type="text" placeholder = "Journey date" 
                                name = "searchJourneyDate"
                                onKeyDown = {this.handleChange} /></th>
                                <th> Booked Date <Form.Control type="text" placeholder = "Booked date"
                                name = "searchBookedDate" 
                                onKeyDown = {this.handleChange} /></th>
                                <th> Status <Form.Control type="text" placeholder = "status"
                                name = "searchStatus" 
                                onKeyDown = {this.handleChange} /></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.bookingRecordList()}
                        </tbody>
                    </Table> : <div style = {{textAlign:"center"}}> 
                <Image
                    alt = ""
                    src = {process.env.REACT_APP_API+"/image/record.png"}
                    height = "190"
                    width = "190"
                />  <br/>
               
                 </div>
            }
                    </> )
                    if(this.props.showError) {
                        console.log("networkError")
                        booking = <NetworkError/>
                    }
                    return booking;
    }
}

const mapStateToProps = state => {
    return {
        showError : state.networkError.showNetworkError
    }
}


const mapDispatchToProps = dispatch => {
    return {
        showNetworkError : () => dispatch(actionTypes.showNetworkError()), 
    }
}

export default connect( mapStateToProps , mapDispatchToProps)(TodayBooking)
