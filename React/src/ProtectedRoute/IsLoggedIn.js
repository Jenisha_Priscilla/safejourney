import React from "react";
import {  Route, Redirect} from "react-router-dom";

const IsLoggedIn = ({ component: Component, ...rest }) => { 
    
  return(
    <Route {...rest} render = {(props) => (
      localStorage.getItem('userData')
        ? <Redirect to = {'/'}/>
        : <Component {...props}/>
    )} />
  )}
 

export default IsLoggedIn