<?php
class Database {
    private $servername = "localhost";
    private $username = "root";
    private $password = "JP1998";
    private $pdo;
    
    public function __construct(){
        try {
            $this->pdo = new PDO("mysql:host=$this->servername;dbname=safeJourney", $this->username, $this->password);
            
            }
        catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
            }
    }

    public function __get($property)
    {
        if(property_exists($this, $property)) {
            return $this->$property;
        }
    }
   
  }