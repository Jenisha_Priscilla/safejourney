import React, {Component} from 'react';
import BookList from "./bookList"
import axios from 'axios';
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import {Redirect} from 'react-router'
import SeatRecord from './seatRecord'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/index'
import * as actionType from '../../../store/actions/networkError'
import NetworkError from '../../../component/networkError'

class Book extends Component 
{
    constructor(){
        super()
        this.state = {
            taskList : [{ Name: "", Age: "", SeatNo: "" }],
            ticket : '',
            add : 1,
            del : 1,
            seats : [],
            ticketError : '',
            seatError : '',
            fieldError : '',
            nameError : '',
            ageError : '',
            redirect : false,
            rate : ''
        }
        this.addNewRow = this.addNewRow.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.clickOnDelete = this.clickOnDelete.bind(this);
        this.validate = this.validate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);   
    }
    
    handleChange(e) {
        if (["Name", "Age", "SeatNo"].includes(e.target.name)) {
            const updatedBus = 
                [...this.state.taskList]
    
            updatedBus[e.target.dataset.id][e.target.name] = e.target.value;
            this.setState({taskList: updatedBus})
            console.log(updatedBus)
        } 
        else { 
            this.setState({ticket: e.target.value, val: this.state.rate * e.target.value})
            localStorage.setItem('url','');
        }
    }

    addNewRow() {
        this.setState((prevState) => ({
            taskList: [...prevState.taskList, { Name: "", Age: "" ,SeatNo:"" }]
        }));   
        this.setState ({
           add: this.state.add+1
        });
    }

    componentDidMount() {
        localStorage.setItem('url', window.location.pathname);
       
        axios.get('/user/getFare/'+this.props.match.params.id)
        .then(response => {
            this.setState({ rate : response.data });  
        })
        .catch(err => this.props.showNetworkError())

        axios.get('/user/getSeat/'+this.props.match.params.id +'/'+ localStorage.getItem ('journeyDate'))
        .then(response => {
            this.setState({ seats : response.data });  
        })
        .catch(err => this.props.showNetworkError())    
    }

    validate () {

        var validate = 0;
        var fieldError = " ";
        var ticketError =" "
        var seatError =" ";
        var nameError = " ";
        var ageError = " ";

        for(var i = 0; i < this.state.taskList.length; i++) {
            if(this.state.taskList[i]['SeatNo'] == "" || this.state.taskList[i]['Name'] == "" || this.state.taskList[i]['Age'] == "") {
                fieldError = '* All fields must be filled';
            }    
        }  

        if(fieldError === " ") { 
            for(var i = 0; i < this.state.taskList.length; i++) {
                var patt1 = /^[a-zA-Z ]+$/g; 
                if (!this.state.taskList[i]['Name'].match(patt1)) {
                    nameError = 'Name must contain only alphabets';
                    console.log(nameError)
                }

                if(this.state.taskList[i]['Age'] <= 0 || this.state.taskList[i]['Age'] > 100 ) {
                    ageError = 'Allowed age is from 1 to 100'
                }

                if(this.state.seats.length >= this.state.taskList[i]['SeatNo'] && this.state.taskList[i]['SeatNo'] > 0) {
                    if(this.state.taskList[i]['SeatNo'] == this.state.seats[this.state.taskList[i]['SeatNo']-1]['seat']) {     
                        validate = validate+1;
                    } 
                }
            }
      
            for(var i = 0; i < this.state.taskList.length; i++) {
                for(var j = i+1; j < this.state.taskList.length; j++) {
                    if(this.state.taskList[i]['SeatNo'] == this.state.taskList[j]['SeatNo']) {
                        seatError = 'seat error'
                    }
                }
            }

            if((this.state.add - this.state.del +1) != this.state.ticket) {
                ticketError = 'Number of tickets and Number of passenger Details are not same' ;
            }
            if(validate != this.state.taskList.length ) {
                seatError = 'seat error' ;
            }
        }

        if(seatError != " "|| ticketError != " " || fieldError!= " " || nameError != " " || ageError != " ") {
            this.setState({seatError, ticketError, fieldError, nameError, ageError})
            return false;
        }
        return true;    
    }
    
    handleSubmit (e) {
        e.preventDefault();
        const isValid= this.validate();
        if(isValid) {
            const obj = {
                userId : localStorage.getItem ('userData'),
                ticket : this.state.ticket,
                JourneyDate : localStorage.getItem ('journeyDate'),
                busid : this.props.match.params.id,
                rate : this.state.rate,
                taskList : this.state.taskList
            }
            axios.post("/user/book", obj
            // {
            //     headers : {
            //         'AUTHENTICATION' : localStorage.getItem('token') 
            //     }
            // }
            )
            .then(() => {
                this.setState({ redirect : true});
            })
            .catch(err => this.props.showNetworkError())    
        }
    }

    clickOnDelete(record) {
        
        this.setState({
            taskList: this.state.taskList.filter(r => r !== record)
        });
        
        this.setState({
            del: this.state.del+1
        });
    }

    Seats() {
        return this.state.seats.map (function(object, i) {
            return <SeatRecord obj = {object} key ={i} />;      
        });
    }
   
    render() {
            let { taskList } = this.state
            console.log(this.props)
            let bookingForm = <>
                        <Card border = "primary" >
                            <Card.Body>
                                <Card.Title>Available seats</Card.Title>
                                {this.Seats()}
                            </Card.Body>
                        </Card>
                    
                    
                    <Form onChange = {this.handleChange} style = {{marginTop: '20px'}}>
                        <Card>
                            <Card.Header> Enter Passenger Details</Card.Header>
                            <Card.Body >
                                <Form.Row >
                                    <Form.Label column sm = {2}> Number of tickets</Form.Label>
                                    <Col sm = {3}>
                                        <Form.Control type = "number"  placeholder = "Enter Ticket Count"
                                        name = "ticket"/> 
                                    </Col>
                                </Form.Row>
                            <br/>
                                <Form.Row >
                                    <Form.Label column sm = {2}> Total Amount</Form.Label>
                                    <Col sm = {3}>
                                        <Form.Control type = "number" value = {this.state.val} disabled />
                                    </Col>
                                </Form.Row>
                             
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.ticketError}
                                </div>     
                                    
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.seatError} 
                                </div>
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.fieldError} 
                                </div>
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.ageError} 
                                </div>
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.nameError} 
                                </div>  

                                <Table responsive style = {{width:'660', marginTop : '20px'}}>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Age</th>
                                            <th>Seat </th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            <BookList 
                                                add = {this.addNewRow} 
                                                delete = {this.clickOnDelete} 
                                                taskList = {taskList} 
                                            />
                                        </tbody>
                                </Table>  
                                        
                             
                                    <Button variant="dark" style = {{margin: "auto", display:"block"}}
                                    onClick = {this.handleSubmit} > Book </Button>
                             
                            </Card.Body> 
                        </Card>      
                    </Form>
                    
                </>
            
            if(this.state.redirect) {
               bookingForm = <Redirect to = '/order'/>;
            }
            if(this.props.showError) {
                console.log("networkError")
                bookingForm = <NetworkError/>
                
            }


            return bookingForm;
       
    }
}

const mapDispatchToProps = dispatch => {
    return {
        authLogout : () => dispatch(actionTypes.authLogout()),
        showNetworkError : () => dispatch(actionType.showNetworkError())
    }
}
const mapStateToProps = state => {
    return {
        showError : state.networkError.showNetworkError
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Book) 

