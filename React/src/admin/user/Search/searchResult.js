import React, { Component } from 'react'
import SearchRecord from './searchRecord'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import SearchDetail from './searchDetail'
import Spinner from '../../../spinner/spinner'
import Image from 'react-bootstrap/Image'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/search'
import * as actionType from '../../../store/actions/auth'
import FilterRecord from './filterRecord'
import { faFilter } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import {Redirect} from 'react-router-dom';
import NetworkError from '../../../component/networkError'
import Pagination from "react-js-pagination";

class SearchResult extends Component 
{
    constructor() {
        super()
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            filterOpen : false
        }
    }
    
    componentDidMount() {
        this.receivedData();
        localStorage.removeItem('url')
    }

    receivedData() {
        const searchData = {
            DepartureStation:localStorage.getItem('departureStation'),
            ArrivalStation:localStorage.getItem('arrivalStation'),
            JourneyDate:localStorage.getItem('journeyDate')     
        };
        this.setState({ searchError : ''})
        this.props.searchSuccess(searchData)
        // this.props.noResult()
    }

    handleFilter = () => {
        this.setState({
            filterOpen: !this.state.filterOpen
        })
    }

    handleClick(e) {
        this.props.pagination(e)
        this.receivedData()
    }

    searchList() {
        return this.props.searchResult.map (function(object, i){
            return <SearchRecord obj = {object} key ={i} />;   
        });
    }

    render() {
        
        if(this.props.stateLost === '1'){
            return <Redirect to = '/'/>
        }
        if(this.props.showError) {
            return <NetworkError/>  
        }
        
        if(this.props.loading) {
           return <Spinner  />
        }
       
        if( this.props.searchResult.length !== 0 ) {
            let filterClass = " ";
            if(this.props.filterOpen){
                filterClass = "d-lg-none d-md-block"
            } else{
                filterClass = "d-none "
            }
            return( 
                <Col style = {{marginTop:15}}>
                    <SearchDetail></SearchDetail>
                    
                    <div className = "d-lg-none d-md-block p-3 m-auto"  onClick = {()=> this.props.filterStatus()}><FontAwesomeIcon icon={ faFilter }/> {' '}Filter </div> 
                        <div className = {filterClass}>
                            <FilterRecord />
                        </div>
                        <div style = {{marginTop:10}}>
                            <Row>
                                <Col md = {3} className = "d-none d-lg-block">
                                    <FilterRecord ></FilterRecord>
                                </Col>
                       
                                <Col md = {9}>
                            
                                    <Card >
                                    {this.searchList()}
                                    </Card>
                                   
                                </Col>
                            </Row>
                            <Row>
                                <Pagination
                                    activePage = {this.props.currentPage}
                                    pageRangeDisplayed = {5}
                                    itemsCountPerPage = {1}
                                    totalItemsCount = {this.props.pageCount}
                                    onChange = {this.handleClick}
                                    disabledClass = 'pagination--disabled'
                                />
                            </Row>  
                        </div>     
                    </Col>
                )
            }
        else {  
            this.props.noResult()
            return (
                
               <div style = {{textAlign:"center", marginTop:50}}> 
                   <Image  
                        height = "250"
                        width = "350" 
                        src = {process.env.REACT_APP_API+"/image/noBus.svg" }
                    />
                    <br/>
                    <div> Sorry! No buses found on your searched route </div>
                </div>
            )         
        }
    }
}

const mapStateToProps = state => {

    return {
        searchResult : state.search.searchResult,
        loading : state.search.loading,
        filterOpen : state.search.filterOpen,
        stateLost : state.search.stateLost,
        pageCount : state.search.pageCount,
        showError : state.networkError.showNetworkError,
        currentPage : state.search.currentPage
    }
}
const mapDispatchToProps = dispatch => { 
    return {
        applyFilter : (filterData) => dispatch(actionTypes.applyFilter(filterData)),
        searchSuccess : (searchData) => dispatch(actionTypes.searchSuccess(searchData)),
        filterStatus : () => dispatch(actionTypes.filterOpen()),
        noResult : () => dispatch(actionTypes.noResult()),
        pagination : (selectedPage) => dispatch(actionTypes.pagination(selectedPage)),
        authLogout : () => dispatch(actionType.authLogout())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);

