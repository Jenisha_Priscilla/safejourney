import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/record'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Badge from 'react-bootstrap/Badge'
import NetworkError from '../../component/networkError'

class CountMonth extends Component 
{
    constructor(){
        super()
        this.state = {
            error : ''
        }
        this.cancelledDetails = this.cancelledDetails.bind(this);
        this.confirmedDetails = this.confirmedDetails.bind(this);
        this.travelledDetails = this.travelledDetails.bind(this);
        this.all = this.all.bind(this);
        this.search = this.search.bind(this);
        this.oneDayRecord = this.oneDayRecord.bind(this);
    }

    componentDidMount() {
        this.props.getCancelledMonthCount(this.props.date);
        this.props.getConfirmedMonthCount(this.props.date);
        this.props.getTravelledCount(this.props.date);
    }

    oneDayRecord() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.history.push('/')
        }
    }

    cancelledDetails() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.getCancelledMonthDetails(this.props.date);
            this.props.history.push('/cancelledMonthDetails')
        }
    }

    confirmedDetails() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.getConfirmedMonthDetails(this.props.date);
            this.props.history.push('/confirmedMonthDetails')
        }
    }
    travelledDetails() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.getTravelledDetails(this.props.date)
            this.props.history.push('/travelledDetails')
        }
    }

    all() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.history.push('/oneMonthRecord')
        }
    }

    search() {
        if(this.props.date == ''){
            this.setState({error : 'Must select a date'})
        } else {
            this.props.getConfirmedMonthDetails(this.props.date);
            this.props.getCancelledMonthDetails(this.props.date);
            this.props.getCancelledMonthCount(this.props.date);
            this.props.getConfirmedMonthCount(this.props.date);
            this.props.getTravelledDetails(this.props.date);
            this.props.getTravelledCount(this.props.date);
            this.props.getOneMonthRecord(this.props.date);
            this.setState({error : ''})
        }
    }
    render() { 
        const style = {
            fontSize : 10,
            color : 'white'
        }

        var date = new Date();
        var ddd = date.getDate();
        var mmm = date.getMonth()+1; 
        var yyy = date.getFullYear();
        if (ddd<10) {
            ddd='0'+ddd;
        } 
        
        if (mmm<10) {
            mmm='0'+mmm;
        } 
        date = yyy+'-'+mmm+'-'+ddd;

        if(this.props.showError) {
            return <NetworkError/>
        }
        
        return(
            <>
                <Row  >
                        <Col xs = {5}>
                            <Form.Control type="date" name = "date"
                                onChange = {this.props.setDate} value = {this.props.date}  max = {date}
                            />
                        </Col>
                        <div style = {{ fontSize:12, color:"red"}}>
                                {this.state.error}
                            </div> 
                        <Col  xs = {1} style = {{paddingRight : '0', paddingLeft : '0'}}>
                            <Button variant = 'dark' onClick = {this.search}  style = {style}>
                                search
                            </Button>
                        </Col>
                        <Col  xs = {1} style = {{paddingRight : '0', paddingLeft : '0'}}>
                            <Button variant = 'secondary' onClick = {this.oneDayRecord} style = {style}>
                                 day 
                            </Button>
                        </Col>
                        <Col xs = {1} style = {{paddingRight : '0', paddingLeft : '0'}}> 
                            <Button variant = 'dark' onClick = {() =>  window.print()} style = {style}>
                                Print
                            </Button>
                        </Col>
                            
                    </Row>
                <br/>
               
                
                    <Row md = {7}>
                       <Col>
                     
                            <Button variant="primary" onClick = {this.all} style = {style}>
                                Show all
                            </Button>
                            {' '}
                            <Button variant="info" onClick = {this.confirmedDetails} style = {style}>
                                Confirmed {' '}
                                <Badge variant="light"> {this.props.confirmedCount}</Badge>
                            </Button>
                            {' '}
                            <Button variant="success" onClick = {this.travelledDetails} style = {style}>
                                Travelled {' '}
                                <Badge variant="light">{this.props.travelledCount}</Badge>
                            </Button>
                            {' '}
                            <Button variant="danger" onClick = {this.cancelledDetails} style = {style}>
                                Cancelled {' '}
                                <Badge variant="light">{this.props.cancelledCount}</Badge>
                            </Button> 
                        </Col>
                    </Row>
           

                        <br/>
                        
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        cancelledCount : state.record.cancelledCount,
        confirmedCount : state.record.confirmedCount,
        travelledCount : state.record.travelledCount,
        date : state.record.date, 
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getConfirmedMonthCount : (date) => dispatch(actionTypes.getConfirmedMonthCount(date)),
        getCancelledMonthCount : (date) => dispatch(actionTypes.getCancelledMonthCount(date)),
        getCancelledMonthDetails : (date) => dispatch(actionTypes.getCancelledMonthDetails(date)),
        getConfirmedMonthDetails : (date) => dispatch(actionTypes.getConfirmedMonthDetails(date)),
        getOneMonthRecord  : (date) => dispatch(actionTypes.getOneMonthRecord (date)),
        setDate : (event) => dispatch(actionTypes.setDate(event)),
        getTravelledDetails : (date) => dispatch(actionTypes.getTravelledDetails(date)),
        getTravelledCount : (date) => dispatch(actionTypes.getTravelledCount(date)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CountMonth))

