import * as actionTypes from '../actions/actionsTypes'

const initialState = {
    userId : null
};
const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.AUTH_SUCCESS:
            return {
                userId : action.userId
            }
        
        case actionTypes.AUTH_LOGOUT:
            return {
                ...state,
                userId : 'logout'
            }
            
        default :
            return state
    }
}
export default authReducer;