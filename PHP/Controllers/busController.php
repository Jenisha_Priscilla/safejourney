<?php
class BusController
{
	private $model;
	function __construct($tile)
	{
		$this->model = new $tile;
	}
	  
	public function delete($id)
  	{
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid->role == "Admin")  {
			return $this->model->delete($id, $isValid->name);
		} else {
			return false;
		}	
	}
	  
	public function view()
	{
		return $this->model->view();
	}

	public function insert()
	{
		require_once './auth/validate.php';
		$isValid = validate();
		if($isValid->role == "Admin")  {
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);
			require_once './class/bus.php';
			$bus = new Bus();
			$bus->__set('routeId',$request->RouteId);
			$bus->__set('busName',$request->busName);
			$bus->__set('busType',$request->busType);
			$bus->__set('plateNo',$request->plateNo);
			$bus->__set('seatsCount',$request->seatCount);
			$bus->__set('fare',$request->fare);
			$bus->__set('departureTime',$request->departureTime);
			$bus->__set('arrivalTime',$request->arrivalTime);
			return $this->model->insert($bus->toArray(), $isValid->name);
		} else {
			return false;
		}
	}

	public function select($id)
	{
		return $this->model->select($id);
	}

	public function searchByBusName($busName) 
	{
		return $this->model->searchByBusName($busName);
	}

	public function update($id)
	{
		require_once './auth/validate.php';
		$isValid = validate();
		// var_dump ($isValid->name);
		if($isValid->role == "Admin") {
			$postdata =file_get_contents("php://input");
			$request = json_decode($postdata);
			require_once './class/bus.php';
			$bus = new Bus();
			$bus->__set('id',$id[0]);
			$bus->__set('routeId',$request->routeId);
			$bus->__set('busName',$request->busName);
			$bus->__set('busType',$request->busType);
			$bus->__set('plateNo',$request->plateNo);
			$bus->__set('seatsCount',$request->seatCount);
			$bus->__set('fare',$request->fare);
			$bus->__set('departureTime',$request->departureTime);
			$bus->__set('arrivalTime',$request->arrivalTime);
			return $this->model->update($bus->__get('id'), $bus->toArray(), $isValid->name);
		} else {
			return false;
		}
	}
}
