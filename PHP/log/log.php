<?php
require_once "./auth/carbon.php";
function logg($admin, $method, ...$arg)
{
    $now = Carbon::now();
    $start = PHP_EOL .str_repeat("-",30). PHP_EOL .$method . PHP_EOL  ;
    $body = PHP_EOL . "Carried out by " . $admin . " in ". $now;

    if(strrchr($method, ":") == ":update") { //update
        $output = implode(', ', array_map(
            function ($v, $k) {
                if(is_array($v)){
                    return $k.'[] = '.implode('&'.$k.'[] = ', $v);
                }else{
                    return $k.' - '.$v;
                }
            }, 
            $arg['1'], 
            array_keys($arg['1'])
        ));
        $details = PHP_EOL . "Update details => ". "Update Id " . $arg['0']. ", " . $output;
        $write = $start . $body . $details ;
        $file = fopen("./log/log.txt","a+");
        fwrite($file, $write);
        fclose($file);
   
    } else if (strrchr($method, ":") == ":delete") { //delete
        $details = PHP_EOL . "Delete details => ". "Delete Id " . $arg['0'];
        $write = $start . $body . $details ;
        $file = fopen("./log/log.txt","a+");
        fwrite($file, $write);
        fclose($file);

    } else { //insert
        $output = implode(', ', array_map(
            function ($v, $k) {
                if(is_array($v)){
                    return $k.'[] = '.implode('&'.$k.'[] = ', $v);
                }else{
                    return $k.' - '.$v;
                }
            }, 
            $arg['0'], 
            array_keys($arg['0'])
        ));
        $details = PHP_EOL . "Insert details => ". $output;
        $write = $start . $body . $details ;
        $file = fopen("./log/log.txt","a+");
        fwrite($file, $write);
        fclose($file);
    }
}