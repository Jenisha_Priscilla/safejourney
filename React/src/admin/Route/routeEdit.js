import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/route'
import Form from 'react-bootstrap/Form'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import NetworkError from '../../component/networkError';

class RouteEdit extends Component 
{
    constructor() {
        super();
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            stationError : '',
            fieldError : ''
        }
    }

    componentDidMount() {
        this.props.getRoute(this.props.id);
    }

    validate() {
        var fieldError = " ";
        var stationError = " ";
        
        if (this.props.route.ArrivalStation == '' || this.props.route.DepartureStation == '') {
            fieldError = 'All fields must be filled';
        }
        if (fieldError === " ") {
            var patt1 = /^[a-zA-Z]+$/g; 
            if (!this.props.route.DepartureStation.match(patt1)) {
                stationError = 'Must contain only alphabets';
            }
            if (!this.props.route.ArrivalStation.match(patt1)) {
                stationError = 'Must contain only alphabets';
            }
        }
        if (fieldError != " " || stationError != " " ) {
            this.setState({fieldError, stationError})
            return false;
        }
        return true;
    }
   
    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const routeData = {
                id:this.props.id,
                departureStation:this.props.route.DepartureStation,
                arrivalStation:this.props.route.ArrivalStation
            };
            this.props.onEdit();
            this.props.routeUpdate(routeData)
        }
    }

    render() {
        if(this.props.showError) {
            console.log("networkError")
            return  <NetworkError/>
        }

        return (
            <tr >
                <td>           
                   {this.props.id}
                </td>
                  
                <td > 
                    <Form.Control type = "text" name = "DepartureStation"
                        value= {this.props.route.DepartureStation}
                        onChange = {this.props.onRouteChange}
                    />
                <div style ={{ fontSize:10, color:"red"}}>
                    {this.state.fieldError}
                </div> 
                              
                <div style = {{ fontSize:12, color:"red"}}>
                    {this.state.stationError}
                </div>     
                </td>
                <td >
                    <Form.Control type = "text" name = "ArrivalStation"
                        value = {this.props.route.ArrivalStation}
                        onChange = {this.props.onRouteChange}
                    />
                </td>
                <td > 
                    <div variant="dark" onClick = {this.onSubmit} > 
                        <FontAwesomeIcon icon = {faCheck} size = "sm" />
                    </div>
                </td>
            </tr>      
            )
        }  
    }
    
    const mapStateToProps = state => {
        return {
            route : state.route.routeEdit,
            showError : state.networkError.showNetworkError
        }
    }
    
    const mapDispatchToProps = dispatch => {
        return {
            getRoute : (routeId) => dispatch(actionTypes.getRoute(routeId)), 
            onRouteChange : (e) => dispatch(actionTypes.handleRouteEdit(e)),
            routeUpdateSuccess : () => dispatch(actionTypes.routeUpdateSuccess()),
            routeUpdate : (routeData) => dispatch(actionTypes.routeUpdate(routeData))
        }
    }

export default connect(mapStateToProps, mapDispatchToProps)(RouteEdit) 