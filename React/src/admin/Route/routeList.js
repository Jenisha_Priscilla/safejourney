import React, { Component } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faTrash} from "@fortawesome/free-solid-svg-icons";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';
import RouteEdit from './routeEdit';
import * as actionTypes from '../../store/actions/index'
import * as actionType from '../../store/actions/networkError'
import NetworkError from '../../component/networkError';

class RouteList extends Component
{
    constructor() {
        super();
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.state = {
            redirect: false,
            delete : false
        }
    }
    
    edit() {
        this.setState ({
            redirect : !this.state.redirect
        })
        console.log('onedit')
        this.props.onUpdate();
    }

    delete() {
        axios.get('/route/delete/'+this.props.obj.Id
        // {
        //     headers : {
        //         'AUTHENTICATION' : localStorage.getItem('token') 
        //     }
        // }
        )
        .then((response)=> {
            if(response.data == false){
                this.props.show()
                this.props.authLogout()
            } else {
                this.setState({ redirect: false, delete: true})
                
            }
            
        })
        .catch(err => this.props.showNetworkError())
    }
    
    render() {

        let result = ( <tr>
                        <td>
                            {this.props.obj.Id}
                        </td>
                        <td>
                            {this.props.obj.DepartureStation}
                        </td>
                        <td>
                            {this.props.obj.ArrivalStation}
                        </td>
                        { this.props.update == 1  ?
                             <td style = {{width: 2}}>
                             <div onClick =  {()=> this.edit(this.props.obj.Id)}>  
                                 <FontAwesomeIcon icon = {faPen} size = "sm" />
                             </div>
                             </td> :null
                        }
                        <td  style = {{width: 2}}>
                            <div variant = "danger" onClick = {this.delete} > 
                                <FontAwesomeIcon icon = {faTrash} size = "sm" /> 
                            </div>
                        </td>
                    </tr> )
                
       
        if (this.state.redirect && !this.props.updateSuccess ) {
            result =  <RouteEdit id = {this.props.obj.Id}  onEdit = {this.edit}/>
        }
       
        if (this.state.delete) {
            result =  <Redirect to={'/route'}/>
        }

        if(this.props.showError) {
            result = <NetworkError/>
        }
        return result;
        
    }
}

const mapStateToProps = state => {
    return {
        update: state.route.update,
        updateSuccess : state.route.updateSuccess,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdate : () => dispatch(actionTypes.updateRoute()),
        authLogout : () => dispatch(actionTypes.authLogout()),
        show : () => dispatch(actionTypes.show()),
        showNetworkError : () => dispatch(actionType.showNetworkError()), 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RouteList)




