import React, {  useState, useEffect } from "react";
import OrderRecord from './orderRecord'
import NoTripModal from '../../../component/noTripModal'
import NetworkError from '../../../component/networkError'
import * as actionTypes from '../../../store/actions/order'
import * as actionType from '../../../store/actions/auth'
import { connect } from 'react-redux';
import Spinner from '../../../spinner/spinner'
import Card from 'react-bootstrap/Card'
import Col from 'react-bootstrap/Col'
import './pagination.css'
import Pagination from "react-js-pagination";

const Order = (props) => {

    const [currentPage, setCurrentPage] = useState(1) 
    useEffect(() => {
        localStorage.removeItem('url')
        if(localStorage.getItem ('userData')){
           receivedData()
        } 
    },[])

    const receivedData = () => {
        props.getOrders(localStorage.getItem ('userData'),currentPage)
    }

    const handleClick = (e) => {
        setCurrentPage(e)
        receivedData()
    }

    const orderList = () => {
        return props.order.map (function(object, i) {
            return <OrderRecord obj = {object} key ={i} />;      
          });
    }

 
       let order =  
           <Col md={{ span: 7, offset: 3}}>
                <Card style = {{marginBottom : 10}}>
                     {orderList()} 
                </Card>
              
                <Pagination
                    activePage = {currentPage}
                    pageRangeDisplayed = {5}
                    itemsCountPerPage = {1}
                    totalItemsCount = {props.pageCount}
                    onChange = {handleClick}
                    disabledClass = 'pagination--disabled'
                />
                
           </Col>     
    
        if(props.redirect) {
            if(props.order.length === 0 ) {
                order = <NoTripModal/>
            }
        }

        if(props.loading) {
            order = <Spinner  />      
        }
        if(props.showError) {
            console.log("networkError")
            order =  <NetworkError/>   
        }
        
       return order;   
   
}

const mapStateToProps = state => {
    return {
        order : state.order.order,
        redirect : state.order.redirect,
        loading : state.order.loading,
        pageCount : state.order.pageCount,
        currentPage : state.order.currentPage,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getOrders : (userId,currentPage) => dispatch(actionTypes.getOrders(userId,currentPage)),
        authLogout : () => dispatch(actionType.authLogout()),
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Order)