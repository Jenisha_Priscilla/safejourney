<?php
class BusBookingDetails
{
    private $userId;
    private $busId;
    private $noOfTickets;
	private $journeyDate;
    private $rate;

    public function __set($property, $value)
    { 
        require_once './validation/validateProperty.php';
        $isValid = valid($value);
        if($isValid) {
            if(property_exists($this, $property)) {
                $this->$property = $value;
            }
            return $this;
        }
    }

    public function __get($property)
    {
        if(property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    public function toArray () 
	{	
        return [
            "userId" => $this->__get('userId'),
            "busId" => $this->__get('busId'),
			"noOfTickets" => $this->__get('noOfTickets'),
            "journeyDate" => $this->__get('journeyDate'),
            "departureTime" => $this->__get('departureTime'),
            "rate" => $this->__get('rate')
        ];
    }
}
