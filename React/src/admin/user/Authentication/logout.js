import React, {useEffect } from 'react';
import {Redirect} from 'react-router'
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions/index'

const Logout = (props) => {

  useEffect(() => {
    localStorage.clear()
    props.authLogout()
},[])

  return (<Redirect to={'/'}/>)
  
}

const mapDispatchToProps = dispatch => {
  return {
      authLogout : () => dispatch(actionTypes.authLogout()), 
  }
}

export default connect(null, mapDispatchToProps)(Logout)
