<?php
require_once './config/config.php';

if(isset($_SERVER['PATH_INFO'])){
    $path= $_SERVER['PATH_INFO'];
    $path_split = explode('/', ltrim($path));
} else{
    $path_split = '/';
}

if($path_split === '/'){
   
	require_once __DIR__.'/Models/indexModel.php';
    require_once __DIR__.'/Controllers/indexController.php';
	require_once __DIR__.'/config/database.php';
        $req_model = new IndexModel();
        $req_controller = new IndexController($req_model);
				
		        // $model = $req_model;
		        // $controller = $req_controller;

		    
		        // $ModelObj = new $req_model;
		        // $ControllerObj = new $req_controller($req_model);

		   
	            	print $ControllerObj->index();
	            
}else{
 
    	$req_controller = $path_split[1];

		$req_model = $path_split[1];

		$req_method = isset($path_split[2])? $path_split[2] :'';

		$req_param = array_slice($path_split, 3);

		$req_controller_exists = __DIR__.'/Controllers/'.$req_controller.'Controller.php';

		if (file_exists($req_controller_exists))
		{
			require_once __DIR__.'/Models/'.$req_model.'Model.php';
	    	require_once __DIR__.'/Controllers/'.$req_controller.'Controller.php';
			require_once __DIR__.'/config/database.php';

	        $model = ucfirst($req_model).'Model';
			$controller = ucfirst($req_controller).'Controller';
			
	        //$ModelObj = new $model($pdo);
	        $ControllerObj = new $controller($model);

			$method = $req_method;
			
	        if ($req_method != '')
            {
                print $ControllerObj->$method($req_param);
            }
            else
            {
				print $ControllerObj->index();
            }
        }
        else
		{
			die('404 - The file - - not found');
		}
}

?>
