import React from 'react'

const OneMonthRecordResult = (props) => {
  
        return (
            <tr>
                <td>
                    {props.obj.BusName}
                </td>
                <td>
                    {props.obj.BusType}
                </td>
                <td>
                    {props.obj.PlateNo}
                </td>
                <td>
                    {props.obj.DepartureStation}
                </td>
                <td>
                    {props.obj.ArrivalStation}
                </td>
                <td>
                    {props.obj.Confirmed}
                </td>
                <td>
                    {props.obj.Cancelled}
                </td>
            </tr>
        )

} 
export default OneMonthRecordResult