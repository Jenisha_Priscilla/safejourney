import React from 'react'
import { connect } from 'react-redux';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faLongArrowAltRight } from "@fortawesome/free-solid-svg-icons";
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

const SearchDetail = () => {
        return(
            <React.Fragment>  
                <Row>
                    <Col md={{ span: 5, offset: 5 }} style = {{fontSize : 18, marginTop : 10, paddingRight : 'auto'}}>
                        {localStorage.getItem('departureStation') }{'  '} 
                        <FontAwesomeIcon icon = {faLongArrowAltRight} size = "1x" />
                        {"  "} {localStorage.getItem('arrivalStation')} {'  |  '} {localStorage.getItem('journeyDate')}
                    </Col>
                </Row>
            </React.Fragment>
        )
}

const mapStateToProps = state => {
    console.log(state)
    return {
        search : state.search.search
    }
}
export default connect(mapStateToProps, null)(SearchDetail);
