import React, {useEffect }  from 'react';
import OneMonthRecordResult from './oneMonthRecordResult'
import Table from 'react-bootstrap/Table'
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/record'
import CountMonth from './countMonth'
import Spinner from '../../spinner/spinner'
import Image from 'react-bootstrap/Image'
import Pagination from "react-js-pagination";

const OneMonthRecord = (props) => {

    useEffect(() => {
        receivedData()
        props.setCurrentPage()
    },[])

    const receivedData = () => {
        props.getOneMonthRecord(props.date);
    }

    const handleClick = (e) => {
        props.pagination(e)
        receivedData()
    }
    
    const bookingRecordList = () => {
        return props.oneMonthRecord.map (function(object, i){   
            return <OneMonthRecordResult obj = {object} key ={i} />; 
        });
    }

        let record = props.loading ? <Spinner/> : props.oneMonthRecord.length?  
       
            <Table responsive hover  >
            <thead>
                <tr>
                    <th> Bus Name </th>
                    <th> Bus Type</th>
                    <th> PlateNo.</th>
                    <th> Departure station </th>
                    <th> Arrival station </th>
                    <th> Confirmed tkts </th>
                    <th> Cancelled tkts </th>
                </tr>
            </thead>
            <tbody>
                {bookingRecordList()}
            </tbody>
        </Table> 
           : <div style ={{textAlign:"center",marginTop:40}}> 
           <Image
               alt = ""
               src = {process.env.REACT_APP_API+"/image/record.png"}
               height = "190"
               width = "190"
           />  <br/>
           
                 </div>
        return (
            <>
                <CountMonth></CountMonth>
                <h5 align = "center">  One month record </h5>
                    {record}
                <Pagination
                        activePage={props.currentPage}
                        pageRangeDisplayed={5}
                        itemsCountPerPage={1}
                        totalItemsCount={props.pageCount}
                        onChange={handleClick}
                        disabledClass = 'pagination--disabled'
                />
            </>
        )
}

const mapStateToProps = state => {
    return {
        oneMonthRecord : state.record.record,
        date : state.record.date,
        loading : state.record.loading,
        pageCount : state.record.pageCount,
        currentPage : state.record.currentPage
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getOneMonthRecord  : (date) => dispatch(actionTypes.getOneMonthRecord(date)),
        setCurrentPage : () => dispatch(actionTypes.setCurrentPage()),
        pagination : (selectedPage) => dispatch(actionTypes.pagination(selectedPage)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OneMonthRecord) 
