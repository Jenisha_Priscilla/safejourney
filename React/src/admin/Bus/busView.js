import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import Col from 'react-bootstrap/Col'
import BusList from './busList'
import { connect } from 'react-redux';
import { Redirect } from "react-router-dom";
import Form from 'react-bootstrap/Form'
import Spinner from '../../spinner/spinner'
import * as actionType from '../../store/actions/networkError'
import NetworkError from '../../component/networkError'

const BusView  = (props)=> {
    const [busData, setBusData] = useState([])
    const [search, setSearch] = useState('')
    const [loading, setLoading] = useState(true)
   
    useEffect(() => {
        if(search.length > 1) {
            searchData()
        } else {
            receivedData()
        }
    }, [props.updateSuccess])
    
    const receivedData = () => {
        axios.get('/bus/view')
        .then(response => {
            setBusData(response.data)
            setLoading(false)
            
        })
        .catch(err => props.showNetworkError())
    }

    const searchData = () => {
        axios.get('/bus/searchByBusName/'+search)
            .then(response => {
                if(response.data) {
                    setBusData(response.data)
                }    
            }) 
            .catch(err => props.showNetworkError())
    }

    const handleChange = (e) => {
        console.log(e.target.value.length)
            searchData()
            setSearch(e.target.value)
        
        if(e.target.value.length == 1) {
            receivedData()
        }
    }

    const busList = () => {
        return busData.map (function(object, i) {
            return <BusList obj = {object} key ={i} />;        
      });
    }
    
    if(props.show) {
        return <Redirect to = './login'/>
    }

    if(props.showError) {
        console.log("networkError")
        return  <NetworkError/>
    }

    if(loading) {
        return <Spinner/>
    }

    
            return (
                <>
                    <Col md={{ span: 4, offset: 7 }}>
                        <Form.Control type = "text" placeholder = "Search by bus name" 
                           onKeyDown = {handleChange} 
                        />
                    </Col>
                    <br/>
                   <Table responsive hover style = {{width:'1450'}}>
                        <thead>
                            <tr>
                                <th style = {{width : 80}}> R.Id </th>
                                <th> Dep. stn</th>
                                <th> Arr. stn</th>
                                <th style = {{width : 150}}> Bus Name </th>
                                <th style = {{width : 170}}> Bus Type</th>
                                <th style = {{width : 170}}> Plate Number </th>
                                <th style = {{width : 80}}> Seats </th>
                                <th style = {{width : 100}}> Fare </th>
                                <th style = {{width : 120}}> Dep. Time</th>
                                <th style = {{width : 120}}> Arr. Time</th>
                                <th colSpan = "2"> Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {busList()} 
                        </tbody>
                    </Table>
                </>
            )
       
}

const mapStateToProps = state => {
console.log(state)
    return {
        updateSuccess: state.bus.updateSuccess,
        show : state.inValidModal.show,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        showNetworkError : () => dispatch(actionType.showNetworkError()), 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BusView)

