import React from 'react';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import { faPhoneAlt } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faFacebook} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 

const Footer = () => {

    const mystyle = {
      width: "100%"
    };

    return(
      <footer style = {mystyle}>
      <Card
        bg = "secondary"
        text = "light"
        
      >
        <Card.Body>
          {/* <Container> */}
            <Row >
              <Col md={8}  >
                <strong> <h5> Safe Journey </h5></strong>
                <p> 
                  Safe Journey is the online bus ticket booking service trusted by many happy customers. 
                  Safe Journey offers bus ticket booking through its website {' '}
                  <span onClick = {()=> {window.open ('https://www.facebook.com/jenisha.briskela',"_blank")}}>
                    <FontAwesomeIcon icon = {faFacebook} size = "lg" /> 
                  </span>
                </p>
              </Col>
              
              <Col md={4} >
                <Row>
                  <Col>
                    <p><a href = 'tel:+9360049674' style = {{color: 'white'}}>  <FontAwesomeIcon icon = {faPhoneAlt} size = "lg" />  {' '} 9360049674 </a></p> 
                  </Col>
                </Row>
             
                <Row>
                  <Col>
                      <p><a href = 'mailto:jenishabriskela@gmail.com' style = {{color: 'white'}}> <FontAwesomeIcon icon = {faEnvelope} size = "lg" />{' '}jenishabriskela@gmail.com </a></p> 
                  </Col>
                </Row>
              </Col>
            </Row>
          {/* </Container> */}
        </Card.Body>
      </Card>
      </footer>
    )
  
}

export default Footer