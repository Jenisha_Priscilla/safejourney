import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/index'
import { Redirect } from 'react-router-dom';
import NetworkError from '../../component/networkError';

class BusInsert extends Component
{
    constructor() {
        super();
        this.onSubmit = this.onSubmit.bind(this);
        this.validate = this.validate.bind(this);
        this.state = {
            fieldError : '',
            busNameError : '',
            busTypeError : '',
            plateNoError : '',
            routeIdError : '',
            fareError : '',
            seatCountError : '',
            departureTimeError : '',
            arrivalTimeError : ''
        }
    }

    componentDidMount() {
        this.props.onInitRouteIds();
    }

    validate() {
        var val = 0 ;
        var fieldError = " ";
        var plateNoError = " ";
        var busNameError = " ";
        var routeIdError = " ";
        var busTypeError = " ";
        var fareError = " ";
        var seatCountError = " ";
        var departureTimeError = " ";
        var arrivalTimeError = " ";
      
        if (this.props.bus.RouteId == '' || this.props.bus.BusName == '' || this.props.bus.BusType == '' || 
        this.props.bus.Fare == '' || this.props.bus.PlateNo == '' || this.props.bus.SeatsCount == '' || 
        this.props.bus.DepartureTime == '' || this.props.bus.ArrivalTime == ''|| this.props.bus.RouteId == undefined || this.props.bus.BusName == undefined|| this.props.bus.BusType == undefined || 
        this.props.bus.Fare == undefined || this.props.bus.PlateNo == undefined || this.props.bus.SeatsCount == undefined || 
        this.props.bus.DepartureTime == undefined || this.props.bus.ArrivalTime == undefined) {
            fieldError = '*All fields must be filled';
        }
        if(fieldError === " ") {
            var patt1 = /^[a-zA-Z ]+$/g; 
            if (!this.props.bus.BusName.match(patt1)) {
                busNameError = 'Must contain only alphabets';
            }
            if (!this.props.bus.BusType.match(patt1)) {
                busTypeError = 'Must contain only alphabets';
            }
            var patt2 = /[A-Z]{2}\s[0-9|\s]{1,2}\s[A-Z|s]{1,2}\s[0-9]{1,4}/g; 
            if (!this.props.bus.PlateNo.match(patt2)) {
                plateNoError = 'Enter valid plate number';
            }
            var patt3 = /^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/
            if (!this.props.bus.DepartureTime.match(patt3)) {
                departureTimeError = 'Enter valid time';
            }
            if (!this.props.bus.ArrivalTime.match(patt3)) {
                arrivalTimeError = 'Enter valid time';
            }
            if (this.props.bus.SeatsCount <= 0) {
                seatCountError = 'Enter valid seat count';
            }
            if (this.props.bus.Fare <= 0) {
                fareError = 'Enter valid fare';
            }
            for (var i = 0; i < this.props.routeIds.length; i++) {
                if (this.props.bus.RouteId == this.props.routeIds[i]['Id']) {
                    val = val+1;
                }
            } 
            if (val === 0) {
                routeIdError = 'Enter valid route id';
            }
        }

        if (fieldError != " " || busNameError != " " || busTypeError != " " || departureTimeError != " " ||
         arrivalTimeError != " " || plateNoError != " " || routeIdError!= " " || fareError != " " || seatCountError != " ") {
            this.setState({fieldError, busNameError, busTypeError,departureTimeError, arrivalTimeError, plateNoError, routeIdError, fareError, seatCountError})
            return false;
        }

        return true;
    }

    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate();
        console.log(isValid)
        if (isValid) {
            const busData = {
                RouteId:this.props.bus.RouteId,
                busName:this.props.bus.BusName,
                busType:this.props.bus.BusType,
                plateNo:this.props.bus.PlateNo,
                seatCount:this.props.bus.SeatsCount,
                fare: this.props.bus.Fare,
                departureTime : this.props.bus.DepartureTime,
                arrivalTime : this.props.bus.ArrivalTime
            };
            console.log(busData)
            this.props.onBusInsert(busData);
        }    
    }

    render() {
        const style = {
            fontSize : 12,
            color : 'red'
        }
        if(this.props.redirect) {
           return <Redirect to = '/viewBus'/>
        }
        
        if(this.props.showError) {
            console.log("networkError")
            return  <NetworkError/>
        }

        if(this.props.show) {
            return <Redirect to = './login'/>
        }

        return (
            <>
                <Col md = {{ span: 4, offset: 4 }}>
                    <h5 align = "center"> Insert Bus </h5> 
                </Col>
               
            
                            <Form onChange = {this.props.onBusChange} style = {{borderStyle: "solid" , borderColor: "black", padding: 20}}>
                                <div style = {style}>
                                    {this.state.fieldError}
                                </div> 

                                <Form.Row>
                                    <Form.Label column lg = {3}> Bus Name </Form.Label>
                                    <Col>
                                        <Form.Control type = "text" name = "BusName"
                                        placeholder = "xxx Travels" value = {this.props.bus.BusName}
                                        />
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.busNameError}
                                </div>  
                                <br/>   

                                <Form.Row>
                                    <Form.Label column lg={3}> Bus type </Form.Label>
                                    <Col>
                                        <Form.Control required type = "text" name = "BusType"
                                        placeholder = "Bus type" 
                                        value = {this.props.bus.BusType}
                                        />
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.busTypeError}
                                </div>  
                                <br/>      

                                <Form.Row>
                                    <Form.Label column lg = {3}> Route Id </Form.Label>
                                    <Col>
                                        <Form.Control type = "number" name = "RouteId"
                                        placeholder = "Refer Route list"
                                        value = {this.props.bus.RouteId}
                                        />
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.routeIdError}
                                </div>  
                                <br/>   

                                <Form.Row>
                                    <Form.Label column lg = {3}> Departure time </Form.Label>
                                    <Col>
                                        <Form.Control type = "text" name = "DepartureTime"
                                        placeholder = "Time in 24 H format"
                                        value = {this.props.bus.DepartureTime}
                                        />
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.departureTimeError}
                                </div> 
                                <br/> 

                                <Form.Row>
                                    <Form.Label column lg = {3}> Arrival time </Form.Label>
                                    <Col>
                                        <Form.Control type = "text" name = "ArrivalTime"
                                        placeholder = "Time in 24 H format"
                                        value = {this.props.bus.ArrivalTime}
                                        />
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.arrivalTimeError}
                                </div> 
                                <br/>   

                                <Form.Row>
                                    <Form.Label column lg = {3}> Plate No. </Form.Label>
                                    <Col>
                                        <Form.Control type = "text" name = "PlateNo"
                                        placeholder = "Enter bus plate No."
                                        value = {this.props.bus.PlateNo}
                                        />
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.plateNoError}
                                </div>
                                <br/>  

                                <Form.Row>
                                    <Form.Label column lg = {3}> Seat count</Form.Label>
                                    <Col>
                                        <Form.Control type = "number" name = "SeatsCount"
                                        placeholder = "No. of seats in bus"
                                        value = {this.props.bus.SeatsCount}
                                        />
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.seatCountError}
                                </div>   
                                <br/> 

                                <Form.Row>
                                    <Form.Label column lg = {3}> Fare </Form.Label>
                                    <Col>
                                        <Form.Control type = "number" name = "Fare"
                                        placeholder = "Enter amount"
                                        value = {this.props.bus.Fare}
                                        onChange = {this.handleChange}/>
                                    </Col>
                                </Form.Row>
                                <div style = {style}>
                                    {this.state.fareError}
                                </div>   
                               
                                <div style = {{marginTop:40, textAlign: "center"}}> 
                                    <Button variant = "dark" onClick = {this.onSubmit}> Add </Button>
                                </div>
                            </Form>
                            
                    </>
                  
        )
    }
}

const mapStateToProps = state => {
    return {
        bus : state.bus.bus,
        routeIds : state.bus.routeIds,
        redirect : state.bus.redirect,
        show : state.inValidModal.show,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onBusChange : (e) => dispatch(actionTypes.handleBusChange(e)),
        onInitRouteIds : () => dispatch(actionTypes.initRouteIds()),
        onBusInsert : (busData) => dispatch(actionTypes.busInsert(busData)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BusInsert)