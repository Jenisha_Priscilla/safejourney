import * as actionTypes from './actionsTypes'
import axios from 'axios';
import { showNetworkError } from './networkError';

export const getOneDayRecord = (date) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/bookingRecord/oneDayRecord/'+date)
        .then(response => {
            console.log(response.data)
            dispatch(setDayRecord(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const getOneMonthRecord = (date) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/bookingRecord/oneMonthRecord/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setRecord(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setRecord = (record) => {
    return {
        type : actionTypes.RECORD,
        record : record
    }
}

export const setDayRecord = (record) => {
    return {
        type : actionTypes.DAY_RECORD,
        dayRecord : record
    }
}

export const getCancelledCount = (date) => {
    return dispatch => {
        axios.get('/bookingRecord/cancelledBooking/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setCancelledCount(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const getCancelledMonthCount = (date) => {
    return dispatch => {
        axios.get('/bookingRecord/cancelledMonthCount/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setCancelledCount(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setCancelledCount = (cancelledCount) => {
    return {
        type : actionTypes.CANCELLED_COUNT,
        cancelledCount : cancelledCount
    }
}

export const getConfirmedCount = (date) => {
    return dispatch => {
        axios.get('/bookingRecord/confirmedBooking/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setConfirmedCount(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const getConfirmedMonthCount = (date) => {
    return dispatch => {
        axios.get('/bookingRecord/confirmedMonthCount/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setConfirmedCount(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setConfirmedCount = (confirmedCount) => {
    return {
        type : actionTypes.CONFIRMED_COUNT,
        confirmedCount : confirmedCount
    }
}

export const getCancelledDetails = (date) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/bookingRecord/cancelledDetails/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setCancelledDetails(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const getCancelledMonthDetails = (date) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/bookingRecord/cancelledMonthDetails/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setCancelledDetails(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setCancelledDetails = (cancelledDetails) => {
    return {
        type : actionTypes.SET_CANCELLED_DETAILS,
        cancelledDetails : cancelledDetails
    }
}


export const getConfirmedDetails = (date) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/bookingRecord/confirmedDetails/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setConfirmedDetails(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const getConfirmedMonthDetails = (date) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/bookingRecord/confirmedMonthDetails/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setConfirmedDetails(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setConfirmedDetails = (confirmedDetails) => {
    return {
        type : actionTypes.SET_CONFIRMED_DETAILS,
        confirmedDetails : confirmedDetails
    }
}


export const setDate = (event) => {
    return {
        type : actionTypes.SET_DATE,
        event : event
    }
}

export const getTravelledDetails = (date) => {
    return dispatch => {
        dispatch(fetchRequest())
        axios.get('/bookingRecord/travelledDetails/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setTravelledDetails(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setTravelledDetails = (travelledDetails) => {
    return {
        type : actionTypes.SET_TRAVELLED_DETAILS,
        travelledDetails : travelledDetails
    }
}

export const getTravelledCount = (date) => {
    return dispatch => {
        axios.get('/bookingRecord/travelledCount/'+ date)
        .then(response => {
            console.log(response.data)
            dispatch(setTravelledCount(response.data))
        })
        .catch(err =>  dispatch(showNetworkError()))
    }
}

export const setTravelledCount = (travelledCount) => {
    return {
        type : actionTypes.TRAVELLED_COUNT,
        travelledCount : travelledCount
    }
}

export const fetchRequest = () => {
    return {
        type : actionTypes.FETCH_REQUEST
    }
}

export const pagination = (selectedPage) => {
    return {
        type : actionTypes.PAGINATION,
        selectedPage : selectedPage
    }
}

export const setCurrentPage = () => {
    return {
        type : actionTypes.SET_CURRENT_PAGE
    }
}

