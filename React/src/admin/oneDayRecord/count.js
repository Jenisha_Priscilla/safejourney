import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/record'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Badge from 'react-bootstrap/Badge'
import NetworkError from '../../component/networkError'

class Count extends Component {
    constructor(){
        super()
        this.state = {
            error : ''
        }
        
        this.cancelledDetails = this.cancelledDetails.bind(this);
        this.confirmedDetails = this.confirmedDetails.bind(this);
        this.all = this.all.bind(this);
        this.oneMonthRecord = this.oneMonthRecord.bind(this);
        this.search = this.search.bind(this);
    }

    componentDidMount() {
        this.props.getCancelledCount(this.props.date);
        this.props.getConfirmedCount(this.props.date);
    }

    cancelledDetails() {
        if(this.state.error === '' && this.props.date !== ''){
            this.props.getCancelledDetails(this.props.date)
            this.props.history.push('./cancelledDetails')
            
        }
    }

    confirmedDetails() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.getConfirmedDetails(this.props.date)
            this.props.history.push('./confirmedDetails')
        }   
    }

    all() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.history.push('./')
        }
    }
   

    oneMonthRecord() {
        if(this.state.error === '' && this.props.date !== '') {
            this.props.history.push('./oneMonthRecord')
        }

    }

    search() {
        if(this.props.date === ''){
            this.setState({error : 'Must select a date'})
        } else {
            this.props.getConfirmedDetails(this.props.date);
            this.props.getCancelledDetails(this.props.date);
            this.props.getCancelledCount(this.props.date);
            this.props.getConfirmedCount(this.props.date);
            this.props.getOneDayRecord(this.props.date);
            this.setState({error : ''})
        }
    }


    render() { 
        const style = {
            fontSize : 10,
            color : 'white'
        }
        var date = new Date();
        var ddd = date.getDate();
        var mmm = date.getMonth()+1; 
        var yyy = date.getFullYear();
        if (ddd<10) {
            ddd='0'+ddd;
        } 
        
        if (mmm<10) {
            mmm='0'+mmm;
        } 
        date = yyy+'-'+mmm+'-'+ddd;

        if(this.props.showError) {
            return <NetworkError/>
        }

        return(
            <>
                    <Row > 
                        <Col xs = {5}>
                            <Form.Control type = "date" name = "date"
                                onChange = {this.props.setDate} value = {this.props.date}  max = {date}
                            />
                        </Col>
                        <div style = {{ fontSize:12, color:"red"}}>
                                {this.state.error}
                            </div> 
                        <Col  xs = {1} > 
                            <Button variant = 'dark' onClick = {this.search} style = {style}>
                                search
                            </Button>
                        </Col>
                        <Col xs = {1} > 
                            <Button variant = 'dark' onClick = {() =>  window.print()} style = {style}>
                                Print
                            </Button>
                        </Col>
                    </Row>
                <br/>
                
 
                    <Row md = {7}>
                       <Col>
                            <Button variant="info" style = {style} onClick = {this.all} >
                                Show all
                            </Button>
                            {' '}
                            <Button variant = "success" style = {style} onClick = {this.confirmedDetails} >
                                Confirmed {' '}
                                <Badge variant="light"> {this.props.confirmedCount}</Badge>
                            </Button>
                            {' '}
                            <Button variant = "danger" onClick = {this.cancelledDetails} style = {style}>
                                Cancelled {' '}
                                <Badge variant="light">{this.props.cancelledCount}</Badge>
                            </Button>
                            {' '}
                            <Button variant = 'secondary' onClick = {this.oneMonthRecord} style = {style}>
                                1 month record
                            </Button>
                        </Col>
                    </Row>
           
                    <br/>   
            </>
        )
    }
}
const mapStateToProps = state => {
    return {
        cancelledCount : state.record.cancelledCount,
        confirmedCount : state.record.confirmedCount,
        date : state.record.date,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getConfirmedCount : (date) => dispatch(actionTypes.getConfirmedCount(date)),
        getCancelledCount : (date) => dispatch(actionTypes.getCancelledCount(date)),
        getOneDayRecord : (date) => dispatch(actionTypes.getOneDayRecord(date)),
        setDate : (event) => dispatch(actionTypes.setDate(event)),
        getConfirmedDetails : (date) => dispatch(actionTypes.getConfirmedDetails(date)),
        getCancelledDetails : (date) => dispatch(actionTypes.getCancelledDetails(date)),
       
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Count))

