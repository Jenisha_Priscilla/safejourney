import React, {memo} from 'react'
import Badge from 'react-bootstrap/Badge'

const SeatRecord = (props) => {

    return (
      <React.Fragment> 
        <Badge  variant="dark">
            {props.obj.seat}
        </Badge>{' '}
      </React.Fragment>
    )
}

export default memo(SeatRecord)