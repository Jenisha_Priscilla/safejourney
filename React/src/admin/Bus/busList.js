import React, { Component } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import {Redirect} from 'react-router-dom';
import BusEdit from './busEdit';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/index'
import * as actionType from '../../store/actions/networkError'
import NetworkError from '../../component/networkError';

class BusList extends Component
{
    constructor() {
        super();
        this.delete = this.delete.bind(this);
        this.edit = this.edit.bind(this);
        this.state = {
            redirect: false,
            delete : false
        }
    }

    delete() {
        axios.get('/bus/delete/'+this.props.obj.BusId
        // {
        //     headers : {
        //         'AUTHENTICATION' : localStorage.getItem('token') 
        //     }
        // }
        )
        
        .then((response) => {
            if(response.data == false){
                this.props.show()
                this.props.authLogout()
            } else {
                this.setState({ redirect: false, delete: true})
               
            }
        })
        .catch(err => this.props.showNetworkError())
    }

    edit() {
            this.setState({ 
                redirect : !this.state.redirect 
            })  
            this.props.onUpdate();
    }

    render() {

        let result =  (
            <tr>
                <td>
                    {this.props.obj.RouteId}
                </td>
                <td>
                    {this.props.obj.DepartureStation}
                </td>
                <td>
                    {this.props.obj.ArrivalStation}
                </td>
                <td>
                    {this.props.obj.BusName}
                </td>
                <td>
                    {this.props.obj.BusType}
                </td>
                <td>
                    {this.props.obj.PlateNo}
                </td>
                <td>
                    {this.props.obj.SeatsCount}
                </td>
                <td>
                    {this.props.obj.Fare}
                </td>
                <td>
                    {this.props.obj.DepartureTime}   
                </td>
                <td>
                    {this.props.obj.ArrivalTime}   
                </td>
                { this.props.update == 1 ?
                <td style = {{width: 2}}>                
                    <div onClick = { this.edit }>  <FontAwesomeIcon icon = {faPen} size = "sm" /></div>
                </td> :null
                }
                
                <td  style = {{width: 2}}>
                    <div variant = "danger" onClick = {this.delete} >  
                        <FontAwesomeIcon icon = {faTrash} size = "sm" /> 
                    </div>
                </td>
            </tr>
        );   
            if (this.state.redirect  && !this.props.updateSuccess ) {
                result = ( <BusEdit id = {this.props.obj.RouteId} BusId = {this.props.obj.BusId} 
                    onEdit = {this.edit}/>)
            } 
            if (this.state.delete) {
                result =  <Redirect to = {'/viewBus'}/>
            }

            if(this.props.showError) {
                result = <NetworkError/>
            }
             
            return result;
        }
}
const mapStateToProps = state => {
    return {
        update: state.bus.update,
        updateSuccess : state.bus.updateSuccess,
        showError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    return {
        showNetworkError : () => dispatch(actionType.showNetworkError()), 
        onUpdate : () => dispatch(actionTypes.updateBus()),
        authLogout : () =>dispatch(actionTypes.authLogout()),
        show : () => dispatch(actionTypes.show())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BusList)








