import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions/route'
import NetworkError from '../../component/networkError';

class RouteInsert extends Component 
{
    constructor() {
        super();
        this.validate = this.validate.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            stationError : '',
            fieldError : '',
            notAvailableError: ''
        }
    }

    validate() {
        var fieldError = " ";
        var stationError = " ";
        
        if (this.props.route.arrivalStation == '' || this.props.route.departureStation == '' ||
        this.props.route.arrivalStation == undefined || this.props.route.departureStation == undefined) 
         {
            fieldError = '* All fields must be filled';
        }

        if (fieldError === " ") {
            var patt1 = /^[a-zA-Z]+$/g; 
            if (!this.props.route.departureStation.match(patt1)) {
                stationError = 'Must contain only alphabets';
            }
            if (!this.props.route.arrivalStation.match(patt1)) {
                stationError = 'Must contain only alphabets';
            }
        }

        if (fieldError != " " || stationError != " " ) {
            this.setState({fieldError, stationError})
            return false;
        }
        return true;
    }
   
    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const routeData = {
                arrivalStation:this.props.route.arrivalStation,
                departureStation:this.props.route.departureStation
            };

            this.props.routeInsert(routeData)
           
        }
    }

    render() {
        const style = {
            fontSize : 12,
            color :'red'
        }
        if(this.props.showError) {
            console.log("networkError")
            return  <NetworkError/>
        }

        return (
            <>
                <Col md = {{ span: 4, offset: 4 }}>
                    <h5 align = "center"> Insert Route </h5> 
                </Col>
                
                            <Form style = {{borderStyle: "solid" , borderColor: "black", padding: 20}}>
                                <div style = {style}>
                                    {this.state.fieldError}
                                </div>  
                                <div style = {style}>
                                    {this.state.stationError}
                                </div>  

                                <Form.Row>
                                    <Form.Label  column lg = {4}> Departure station </Form.Label>
                                    <Col>
                                        <Form.Control type = "text" name = "departureStation"
                                            placeholder = "From" 
                                            onChange = {this.props.onRouteChange} 
                                            value = {this.props.route.DepartureStation} 
                                        />
                                    </Col>
                                </Form.Row>
                                <br/>
                                <Form.Row>
                                    <Form.Label  column lg = {4}> Arrival station </Form.Label> 
                                    <Col>   
                                        <Form.Control type = "text" name = "arrivalStation"
                                            placeholder = "To"
                                            onChange = {this.props.onRouteChange}
                                            value = {this.props.route.ArrivalStation}  
                                        />   
                                    </Col>
                                </Form.Row>
                               
                                <div style = {{marginTop:40, textAlign: "center"}}> 
                                    <Button variant = "dark" onClick = {this.onSubmit}> Add </Button>
                                </div>
                            </Form>
                       
        </>
            )
       }
    }

    const mapStateToProps = state => {
        return {
            route : state.route.route,
            insertSuccess : state.route.insertSuccess,
            showError : state.networkError.showNetworkError
        }
    }
    
    const mapDispatchToProps = dispatch => {
        return {
            onRouteChange : (e) => dispatch(actionTypes.handleStationChange(e)),
            routeInsert : (routeData) => dispatch(actionTypes.routeInsert(routeData))
        }
    }

export default connect(mapStateToProps, mapDispatchToProps)( RouteInsert) 
