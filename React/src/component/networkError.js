import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/networkError'
import Modal from 'react-bootstrap/Modal'
import React from "react";
import { withRouter } from 'react-router-dom';

const NetworkError = (props) => 
{
    return (
      <>
        <Modal
          show = {props.showNetworkError}
          onHide = {()=> {props.hideNetworkError();
          }}
        >
          <Modal.Header closeButton>
            <Modal.Title >
              NETWORK ERROR
            </Modal.Title>
          </Modal.Header>
          <Modal.Body> Network error. Try after sometime</Modal.Body>
        </Modal>
      </>
    );
  }

  const mapStateToProps = state => {
    return {
        showNetworkError : state.networkError.showNetworkError
    }
}

const mapDispatchToProps = dispatch => {
    
    return {
        hideNetworkError : () => dispatch(actionTypes.hideNetworkError()),
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NetworkError))