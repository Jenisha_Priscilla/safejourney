<?php

function generateToken($payload) 
{
	$header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
	$base64UrlHeader =  str_replace(['+','/','='],['-','_',''],base64_encode($header));
	$base64UrlPayload = str_replace(['+','/','='],['-','_',''],base64_encode($payload));
	$signature = hash_hmac('sha256',$base64UrlHeader.".".$base64UrlPayload,'abc123!',true);
	$base64UrlSignature = str_replace(['+','/','='],['-','_',''],base64_encode($signature));
	$jwt = $base64UrlHeader.".".$base64UrlPayload.".".$base64UrlSignature;
	$data['payload'] = json_decode($payload);
	$data['token'] = $jwt;
    return $data;
}
?>
