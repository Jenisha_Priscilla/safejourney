import React from 'react'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faLongArrowAltRight } from "@fortawesome/free-solid-svg-icons";
import { faRupeeSign} from "@fortawesome/free-solid-svg-icons";
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { withRouter } from 'react-router-dom';

const SearchRecord = (props) => {
        
        return (
            <>
                <Card border = 'primary' className = "d-none d-lg-block p-auto">
                    <Card.Body>
                        <Row>
                            <Col style = {{fontSize : 15, fontWeight: 'bold'}}>
                                {props.obj.BusName.toUpperCase()}
                            </Col>
                            <Col>
                                {props.obj.DepartureTime}
                            </Col>
                            <Col sm = {1} className = "p-0">
                                <FontAwesomeIcon icon={ faLongArrowAltRight }/>
                            </Col>
                            <Col >
                                {props.obj.ArrivalTime}
                            </Col>
                            <Col style = {{color : 'green', fontWeight : 'bold'}}>
                            <FontAwesomeIcon icon={ faRupeeSign }/> {' '}{props.obj.Fare}
                            </Col>
                            <Col>
                            {
                                props.obj.rem != 0 ? 
                                    
                                        <Button style = {{fontSize: 10 ,backgroundColor :'black', color:'white'}} 
                                        onClick = {() => props.history.push('/book/'+props.obj.Id +"/"+ props.obj.DepartureTime)}>Book </Button>  
                                    :
                                    <Button style = {{fontSize: 10 ,backgroundColor :'black', color:'white'}} disabled> Book </Button> 
                            }
                            </Col>
                        </Row>
                        <Row style = {{fontSize : 10}}>
                            <Col>
                                {props.obj.BusType}
                            </Col>   
                            < Col >
                                {localStorage.getItem('departureStation')}
                            </Col>
                            < Col sm = {1} className = "p-0">
                            </Col>
                            <Col>
                                {localStorage.getItem('arrivalStation')} 
                            </Col>
                            < Col>
                            </Col>
                            <Col>
                                {props.obj.rem}{' Seat(s) left'}
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
               
                  
                <Card border = 'primary' className = "d-lg-none d-md-block" 
                    onClick = {()=> props.obj.rem != 0 ? props.history.push('/book/'+props.obj.Id +"/"+ props.obj.DepartureTime): null}>
                    <Card.Body>
                    <Row md={4}>
                        <Col xs={8} style = {{fontSize : 15, fontWeight: 'bold'}}>
                            {props.obj.BusName.toUpperCase()}
                        </Col>
                        <Col style = {{color : 'red', fontWeight : 'bold'}}><FontAwesomeIcon icon={ faRupeeSign }/> {' '}
                            {props.obj.Fare}
                        </Col>
                    </Row>
                    <Row md={4} style = {{fontSize : 10}}>
                        <Col xs = {8}> {props.obj.BusType}</Col>
                        <Col> {props.obj.rem}{' Seat[s] left'}</Col>
                    </Row>
                    <Row md={4} style = {{fontSize : 13}}>
                        <Col xs ={8}> {props.obj.DepartureTime }{' '}
                        <FontAwesomeIcon icon = { faLongArrowAltRight }/> 
                        {' '}{props.obj.ArrivalTime} 
                        </Col>
                    </Row>
                    </Card.Body>
                </Card>
                <br/>
                
             </>
          
        )
}

export default withRouter(SearchRecord);




