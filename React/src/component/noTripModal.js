import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import {withRouter} from 'react-router-dom'

class NoTripModal extends Component 
{
  constructor() {
    super()
    this.state = {
      show: true
    }
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.setState({ show: false});
    this.props.history.push('/')
  }

  render() {
    return (
      <Modal show = {this.state.show} onHide = {this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title> No Trips </Modal.Title>
        </Modal.Header>
        <Modal.Body> Looks like no trips to show. We are redirecting you to Home page </Modal.Body>
        <Modal.Footer>
          <Link to = "/" style = {{textDecoration: 'none'}}> Okay </Link>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default withRouter(NoTripModal)