import * as actionTypes from './actionsTypes'

export const show = () => {
    return {
        type : actionTypes.SHOW_MODAL
    }
}

export const hide = () => {
    return {
        type : actionTypes.HIDE_MODAL
    }
}
