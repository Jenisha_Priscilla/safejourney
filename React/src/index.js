import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import  {Provider} from 'react-redux'
import { createStore, applyMiddleware, compose ,combineReducers} from 'redux'
import reducer from './store/reducers/bus'
import thunk from 'redux-thunk'
import orderReducer from './store/reducers/order'
import routeReducer from './store/reducers/route'
import authReducer from './store/reducers/auth'
import searchReducer from './store/reducers/search'
import recordReducer from './store/reducers/record'
import inValidModalreducer from './store/reducers/inValidModal'
import networkError from './store/reducers/networkError'
import axios from 'axios';

axios.defaults.headers.common['AUTHENTICATION'] = JSON.stringify(localStorage.getItem('token')) ? JSON.stringify(localStorage.getItem('token')): null;

axios.defaults.baseURL = process.env.REACT_APP_API;

const rootReducer = combineReducers ({
    bus : reducer,
    order : orderReducer,
    route : routeReducer,
    auth : authReducer,
    search : searchReducer,
    record : recordReducer,
    inValidModal : inValidModalreducer,
    networkError : networkError
}); 

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (
    <Provider store = {store}>
        <App/>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
