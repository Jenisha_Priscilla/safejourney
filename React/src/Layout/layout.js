import React from 'react'
import Footer from '../component/footer'

 const Layout = (props) => {
    const mystyle = {
        minHeight : '100vh',
        marginTop : '50px'
      };
      
    return (
        <React.Fragment >
            {props.nav}
            <div style = {mystyle}>
                {props.children}
            </div>     
            <Footer/>
        </React.Fragment>
    )
 }
 export default Layout;