<?php
class Passenger
{
    private $name;
    private $age;
	private $seatNo;
	
    public function __set($property, $value)
    { 
        require_once './validation/validateProperty.php';
        $isValid = valid($value);
        if($isValid) {
            if(property_exists($this, $property)) {
                $this->$property = $value;
            }
            return $this;
        }
    }

    public function __get($property)
    {
        if(property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function toArray () 
	{	
        return [
            "name" => $this->__get('name'),
			"age" => $this->__get('age'),
            "seatNo" => $this->__get('seatNo')
        ];
    }
}
