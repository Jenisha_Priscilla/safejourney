import * as actionTypes from '../actions/actionsTypes'

const initialState = {
    route: {},
    routeEdit : {},
    insertSuccess :false,
    update : 1,
    updateSuccess : false,
    redirect : false
}

const routeReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SET_ROUTE:
            return {
                ...state,
                routeEdit : action.route
            }

        case actionTypes.HANDLE_STATIONCHANGE :
            action.event.persist();
            const updatedRoute = {
                ...state.route
            }
            const updatedRouteElement = {
                ...updatedRoute[action.event.target.name]
            }
          
            updatedRouteElement.value = action.event.target.value;
            updatedRoute[action.event.target.name] = updatedRouteElement.value 
            return {
                ...state,
                route : updatedRoute
            }

            case actionTypes.HANDLE_ROUTE_EDIT :
                action.event.persist();
                const updateRoute = {
                    ...state.routeEdit
                }
                const updateRouteElement = {
                    ...updateRoute[action.event.target.name]
                }
              
                updateRouteElement.value = action.event.target.value;
                updateRoute[action.event.target.name] = updateRouteElement.value 
                return {
                    ...state,
                    routeEdit : updateRoute
                }

        case actionTypes.ROUTE_UPDATE:
            return{
                ...state,
                update : state.update +1,
                updateSuccess : false
            }

        case actionTypes.ROUTE_UPDATE_SUCCESS:
            return {
                ...state,
                update : state.update-2,
                updateSuccess : true
            }

        case actionTypes.ROUTE_INSERT_SUCCESS :
            return {
                ...state,
                route: {
                    ArrivalStation : '',
                    DepartureStation : ''
                },
                insertSuccess : true
            }
            
        default : 
            return state;
    }
}

export default routeReducer