import React from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col' 
import RouteInsert from './routeInsert';
import RouteView from './routeView'
import Hoc from '../../hoc/hoc'

const Route = () => {

return (
    <>
        <Row style = {{margin: '10px'}}>
            <Col>
                <RouteView/>
            </Col>
            <Col >
                <RouteInsert/>   
            </Col>
        </Row>
    </>
    )
}

export default Route
